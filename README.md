# README #

This README provides steps that are necessary to get the BioASF application up and running for two example biological pathways described in the paper (see [Wiki](https://bitbucket.org/rezahay/bioasf/wiki/Home) for more information). You can also use these steps to run BioASF for simulating your own biological pathways.

### What is BioASF? ###

BioASF is a simulation software framework for simulating biological pathway models specified in BioPAX. The following input is needed to run BioASF:
   
* biological model specified in BioPAX,  
* a list of initial concentration values of the physical entities,   
* simulation rules indicating how biological interactions should be performed, and   
* analysis rules indicating how the simulation results over time are interpreted and analyzed.  

Based on these inputs, BioASF automatically generates a hierarchical multi-agent system (MAS) where each agent in the system is a goal-directed and autonomous entity responsible for executing a BioPAX interaction and pathway. The generated MAS can be run to perform the simulation of the given biological model. Various information during simulation (i.e., simulation results) are written in a log file. Both the location of the log file as well as the location of the input items mentioned above can be specified by users in a configuration file. Additionally, BioASF provides an *ANT* script (build.xml) which can be used to generate the MAS and run it with one command: *ant*. 
  
Version: 0.8

### How do I get set up BioASF to run my own model? ###

1. Be sure you have installed Java 1.7 or higher version and it is included in the system-path.
1. Be sure you have installed Apache ANT 1.9.4 or higher version and it is included in the system-path.
1. Download BioASF.zip from *Downloads* and unzip it into a folder (for example *myFolder*).
1. Go to *myFolder/BioASF*; this folder contains:  
*config  
examples  
lib  
logs  
rules  
build.xml*
1. Put your own JAR files in *lib/CustomJars*; JARS that you need for your simulation and analysis rules.
1. Open config/log4j.xml and change the log-file location ("your-path") in *<param name="file" value="/your-path/BioASF/logs/BioASF.log"/>*.

### How can I run the haematopoietic example? ###
1. Open *config/ebf-config.properties*. Be sure that the following configuration items have been set as below:  
*biopax_model = ./examples/Haematopoiesis.owl* (location of your BioPAX model)  
*algorithm_class = nl.vu.bioinf.ebf.model.algorithm.HematSimulationRules* (the fully qualified class name of your simulation rules)  
*analysis_class = nl.vu.bioinf.ebf.model.analysis.HematAnalysisAgent* (the fully qualified class name of your analysis rules)    
*step_type = sync* (your simulation mode; it can be sync, async, mps)
*output_dir= ./hemat-model* (the folder for generated Java files and build or class files)
1. Go to rules and see the example analysis and simulation rules in *nl/vu/bioinf/ebf/model/analysis and nl/vu/bioinf/ebf/model/algorithm*. 
1. Go back to *myFolder/BioASF* and run "ant". 
1. You will see the generated multi-agent code in the folder *myFolder/BioASF/hemat-model*. This folder contains:  
*build (contains class files of the generated multi-agent system)     
JavaSources (contains source files of the generated multi-agent system)   
JavaResources (contains initial concentrations)*     
1. You will see the logs including the simulation result in  */your-path/BioASF/logs/BioASF.log*.

### How can I run the WNT example? ###
1. Open *config/ebf-config.properties*. Be sure that the following configuration items have been set as below:  
*biopax_model = ./examples/Wnt-Experiment.owl* (location of your BioPAX model)  
*algorithm_class = nl.vu.bioinf.ebf.model.algorithm.WntSimulationRules* (the fully qualified class name of your simulation rules)  
*analysis_class = nl.vu.bioinf.ebf.model.analysis.WntAnalysisAgent* (the fully qualified class name of your analysis rules)  
*initial_concentrations = ./examples/Wnt-Init-Cons.dat*  
*step_type = mps* (your simulation mode; it can be sync, async, mps)
*output_dir= ./wnt-model* (the folder for generated Java files and build or class files)
1. Go to rules and see the example analysis and simulation rules in *nl/vu/bioinf/ebf/model/analysis and nl/vu/bioinf/ebf/model/algorithm*. 
1. Go back to *myFolder/BioASF* and run "ant". 
1. You will see the generated multi-agent code in the folder *myFolder/BioASF/wnt-model*. This folder contains:  
*build (contains class files of the generated multi-agent system)   
JavaSources (contains source files of the generated multi-agent system)   
JavaResources (contains initial concentrations)   
RSources (contains R module)*  
1. You will see the logs including the simulation result in  */your-path/BioASF/logs/BioASF.log*.
1. The folder *myFolder/BioASF/wnt-model* contains now the simulation result (*bcat.dat*). 
1. Start your R system.
1. Use *myFolder/BioASF/wnt-model/RSources/Wnt-Plot.R* R-module to plot the simulation result (*bcat.dat*).

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Reza Haydarlou (e-mail in the paper)