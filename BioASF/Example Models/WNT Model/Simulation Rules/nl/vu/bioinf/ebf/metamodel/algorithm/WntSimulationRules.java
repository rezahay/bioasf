package nl.vu.bioinf.ebf.model.algorithm;

import java.util.Collection;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.interaction.ConvAgent;
import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;
import nl.vu.bioinf.ebf.metamodel.algorithm.PerformIntAlgorithm;

import org.apache.log4j.Logger;

public class WntSimulationRules implements PerformIntAlgorithm {
	private static Logger logger = Logger.getLogger(WntSimulationRules.class);
	
	public void performDegradations(BioPAXEvent event) {
	}
	
	public boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		Map<String, Integer> inPartConMap = event.getInPartConMap();
		Collection<String> inParts = inPartConMap.keySet();
		Map<String, Float> stoichiometryMap = null;
		if (thisInt instanceof ConvAgent) {
			stoichiometryMap = ((ConvAgent)thisInt).getStoichiometryMap();
		}
		for (String inPart : inParts) {
			int requiredConcentration = 1;
			if (stoichiometryMap != null && stoichiometryMap.containsKey(inPart)) {
				requiredConcentration = Math.round(stoichiometryMap.get(inPart));
			}
			int inConcentration = inPartConMap.get(inPart);
			if (inConcentration < requiredConcentration) {
    			return false;
    		}
		}
		
		return true;
	}
	
	private void performTmpRegInteraction(GenericAgent thisInt, BioPAXEvent event) {
		// The input must be consumed and it should not available for other interactions any more. So, it must be decreased with 1.
		Map<String, Integer> inPartConMap = event.getInPartConMap();
		Collection<String> inParts = inPartConMap.keySet();
		for (String inPart : inParts) {
			inPartConMap.put(inPart, -1);
		}
		
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		Collection<String> outParts = outPartConMap.keySet();
		for (String outPart : outParts) {
			outPartConMap.put(outPart, 1);
		}
	}
	
	private void performTmpReacInteraction(GenericAgent thisInt, BioPAXEvent event) {
		Map<String, Integer> inPartConMap = event.getInPartConMap();
		String templateId = thisInt.getInputParticipants().iterator().next();
		Collection<String> inParts = inPartConMap.keySet();
		for (String inPart : inParts) {
			int deltaConcentration = 1;
			// Don't change the value of a gene template.
			if (templateId.equalsIgnoreCase(inPart)) {
				deltaConcentration = 0;
			}
			inPartConMap.put(inPart, -deltaConcentration);
		}
		
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		Collection<String> outParts = outPartConMap.keySet();
		for (String outPart : outParts) {
			outPartConMap.put(outPart, 1);
		}
	}
	
	private void performConvInteraction(GenericAgent thisInt, BioPAXEvent event) {
		Map<String, Integer> inPartConMap = event.getInPartConMap();
		Collection<String> inParts = inPartConMap.keySet();
		Map<String, Float> stoichiometryMap = ((ConvAgent)thisInt).getStoichiometryMap();
		for (String inPart : inParts) {
			int deltaConcentration = 1;
			if (stoichiometryMap.containsKey(inPart)) {
				deltaConcentration = Math.round(stoichiometryMap.get(inPart));
			}
			inPartConMap.put(inPart, -deltaConcentration);
		}
		
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		Collection<String> outParts = outPartConMap.keySet();
		//for (String outPart : outParts) {
		//	outPartConMap.put(outPart, 1);
		//}
		for (String outPart : outParts) {
			int deltaConcentration = 1;
			if (stoichiometryMap.containsKey(outPart)) {
				deltaConcentration = Math.round(stoichiometryMap.get(outPart));
			}
			outPartConMap.put(outPart, deltaConcentration);
		}
	}
	
	public void performInteraction(GenericAgent thisInt, BioPAXEvent event) {
		BioPAXInteactionType intType = event.getIntType();
    	switch (intType) {
    	case CmpAsms: case Degrs: case Trans: case Convs: case BioReacs:
    		performConvInteraction(thisInt, event);
    		break;
    	case Ctrls: case TmpRegs: case Cats: case Mods: 
    		performTmpRegInteraction(thisInt, event);
    		break;
    	case TmpReacs:	
    		performTmpReacInteraction(thisInt, event);
    		break;
    	default:
    		break;
    	}
	}	
}

