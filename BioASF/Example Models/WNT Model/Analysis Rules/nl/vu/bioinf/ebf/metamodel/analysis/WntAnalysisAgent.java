package nl.vu.bioinf.ebf.model.analysis;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.analysis.GenericAnalysisAgent;

import org.apache.log4j.Logger;

public class WntAnalysisAgent extends GenericAnalysisAgent {
	private static Logger logger = Logger.getLogger(WntAnalysisAgent.class); 
	
	private static final String BCAT_FILE_PATH = "./wnt-model/bcat.dat";
	private static final String AXIN_FILE_PATH = "./wnt-model/axin.dat";
	
	private static final int MAX_WNT_CON = 5;
	private static final int MAX_STEPS = 100;
	private static final int MAX_EXPRS = 50;
	
	private static final String BCAT_DISPLAY_NAME = "CTNNB1";
	private static final String AXIN_DISPLAY_NAME = "AXIN";
	private static final String WNT_DISPLAY_NAME = "WNT";
	
	// 0-5
	private static int curWntCon = 0;
	private static int curExpr = 1;
	private static int curStep = 1;
	
	private static StringBuffer bcatCons = new StringBuffer();
	//private static StringBuffer axinCons = new StringBuffer();
	
	private static boolean populateEnvFromInitialConcentrations = true;
	
	private void writeExprHeader() {
		bcatCons.append("Wnt = " + curWntCon + "\n");
		//axinCons.append("Wnt = " + curWntCon + "\n");
	}
	
	private void writeExprStep() {
		bcatCons.append(analysisBioEnv.getConcentration(BCAT_DISPLAY_NAME) + "\t");
		//axinCons.append(analysisBioEnv.getConcentration(AXIN_DISPLAY_NAME) + "\t");
	}
	
	private void writeNewLine() {
		bcatCons.append("\n");
		//axinCons.append("\n");
	}
	
	private void writeExprFooter() {
		bcatCons.append("End\n");
		//axinCons.append("End\n");
	}
	
	private void writeContentToFile(String content, String filePath) {
        Writer writer = null;
        try {
            writer = new FileWriter(filePath);
            writer.write(content);
        } 
        catch (IOException e) {
            logger.error("Error writing the file '" + filePath + "'.", e);
        } 
        finally {
            if (writer != null) {
                try {
                    writer.close();
                } 
                catch (IOException e) {
                	logger.error("Error closing the file '" + filePath + "'.", e);
                }
            }
        }
    }

	private void writeExprFile() {
		writeContentToFile(bcatCons.toString(), BCAT_FILE_PATH);
		//writeContentToFile(axinCons.toString(), AXIN_FILE_PATH);
	}
	
	protected void effectuateNextState() {
		analysisBioEnv.populateAnalysisEnvFromConcentrations(populateEnvFromInitialConcentrations);
		if (logger.isInfoEnabled()) {
			logger.info("##Initial state populated by AA for step [" + curStep + "]: " + analysisBioEnv.printState());
		}
	}
	
	protected void init() throws Exception {
		writeExprHeader();
		effectuateNextState();
    }
	
	private void stopAnalysis() {
		if (logger.isInfoEnabled()) {
			logger.info("##Number of steps reached maximum.");
		}	
		
		writeNewLine();
		writeExprFooter();
		writeExprFile();
		
		System.exit(0);
	}
	
	private void considerStoppingSimulation() {
    	stopSimulation();
    	writeExprStep();
    	if (curWntCon == MAX_WNT_CON && curExpr == MAX_EXPRS && curStep == MAX_STEPS) {
    		stopAnalysis();
    	}
    	else if (curExpr == MAX_EXPRS && curStep == MAX_STEPS) {
    		if (logger.isInfoEnabled()) {
    			logger.info("##Experiment for WNT-value '" + curWntCon + "' finished.");
    		}
    		curWntCon++;
    		curExpr = 1;
    		curStep = 1;
    		writeNewLine();
    		writeExprHeader();
    		analysisBioEnv.setInitialConcentration(WNT_DISPLAY_NAME, curWntCon);
    		populateEnvFromInitialConcentrations = true;
    		restartSimulation();
    	}
    	else if (curStep == MAX_STEPS) {
    		if (logger.isInfoEnabled()) {
    			logger.info("##Experiment number '" + curExpr + "' finished.");
    		}
    		curExpr++;
    		curStep = 1;
    		writeNewLine();
    		populateEnvFromInitialConcentrations = true;
			restartSimulation();
		}
		else {
			curStep++;
			populateEnvFromInitialConcentrations = false;
			restartSimulation();
		}
    }
	
	protected void handleIAEffectorEvent(BioPAXEvent IAEffectorEvent) {
		analysisBioEnv.updateAnalysisEnv(IAEffectorEvent);
		if (logger.isInfoEnabled()) {
			logger.info(analysisBioEnv.printState());
		}	
    }

	protected void handlePWayAAAllIntsPerformedEvent(BioPAXEvent pwayAAStatEvent) {
		if (logger.isDebugEnabled()) {
			logger.debug("$$All_Interactions_Performed message received from PWay.");
		}
		
    	considerStoppingSimulation();
    }
	
    protected synchronized void handlePWayAANoIntPerformedEvent(BioPAXEvent pwayAAStatEvent) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("$$No_Interaction_Performed message received from PWay.");
    	}	
    	considerStoppingSimulation();
    }
    
    public static void main(String[] args) {
    	if (logger.isDebugEnabled()) {
	    	logger.debug("========================================");
	    	logger.debug("WntAgent: Starting & initialising ...");
	    	logger.debug("========================================");
    	}	
        try {
        	new WntAnalysisAgent().start(args);
        }
        catch (Exception e) {
            logger.error("Exception in WntAgent: ", e);
            System.exit(1);
        }
    }
}

