package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Entity;
import nl.vu.bioinf.ebf.metamodel.Gene;
import nl.vu.bioinf.ebf.metamodel.Interaction;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayMasterAgent;

import org.apache.log4j.Logger;


public abstract class IntAgent extends GenericAgent {
	private static Logger logger = Logger.getLogger(IntAgent.class);
	
	protected Interaction intr = null;
	protected ArrayList<Control> ctrls = null;
	protected ArrayList<CtrlAgent> ctrlAgents = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Ints;
	}
	
	// If there are no inputs then return empty list and not a null.
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>(); 
		ArrayList<Entity> pIns = intr.getParticipant();
		
		// We consider an participant as an input if it is a PhysicalEntity or a Gen. A Pathway participant and 
		// an Interaction participant are considered as sensors. At this moment, we don't know how to deal a
		// Pathway or Interaction as a Participant.
		if (pIns != null) {
			for (Entity entity : pIns) {
				if (entity instanceof PhysicalEntity || entity instanceof Gene) {
					inputParticipants.add(entity.getId());
				}	
			}
		}
		return inputParticipants;
	}
	
	// If there are no outputs then return empty list and not a null.
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		ArrayList<Entity> pOuts = intr.getParticipant();
		
		// We consider an participant as an output if it is a PhysicalEntity or a Gen. A Pathway participant and 
		// an Interaction participant are considered as sensors.
		if (pOuts != null) {
			for (Entity entity : pOuts) {
				if (entity instanceof PhysicalEntity || entity instanceof Gene) {
					outputParticipants.add(entity.getId());
				}
			}
		}
		
		return outputParticipants;
	}
	
	public Collection<String> getSensoryControlParticipants() {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		// Add Control interaction which controls this genetic interaction.
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.add(ctrl.getId());
			}
		}
		
		return controlParticipants;
	}
	
	public Collection<String> getEffectorControlParticipants(BioPAXEvent event) {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.addAll(getEffectorControlParticipants(event, ctrl, ctrl.getController_PhysicalEntity()));
			}
		}
		
		return controlParticipants;
	}
	
	// Give back to the environment either the control-element itself or its controllers but not both of them. So, the control-element
	// is considered as consumed from the EA point of view.
	public Collection<String> getEffectorControlParticipants(BioPAXEvent event, Control ctrl, ArrayList<PhysicalEntity> controllers) {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		//if (event == null || event.getInConcentation(ctrl.getId()) <= 0) {
		if (event == null) {	
			controlParticipants.add(ctrl.getId());
		}	
		else {
			// Gather the ids of controllers which are PhysicalEntity objects.
			for (PhysicalEntity controller : controllers) {
				controlParticipants.add(controller.getId());
			}
		}
		
		return controlParticipants;
	}
	
	public Collection<String> getSensoryParticipants() {
		Collection<String> sensoryParticipants = new ArrayList<String>();
		sensoryParticipants.addAll(this.getInputParticipants());
		sensoryParticipants.addAll(this.getSensoryControlParticipants());

		return sensoryParticipants;
	}
	
	public boolean hasSensoryControls() {
		return ctrls != null;
	}
	
	public Collection<Control> getSensoryControls() {
		Collection<Control> sensoryControls = new ArrayList<Control>();
		
		if (ctrls != null) {
			sensoryControls.addAll(ctrls);
		}
		
		return sensoryControls;
	}
	
	public Collection<CtrlAgent> getSensoryControlAgents() {
		Collection<CtrlAgent> sensoryControlAgents = new ArrayList<CtrlAgent>();
		
		if (ctrlAgents != null) {
			sensoryControlAgents.addAll(ctrlAgents);
		}
		
		return sensoryControlAgents;
	}
	
	public Collection<String> getEffectorParticipants(BioPAXEvent event) {
		Collection<String> outputParticipants = new ArrayList<String>();
		outputParticipants.addAll(this.getOutputParticipants());
		
		// The assumption is that Controls are not consumed. So, we have to give it back to the environment. However,
		// note that we do give back the controllers of a Control. If we give back the Control itself then its controller
		// never becomes free to be used by some other interaction. Suppose that the controller of a TemplateReactionRegulation
		// is a transcription factor Gata1 and suppose that there is some other interaction (e.g., Degradation) in the 
		// pathway. If TemplateReactionRegulation can be performed before Degradation then Degradation would never get a
		// chance to degrade Gata1, in case if we give back TemplateReactionRegulation and not Gata1.
		//outputParticipants.addAll(this.getEffectorControlParticipants(event));
		
		return outputParticipants;
	}
	
	public boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		return PWayMasterAgent.getPerformIntAlgorithm().canIntBePerformed(thisInt, event);
	}
	
	public void performInteraction(GenericAgent thisInt, BioPAXEvent event) {
		PWayMasterAgent.getPerformIntAlgorithm().performInteraction(thisInt, event);
	}
}

