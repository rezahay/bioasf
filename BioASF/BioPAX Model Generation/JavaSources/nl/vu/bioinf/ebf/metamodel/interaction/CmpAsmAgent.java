package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.ComplexAssembly;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class CmpAsmAgent extends ConvAgent {
	private static Logger logger = Logger.getLogger(CmpAsmAgent.class);
	
	protected ComplexAssembly cmpAsm = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.CmpAsms;
	}
	
	public Collection<String> getInputParticipants() {
		conv = cmpAsm;
		return super.getInputParticipants();
	}
	
	public Collection<String> getOutputParticipants() {
		conv = cmpAsm;
		return super.getOutputParticipants();
	}
}

