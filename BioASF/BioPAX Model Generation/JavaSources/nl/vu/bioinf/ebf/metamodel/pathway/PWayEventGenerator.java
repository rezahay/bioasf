package nl.vu.bioinf.ebf.metamodel.pathway;

import java.util.Observable;

import nl.vu.bioinf.ebf.metamodel.analysis.GenericAnalysisAgent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;

import org.apache.log4j.Logger;

public class PWayEventGenerator extends Observable {
	private static Logger logger = Logger.getLogger(PWayEventGenerator.class);
	
	public PWayEventGenerator(GenericAnalysisAgent analysisAgent) {
		addObserver(analysisAgent);
	}
	
	public void generateEvent(BioPAXEvent event) {
		setChanged();
		notifyObservers(event);
	}
	
}

