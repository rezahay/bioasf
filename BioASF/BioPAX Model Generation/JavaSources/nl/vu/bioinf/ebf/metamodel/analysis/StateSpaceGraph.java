package nl.vu.bioinf.ebf.metamodel.analysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.graphstream.algorithm.TarjanStronglyConnectedComponents;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

public class StateSpaceGraph {
	private static Logger logger = Logger.getLogger(StateSpaceGraph.class);
	
	private Map<Node, Integer> compMap = new HashMap<Node, Integer>();
	private HashMap<Integer, Boolean> termMap = new HashMap<Integer, Boolean>();
	private int termNum = 0;
	
	private int edgeIdPostfix = 0;
	private Graph stateSpaceGraph = null;
	private Map<Object, List<Node>> prevSccIndxToNodes = new HashMap<Object, List<Node>>();
	private Map<Object, List<Node>> curSccIndxToNodes = new HashMap<Object, List<Node>>();
	
	public StateSpaceGraph() {
		initiateGraph();
	}
	
	private void initiateGraph() {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		stateSpaceGraph = new MultiGraph("State Space Graph");
		stateSpaceGraph.addAttribute("ui.stylesheet", "node { shape: circle; size: 55px, 40px; fill-color: orange;  }");
	}
	
	private void addNodeToSccList(Object sccIndx, Node node) {
    	List<Node> nodes = null;
    	if (curSccIndxToNodes.containsKey(sccIndx)) {
    		nodes = curSccIndxToNodes.get(sccIndx);
    	}
    	else {
    		nodes = new ArrayList<Node>();
    		curSccIndxToNodes.put(sccIndx, nodes);
    	}
    	nodes.add(node);
	}
	
	public boolean compareSCCs() {
		if (curSccIndxToNodes.size() != prevSccIndxToNodes.size()) {
			System.out.println("$$$$$$$$$$$$$$$$$$ different SCCs.");
			return false;
		}
		for (Object sccIndx : curSccIndxToNodes.keySet()) {
			List<Node> curNodes = curSccIndxToNodes.get(sccIndx);
			List<Node> prevNodes = prevSccIndxToNodes.get(sccIndx);
			if (curNodes.size() != prevNodes.size()) {
				System.out.println("$$$$$$$$$$$$$$$$$$ different nodes.");
				return false;
			}
		}
		return true;
	}
	
	
	public int getSizeOfLargestSCC() {
		int largestSCCSize = 0;
		
		for (List<Node> nodes : curSccIndxToNodes.values()) {
			int numOfSCCNodes = nodes.size();
			if (largestSCCSize < numOfSCCNodes) {
				largestSCCSize = numOfSCCNodes;
			}
		}
		
		return largestSCCSize;
	}
	
	public int getNumOfSCCs() {
		return curSccIndxToNodes.size();
	}
	
	public String printSCCNodes() {
		StringBuffer result = new StringBuffer();
		result.append("############################## Number SCCs: " + curSccIndxToNodes.size() + " #############################\n");
		for (Object sccIndx : curSccIndxToNodes.keySet()) {
			List<Node> nodes = curSccIndxToNodes.get(sccIndx);
			result.append("##Number of nodes for SCC '" + sccIndx + "' is: " + nodes.size() + "\n");
			for (Iterator iterator = nodes.iterator(); iterator.hasNext();) {
				Node node = (Node) iterator.next();
				result.append("{");
				for (Iterator iterator2 = node.getEnteringEdgeIterator(); iterator2.hasNext();) {
					Edge enteringEdge = (Edge) iterator2.next();
					result.append(enteringEdge.getSourceNode().getId() + "(" + enteringEdge.getSourceNode().getAttribute("SCC") + "),");
				}
				if (result.charAt(result.length()-1) == ',') {
					result.setLength(result.length() - 1);
				}	
				result.append("} --> {" + node.getId() + "(" + node.getAttribute("SCC") + ")} --> {");
				for (Iterator iterator3 = node.getLeavingEdgeIterator(); iterator3.hasNext();) {
					Edge leavingEdge = (Edge) iterator3.next();
					result.append(leavingEdge.getTargetNode().getId() + "(" + leavingEdge.getTargetNode().getAttribute("SCC") + "),");
				}
				if (result.charAt(result.length()-1) == ',') {
					result.setLength(result.length() - 1);
				}
				result.append("}\n");
			}
		}
		
		return result.toString();
	}
	
	public void displayGraph() {
		stateSpaceGraph.display();
	}
	
	public void updateGraph(int sourceState, int targetState) {
		String sourceStateId = String.valueOf(sourceState);
		String targetStateId = String.valueOf(targetState);
		
		Node sourceStateNode = stateSpaceGraph.getNode(sourceStateId);
		if (sourceStateNode == null) {
			sourceStateNode = stateSpaceGraph.addNode(sourceStateId);
			sourceStateNode.addAttribute("ui.label", sourceStateId);
		}
		
		Node targetStateNode = stateSpaceGraph.getNode(targetStateId);
		if (targetStateNode == null) {
			targetStateNode = stateSpaceGraph.addNode(targetStateId);
			targetStateNode.addAttribute("ui.label", targetStateId);
		}
		
		for (Iterator iterator = sourceStateNode.getLeavingEdgeIterator(); iterator.hasNext();) {
			Edge leavingEdge = (Edge) iterator.next();
			if (leavingEdge.getTargetNode().getId().equalsIgnoreCase(targetStateId)) {
				// Edge does exist. We are not going to add any edge to the graph.
				return;
			}
		}
		
		// Add an edge to the graph.
		String edgeId = sourceStateId + String.valueOf(++edgeIdPostfix);
		stateSpaceGraph.addEdge(edgeId , sourceStateId, targetStateId, true);
	}
	
	public void findSCC() {
		TarjanStronglyConnectedComponents scc = new TarjanStronglyConnectedComponents();
		scc.setSCCIndexAttribute("SCC");
        scc.init(stateSpaceGraph);
        scc.compute();
        prevSccIndxToNodes.clear();
        prevSccIndxToNodes.putAll(curSccIndxToNodes);
        curSccIndxToNodes.clear();
        for (Node node : stateSpaceGraph.getEachNode()) {
        	addNodeToSccList(node.getAttribute("SCC"), node);
        }
	}
	
	public void findTSCC() {
		TarjanStronglyConnectedComponents scc = new TarjanStronglyConnectedComponents();
		scc.setSCCIndexAttribute("SCC");
        scc.init(stateSpaceGraph);
        scc.compute();
        
		for (Node node : stateSpaceGraph.getEachNode()) {
			compMap.put(node, (Integer)node.getAttribute("SCC"));
        }
		
		int compMax = Collections.max(compMap.values());
		for (int i = 0; i <= compMax; i++) {
			termMap.put(i, true);
		}
		
		int compNum = 0;
		for (Edge edge : stateSpaceGraph.getEachEdge()) {
			int sourceComp = compMap.get(edge.getSourceNode());
			int targetComp = compMap.get(edge.getTargetNode());
			if (sourceComp != targetComp) {
				if (termMap.put(sourceComp, false)) {
					// if sourceComp was believed to be terminal
					compNum++;
				}
			}
		}
		
		termNum = compMax - compNum + 1;
	}
	
	public String printTSCC(AnalysisBioEnv globalBioEnv) {
		StringBuffer result = new StringBuffer();
		result.append("Components Found: " + termMap.size() + "\n");
		result.append("Terminal Components Found: " + termNum + "\n");
		
		result.append("Comp#");
		result.append(globalBioEnv.printOrderedGeneList());
		result.append("\n");
		
		// loops through the terminal components
		for (int term : termMap.keySet()) {
			if (!termMap.get(term)) {
				continue;
			}	
			// loops through the states of the selected component
			for (Node node : compMap.keySet()) {
				if (compMap.get(node) != term) {
					continue;
				}	
				result.append(term);
				int state = Integer.valueOf(node.getId());
				result.append(globalBioEnv.printBitVector(state));
				result.append("\n");
			}
		}
		
		return result.toString();
	}
}
