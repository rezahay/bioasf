package nl.vu.bioinf.ebf.metamodel.event;

public enum BioPAXInteactionType {
	Ints, Ctrls, Convs, GInts, MInts, TmpReacs, Cats, TmpRegs, Mods, CmpAsms, BioReacs, Degrs, Trans;
}

