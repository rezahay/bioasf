package nl.vu.bioinf.ebf.metamodel.analysis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.TreeMap;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;



public class AnalysisBioEnv {
	private static Logger logger = Logger.getLogger(AnalysisBioEnv.class);

	private static class ValueComparator implements Comparator<String> {
	    Map<String, String> base;
	    public ValueComparator(Map<String, String> base) {
	        this.base = base;
	    }

	    public int compare(String a, String b) {
	    	return base.get(a).compareTo(base.get(b));
	    }
	}
	
	private static AnalysisBioEnv analysisBioEnv = AnalysisBioEnv.getInstance();
	
	// This map contains the initial concentration value for each participant key (Prot_00001 --> 5).
	private static Map<String, Integer> initialConcentrations = new HashMap<String, Integer>();
	
	// This map contains the current concentration value for each participant key (Prot_00001 --> 1).
	private static Map<String, Integer> workingConcentrationStorage = new HashMap<String, Integer>();
	
	// This is the backup of the workingConcentrationStorage.
	private static Map<String, Integer> backupConcentrationStorage = new HashMap<String, Integer>();
	
	// It contains the names of the proteins (Prot_00001 --> BCAT).
	private static Map<String, String> keyTodisplayNameMap = new HashMap<String, String>();
	
	// It contains the names of the proteins (BCAT --> Prot_00001).
	private static Map<String, String> displayNameToKeyMap = new HashMap<String, String>();
	
	// Keys of physical entities (e.g, genes or proteins) used for generating a state (which is a number). List guarantees the
	// order but set does not. For example, if Prot_00005 = Erg, Prot_00003 = Fli1, Prot_00006 = Zpfm then physicalEntityKeyList
	// contains Prot_00005, Prot_00003, Prot_00006.
	private static List<String> physicalEntityKeyList = null;
	
	public static AnalysisBioEnv getInstance() {
		if (analysisBioEnv == null) {
			analysisBioEnv = new AnalysisBioEnv();
		}
		return analysisBioEnv;
	}
	
	private void consPhysicalEntityList() {
		ValueComparator valueComparator =  new ValueComparator(keyTodisplayNameMap);
		Map<String, String> orderedDisplayNames = new TreeMap<String, String>(valueComparator);
		orderedDisplayNames.putAll(keyTodisplayNameMap);
		physicalEntityKeyList = new ArrayList<String>(orderedDisplayNames.keySet());
	}
	
	private void consDisplayNamesAndInitialConcentrations(Properties properties) {
    	for (String fullyQualifiedKey : properties.stringPropertyNames()) {
    		int concentration = Integer.parseInt(properties.get(fullyQualifiedKey).toString());
			String key = fullyQualifiedKey.substring(0, fullyQualifiedKey.indexOf('('));
			
			initialConcentrations.put(key, concentration);
			
			String displayName = fullyQualifiedKey.substring(fullyQualifiedKey.indexOf('(')+1, fullyQualifiedKey.indexOf(')'));			
			keyTodisplayNameMap.put(key, displayName);
			displayNameToKeyMap.put(displayName, key);
		}
    }
	
	public void createAnalysisEnv(String resourceOutputDir) {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream(resourceOutputDir + "/" + Constants.EXPRESSION_FILE));
    		int numOfPhysicalEntities = properties.size();
    		consDisplayNamesAndInitialConcentrations(properties);
    		consPhysicalEntityList();
    		
    		logger.info("Number of biological entities in the concentration file is: " + numOfPhysicalEntities);
    	}   
    	catch (FileNotFoundException fnf) {
    		logger.error("File '" + Constants.EXPRESSION_FILE + "' could not be found.", fnf);
    	}
    	catch (IOException ioe) {
        	logger.error("File '" + Constants.EXPRESSION_FILE + "' could not be read.", ioe);
        }
	}
	
	public int getNumOfPhysicalEntities() {
		return physicalEntityKeyList.size();
	}
	
	public int[] consBitVectorFromState(int state) {
		int numOfPhysicalEntities = physicalEntityKeyList.size();
		String stateInBase2 = Integer.toString(state, 2); 
		int numOfLeadingZeros = numOfPhysicalEntities - stateInBase2.length();
		for (int i = 0; i < numOfLeadingZeros; i++) {
			stateInBase2 = "0" + stateInBase2;
		}
		
		// It contains a state in the form of '0's and '1's.
		int[] bitVector = new int[numOfPhysicalEntities];
		/*
		char[] bits = stateInBase2.toCharArray();
		for (int i = 0; i < numOfPhysicalEntities ; i++) {
			int bit = Character.getNumericValue(bits[i]);
			bitVector[i] = bit;
		}
		*/
		
		char[] bits = stateInBase2.toCharArray();
		for (int i = 0; i < numOfPhysicalEntities ; i++) {
			int bit = Character.getNumericValue(bits[i]);
			bitVector[(numOfPhysicalEntities-1)-i] = bit;
		}
		
		return bitVector;
	}
	
	public void populateAnalysisEnvFromState(int state) {
		workingConcentrationStorage.clear();
		int[] bitVector = consBitVectorFromState(state);
		for (String physicalEntityKey : physicalEntityKeyList) {
			int index = physicalEntityKeyList.indexOf(physicalEntityKey);
			int concentration = bitVector[index];
			workingConcentrationStorage.put(physicalEntityKey, concentration);
		}
	}
	
	/*
	 * fromInitialConcentrations = true --> populate environment from initial concentrations.
	 * fromInitialConcentrations = false --> populate environment from current concentrations.
	 */
	public void populateAnalysisEnvFromConcentrations(boolean fromInitialConcentrations) {
		if (fromInitialConcentrations) {
			workingConcentrationStorage.clear();
			workingConcentrationStorage.putAll(initialConcentrations);
		}	
	}
	
	public void storeAnalysisEnvToBackup() {
		backupConcentrationStorage.clear();
		backupConcentrationStorage.putAll(workingConcentrationStorage);
	}
	
	public void retrieveAnalysisEnvFromBackup() {
		workingConcentrationStorage.clear();
		workingConcentrationStorage.putAll(backupConcentrationStorage);
	}
	
	public int consStateFromAnalysisEnv() {
		int state = 0;
		
		List<Integer> indexes = new ArrayList<Integer>();
		for (String entityName: workingConcentrationStorage.keySet()) {
			if (workingConcentrationStorage.get(entityName) == 1) {
				indexes.add(physicalEntityKeyList.indexOf(entityName));
			}
		}
		
    	for (Integer index : indexes) {
			// Java bug: In the statement below 'state' is not increased by 'state'. 
			// state += (int)Math.pow(2, index);
			state = state + (int)Math.pow(2, index);
		}
		
		return state;
	}
	
	public BioPAXEvent consAnalysisEAEffector() {
    	BioPAXEvent EAEffectorEvent = new BioPAXEvent();
    	EAEffectorEvent.setAgentId(null);
    	EAEffectorEvent.setIntType(null);
    	EAEffectorEvent.setIntPerformed(false);
    	EAEffectorEvent.setEventType(Constants.EA_EFFECTOR);
    	
    	Map<String, Integer> envInPartConMap = new HashMap<String, Integer>(workingConcentrationStorage);
    	EAEffectorEvent.setInPartConMap(envInPartConMap);
    	
    	return EAEffectorEvent;
	}
	
	public void updateAnalysisEnvWithSensors(BioPAXEvent IAEffectorEvent) {
		Map<String, Integer> inPartConMap = IAEffectorEvent.getInPartConMap();
		Collection<String> sensorParticipants = inPartConMap.keySet();
		for (String participantId : sensorParticipants) {
			int outConcentration = inPartConMap.get(participantId);
			updateConcentration(participantId, outConcentration);
		}
	}
	
	public void updateAnalysisEnvWithEffectors(BioPAXEvent IAEffectorEvent) {
		Map<String, Integer> outPartConMap = IAEffectorEvent.getOutPartConMap();
		Collection<String> effectorParticipants = outPartConMap.keySet();
		for (String participantId : effectorParticipants) {
			int outConcentration = outPartConMap.get(participantId);
			updateConcentration(participantId, outConcentration);
		}
	}
	
	public void updateAnalysisEnv(BioPAXEvent IAEffectorEvent) {
		updateAnalysisEnvWithSensors(IAEffectorEvent);
		updateAnalysisEnvWithEffectors(IAEffectorEvent);
	}
	
	// ================================================================================================
	
	private void printState(StringBuffer result) {
		for (String physicalEntityKey : physicalEntityKeyList) {
			result.append(keyTodisplayNameMap.get(physicalEntityKey));
			result.append("=");
			result.append(workingConcentrationStorage.get(physicalEntityKey));
			result.append(",");
		}
		result.setLength(result.length() - 1);
		result.append("}");
	}
	
	public String printState() {
		StringBuffer result = new StringBuffer();
		result.append("state {");
		printState(result);
		return result.toString();
	}
	
	public String printState(int state) {
		StringBuffer result = new StringBuffer();
		result.append("state (" + state + ") {");
		printState(result);
		return result.toString();
	}
	
	public List<String> getOrderedGeneList() {
		List<String> orderedGeneList = new ArrayList<String>();
		for (String physicalEntityKey : physicalEntityKeyList) {
			orderedGeneList.add(keyTodisplayNameMap.get(physicalEntityKey));
		}
		
		return orderedGeneList;
	}
	
	public String printOrderedGeneList() {
		StringBuffer result = new StringBuffer();
		for (String physicalEntityKey : physicalEntityKeyList) {
			result.append("\t");
			result.append(keyTodisplayNameMap.get(physicalEntityKey));
		}
		
		return result.toString();
	}
	
	public String printBitVector(int state) {
		StringBuffer result = new StringBuffer();
		int[] bitVector = consBitVectorFromState(state);
		for (String physicalEntityKey : physicalEntityKeyList) {
			int index = physicalEntityKeyList.indexOf(physicalEntityKey);
			int concentration = bitVector[index];
			result.append("\t");
			result.append(concentration);
		}
		
		return result.toString();
	}
	
	// ================================================================================================

	private void updateConcentration(String participantId, int deltaConcentration) {
		int envConcentration = workingConcentrationStorage.get(participantId);
		envConcentration = envConcentration + deltaConcentration;
		workingConcentrationStorage.put(participantId, new Integer(envConcentration));
	}
	
	public void adjustConcentration(String displayName, int deltaConcentration) {
		String key = displayNameToKeyMap.get(displayName);
		updateConcentration(key, deltaConcentration);
	}
	
	public int getConcentration(String displayName) {
		String key = displayNameToKeyMap.get(displayName);
		return workingConcentrationStorage.get(key);
	}
	
	public void adjustInitialConcentration(String displayName, int deltaConcentration) {
		String key = displayNameToKeyMap.get(displayName);
		int newConcentration = initialConcentrations.get(key);
		newConcentration = newConcentration + deltaConcentration;
		initialConcentrations.put(key, new Integer(newConcentration));
	}
	
	public int getInitialConcentration(String displayName) {
		String key = displayNameToKeyMap.get(displayName);
		return initialConcentrations.get(key);
	}
	
	public void setInitialConcentration(String displayName, int newConcentration) {
		String key = displayNameToKeyMap.get(displayName);
		initialConcentrations.put(key, new Integer(newConcentration));
	}
}
