package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class IAEffectorQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(IAEffectorQEH.class);

    public IAEffectorQEH() {   
    	super.init(Constants.IA_EFFECTOR_QUEUE);
    }
}

