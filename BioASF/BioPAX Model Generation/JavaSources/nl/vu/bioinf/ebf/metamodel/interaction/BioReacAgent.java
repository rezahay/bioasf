package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.BiochemicalReaction;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class BioReacAgent extends ConvAgent {
	private static Logger logger = Logger.getLogger(BioReacAgent.class);
	
	protected BiochemicalReaction bioReac = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.BioReacs;
	}
	
	public Collection<String> getInputParticipants() {
		conv = bioReac;
		return super.getInputParticipants();
	}
	
	public Collection<String> getOutputParticipants() {
		conv = bioReac;
		return super.getOutputParticipants();
	}
}

