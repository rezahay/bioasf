package nl.vu.bioinf.ebf.metamodel.event;

import javax.jms.DeliveryMode;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

public abstract class BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(BioPAXQueueEventHandler.class);

    private static final long SLEEP_TIME = 100;
    private static final boolean TRANSACTED = false;
    private static final boolean PERSISTENT = false;
	
    // Should not be static for Local broker (VM).
	private QueueConnection connection = null;
	
	private QueueSession session = null;
    private Queue queue = null;

    protected void init(String destination) {
        try {
        	if (logger.isDebugEnabled()) {
        		logger.debug("Connecting to URL from BioPAXQueueEventHandler: " + Constants.BROKER_URL);
        	}	
        	
            // Create a shared connection.
            ActiveMQConnectionFactory connectionFactory = 
            		new ActiveMQConnectionFactory(Constants.BROKER_USER, Constants.BROKER_PASSWORD, Constants.BROKER_URL);
           if (connection == null) {
            	connection = connectionFactory.createQueueConnection();
            	connection.start();
            	if (logger.isDebugEnabled()) {
            		logger.debug("connection created for: " + destination);
            	}	
           }	

            // Create a session per thread.
            session = connection.createQueueSession(TRANSACTED, Session.AUTO_ACKNOWLEDGE);
            queue = session.createQueue(destination);
        } 
        catch (Exception e) {
            logger.error("Exception durinig init(): ", e);
        } 
    }   
    
    public MessageProducer createProducer() {
    	MessageProducer producer = null;
    	try {
    		producer = session.createProducer(queue);

            if (PERSISTENT) {
            	producer.setDeliveryMode(DeliveryMode.PERSISTENT);
            } 
            else {
            	producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            }
    	}
    	catch (Throwable t) {
            logger.error("Failed during createProducer: ", t);
        } 
    	return producer;
    }
    
    public void produce(BioPAXEvent event, MessageProducer producer) {
    	try {
    		TextMessage message = null;
            message = session.createTextMessage();
            message.setStringProperty(Constants.EVENT_TYPE, event.getEventType());
            message.setStringProperty(Constants.AGENT_ID, event.getAgentId());
            message.setStringProperty(Constants.INT_TYPE, event.getIntTypeAsString());
            message.setBooleanProperty(Constants.INT_PERFORMED, event.isIntPerformed());
            /*
            message.setStringProperty(Constants.IN_PARTICIPANT_IDS, event.getInParticipantIdsAsString());
            message.setStringProperty(Constants.IN_CONCENTRATIONS, event.getInConcentrationsAsString());
            message.setStringProperty(Constants.OUT_PARTICIPANT_IDS, event.getOutParticipantIdsAsString());
            message.setStringProperty(Constants.OUT_CONCENTRATIONS, event.getOutConcentrationsAsString());
            */
            message.setStringProperty(Constants.IN_PART_CON_MAP, event.getInPartConMapAsString());
            message.setStringProperty(Constants.OUT_PART_CON_MAP, event.getOutPartConMapAsString());

            if (logger.isDebugEnabled()) {
            	logger.debug("Sending event: " + event.toString());
            }	
            
            producer.send(message);

            Thread.sleep(SLEEP_TIME);
            
        } 
    	catch (Throwable t) {
            logger.error("Produce failed because: ", t);
        }
    }
    
    public void createConsumer(MessageListener listner, String messageSelector) {
    	try {
    		MessageConsumer consumer = session.createConsumer(queue, messageSelector, true);
    		consumer.setMessageListener(listner);
    		if (logger.isDebugEnabled()) {
                logger.debug("Listening (to Queue=" + queue.getQueueName() + ")\n");
            }
    	}
    	catch (Throwable t) {
            logger.error("Failed during createConsumer: ", t);
        } 
    }

    
    public void closeAll() {
    	try {
	        if (session != null) {
	            session.close();
	        }
	        if (connection != null) {
	            connection.close();
	        }
    	}
        catch (Throwable ignore) {
        	logger.error("Exception during closing JMS session: ", ignore);
        }
    }
}

