package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;
import nl.vu.bioinf.ebf.metamodel.Modulation;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class ModAgent extends CtrlAgent {
	private static Logger logger = Logger.getLogger(ModAgent.class);
	
	
	protected Modulation mod = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Mods;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();		
		ArrayList<PhysicalEntity> pIns = mod.getController();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		ctrl = mod;
		return super.getOutputParticipants();
	}
	
}

