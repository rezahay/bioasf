package nl.vu.bioinf.ebf.metamodel.algorithm;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Enum_controlType;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayMasterAgent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class PerformIntHemat implements PerformIntAlgorithm {
	private static Logger logger = Logger.getLogger(PerformIntHemat.class);
	
	private static Collection<String> nonDegradables = new HashSet<String>();
	
	private boolean checkInputConcentrations(BioPAXEvent event, boolean checkForPresence) {
		Collection<Integer> inConcentrations = event.getInPartConMap().values();
		for (int inConcentration : inConcentrations) {
			if (checkForPresence) {
				if (inConcentration <= 0) {
	    			return false;
	    		}
			}
			else {
				if (inConcentration > 0) {
	    			return false;
	    		}
			}
		}
		
		return true;
	}
	
	private boolean checkSensoryConcentrations(GenericAgent thisInt, BioPAXEvent event,  boolean checkForPresence) {
		Collection<Control> controls = thisInt.getSensoryControls();
		Control control = controls.iterator().next();
		String controlParticipant = control.getId();
		int controlConcentration = event.getInConcentation(controlParticipant);
		if (checkForPresence) {
			if (controlConcentration > 0) {
				return true;
			}
		}
		else {
			if (controlConcentration <= 0) {
    			return true;
    		}
		}

		// None of controls has an accepted concentration level.
		return false;
	}
	
	private boolean checkOutputConcentrations(BioPAXEvent event, boolean checkForPresence) {
		Collection<Integer> outConcentrations = event.getOutPartConMap().values();
		if (outConcentrations.size() != 1) {
			logger.warn("!!A tmpReac interaction in Hematopoiesis network must only have one output.");
			return false;
		}
		Integer outConcentration = outConcentrations.iterator().next();
		if (checkForPresence) {
			if (outConcentration <= 0) {
    			return false;
    		}
		}
		else {
			if (outConcentration >= 1) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean canTmpRegBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		// Pre-enablement check. TmpReg has no post-enablement check.
		return checkInputConcentrations(event, true);
	}
	
	private boolean canTmpReacBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		Collection<Control> controls = thisInt.getSensoryControls();
		if (controls.size() != 1 ) {
			logger.warn("Each tmpReac must only have one control.");
			return false;
		}
		
		// Pre-enablement check.
		if (checkSensoryConcentrations(thisInt, event, true) == false) {
			return false;
		}
		
		// Post-enablement check.
		Control control = controls.iterator().next();
		Enum_controlType ctrlType = control.getControlType();
		switch (ctrlType) {
		case ACTIVATION: 
			Collection<String> outParticipants = event.getOutPartConMap().keySet();
			String outParticipant = outParticipants.iterator().next();
			nonDegradables.add(outParticipant);
			// concentration value of the output must be zero.
			return checkOutputConcentrations(event, false); 
		case INHIBITION: 
			// concentration value of the output must be one.
			return checkOutputConcentrations(event, true); 
		default:
    		break;
		}
		
		return true;
	}
	
	
	
	/*
	 * input:
	 * if (all-inputs-of-tmpReg == 1) --> tmpReg = 1; 
	 * if (one-of-inputs-of-tmpReg == 0) --> tmpReg = 0;
	 * 
	 * if (controlType == activation) && tmpReg == 0 && output > 0 --> output--;
	 * if (controlType == inhibition) && tmpReg == 1 && output > 0 --> output--;
	 * 
	 * if (controlType == activation) && one-of-inputs == 0 && output > 0 --> output--;
	 * if (controlType == inhibition) && one-of-inputs == 1 && output > 0 --> output--;
	 * A new IAEffector must be built after each degradation.
	 */
	public void performDegradations(BioPAXEvent event) {
		Collection<String> participantIds = event.getInPartConMap().keySet();
		for (String participantId : participantIds) {
			if (nonDegradables.contains(participantId)) {
				continue;
			}	
			int concentration = event.getInConcentation(participantId);
			if (concentration <= 0) {
				continue;	
			}
		
			// Degradation criteria was met.
			if (logger.isDebugEnabled()) {
				logger.debug("## Degradation ##");
			}
			
			BioPAXEvent IAEffectorEvent = new BioPAXEvent();
			IAEffectorEvent.setEventType(Constants.IA_EFFECTOR);
			Map<String, Integer> inPartConMap = new HashMap<String, Integer>();
			inPartConMap.put(participantId, concentration);
			IAEffectorEvent.setInPartConMap(inPartConMap);
			Map<String, Integer> outPartConMap = new HashMap<String, Integer>();
			outPartConMap.put(participantId, -1);
			IAEffectorEvent.setOutPartConMap(outPartConMap);
			PWayMasterAgent.getPwayMaster().generateIAEffectorEvent(IAEffectorEvent);
		}	
		nonDegradables.clear();
	}
	
	
	/*
	 * Nothing will be consumed. The same inputs will be used for all interactions (SYNC).
	 * Firing a transition (interaction) has no impact on the other interactions. 
	 * if (controlType == activation) --> (1) all inputs must be 1, (2) all outputs must be 0, (3) degradation of output is disabled.
	 * if (controlType == inhibition) --> (1) all inputs must be 0, (2) all outputs must be 1.
	 */
	public boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		BioPAXInteactionType intType = event.getIntType();
    	switch (intType) {
    	case TmpRegs: 
    		return canTmpRegBePerformed(thisInt, event);
    	case TmpReacs:
    		return canTmpReacBePerformed(thisInt, event);
    	default:
    		break;	
		}
				
		return false;
	}
	
	private void performTmpRegInteraction(GenericAgent thisInt, BioPAXEvent event) {
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		String outPart = outPartConMap.keySet().iterator().next();
		outPartConMap.put(outPart, 1);
	}	
	
	private void performTmpReacInteraction(GenericAgent thisInt, BioPAXEvent event) {
		Collection<Control> controls = thisInt.getSensoryControls();
		Control control = controls.iterator().next();
		int outConcentration = 0;
		Enum_controlType ctrlType = control.getControlType();
		
		// This is about the concentration of the real output of this tmpReac.
		switch (ctrlType) {
		case ACTIVATION: 
			outConcentration = 1;
			break;
		case INHIBITION: 
			outConcentration = -1;
			break;
		default:
    		break;
		}
    	
    	Map<String, Integer> outPartConMap = event.getOutPartConMap();
		String outPart = outPartConMap.keySet().iterator().next();
		outPartConMap.put(outPart, outConcentration);
	}
	
	public void performInteraction(GenericAgent thisInt, BioPAXEvent event) {
		BioPAXInteactionType intType = event.getIntType();
    	switch (intType) {
    	case TmpRegs: 
    		performTmpRegInteraction(thisInt, event);
    		break;
    	case TmpReacs:	
    		performTmpReacInteraction(thisInt, event);
    		break;
    	default:
    		break;
    	}
	}	
}

