package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.Catalysis;
import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Conversion;
import nl.vu.bioinf.ebf.metamodel.Degradation;
import nl.vu.bioinf.ebf.metamodel.Enum_conversionDirection;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.Stoichiometry;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;

public abstract class ConvAgent extends IntAgent {
	private static Logger logger = Logger.getLogger(ConvAgent.class);
	
	protected Conversion conv = null;
	protected ArrayList<Catalysis> cats = null;
	protected ArrayList<CatAgent> catAgents = null;
	
	// We consider direction LEFT_TO_RIGHT if it is empty, REVERSIBLE, or LEFT_TO_RIGHT.
	private boolean isDirectionLeftToRight() {
		if (conv instanceof Degradation) {
			return true;
		}
		
		boolean result = true;
		Enum_conversionDirection conversionDirection = conv.getConversionDirection();
		if (conversionDirection == Enum_conversionDirection.RIGHT_TO_LEFT) {
			result = false;
		}
		return result;
	}
	
	/*
	 * Output:
	 * - empty map (<>) or (<Cmp_000006, 3.5>, <Prot_000002, 2.0>)
	 */
	public Map<String, Float> getStoichiometryMap() {
		Map<String, Float> stoichiometryMap = new HashMap<String, Float>();
		
		ArrayList<Stoichiometry> partStoichiometries = conv.getParticipantStoichiometry();
		if (partStoichiometries == null) {
			return stoichiometryMap; 
		}
		
		for (Stoichiometry partStoichiometry : partStoichiometries) {
			stoichiometryMap.put(partStoichiometry.getPhysicalEntity().getId(), partStoichiometry.getStoichiometricCoefficient());
		}
		
		return stoichiometryMap;
	}
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Convs;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();
		ArrayList<PhysicalEntity> pIns = null;
		if (isDirectionLeftToRight()) {
			pIns = conv.getLeft();
		}
		else {
			pIns = conv.getRight();
		}
		
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		ArrayList<PhysicalEntity> pOuts = null;
		if (isDirectionLeftToRight()) {
			pOuts = conv.getRight();
		}
		else {
			pOuts = conv.getLeft();
		}
		
		if (pOuts != null) {
			for (PhysicalEntity physicalEntity : pOuts) {
				outputParticipants.add(physicalEntity.getId());
			}
		}
		
		return outputParticipants;
	}
	
	public Collection<String> getSensoryControlParticipants() {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		// Add Catalysis which controls this conversion.
		if (cats != null) {
			for (Catalysis cat : cats) {
				controlParticipants.add(cat.getId());
			}
		}
		
		// Add Cotrol which controls this conversion.
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.add(ctrl.getId());
			}
		}
		
		return controlParticipants;
	}
	
	public boolean hasSensoryControls() {
		return ctrls != null || cats != null;
	}
	
	public Collection<Control> getSensoryControls() {
		Collection<Control> sensoryControls = new ArrayList<Control>();
		
		if (ctrls != null) {
			sensoryControls.addAll(ctrls);
		}
		
		if (cats != null) {
			sensoryControls.addAll(cats);
		}
		
		return sensoryControls;
	}
	
	public Collection<CtrlAgent> getSensoryControlAgents() {
		Collection<CtrlAgent> sensoryControlAgents = new ArrayList<CtrlAgent>();
		
		if (catAgents != null) {
			sensoryControlAgents.addAll(catAgents);
		}
		
		if (ctrlAgents != null) {
			sensoryControlAgents.addAll(ctrlAgents);
		}
		
		return sensoryControlAgents;
	}
	
	public Collection<String> getEffectorControlParticipants(BioPAXEvent event) {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		if (cats != null) {
			for (Catalysis cat : cats) {
				controlParticipants.addAll(getEffectorControlParticipants(event, cat, cat.getController()));
			}
		}
		
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.addAll(getEffectorControlParticipants(event, ctrl, ctrl.getController_PhysicalEntity()));
			}
		}
		
		return controlParticipants;
	}
}

