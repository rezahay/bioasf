package nl.vu.bioinf.ebf.metamodel.util;

import java.util.Collections;
import java.util.List;

public class CollectionUtils {
	public static List<String> checkStringList(List<String> list) {
	    return list == null ? Collections.EMPTY_LIST : list;
	}
	
	public static List<Object> checkList(List<Object> list) {
	    return list == null ? Collections.EMPTY_LIST : list;
	}
}
