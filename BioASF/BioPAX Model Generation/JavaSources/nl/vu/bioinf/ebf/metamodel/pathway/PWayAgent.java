package nl.vu.bioinf.ebf.metamodel.pathway;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import nl.vu.bioinf.ebf.metamodel.Pathway;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.interaction.CtrlAgent;
import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;
//import nl.vu.bioinf.ebf.model.pways.PWayA_Master;

import org.apache.log4j.Logger;

public abstract class PWayAgent extends Observable implements Observer {
	private static Logger logger = Logger.getLogger(PWayAgent.class);
	
	// All pathways share this local environment.
	private static InteractionBioEnv interactionBioEnv = InteractionBioEnv.getInstance();
	
	private boolean thisAgentStarted = false;
	
	// It will be used to send the result of the execution of this pathway-agent to its parent.
	protected PWayStatEvent pwayStatEvent = null;
	
	protected Pathway pway = null;
    protected ArrayList<PWayAgent> subPWayAgents = null;
    protected ArrayList<GenericAgent> subIntAgents = null;
    
    /*
     * Suppose the following example in which input of TmpReac_000001 is TmpReg_000001 and TmpReg_000001's input is Ctrl_000001:
     * 
     * {IAEffector, Ctrl_000001, Ctrls, true, [Prot_000007=-1], [Ctrl_000001=1]}
     * {IAEffector, TmpReg_000001, TmpRegs, true, [Ctrl_000001=-1,Cmp_000006=-1], [TmpReg_000001=1]}
     * {IAEffector, TmpReac_000001, TmpReacs, true, [TmpReg_000001=-1,DnaReg_000001=0], [Prot_000010=1]}
     * 
     * Before sending the IAEffector of TmpReac_000001 to the analyser, we have to replace TmpReg_000001=-1 with
     * Prot_000007=-1 (which is the replacement of Ctrl_000001=-1) and Cmp_000006=1.
     */
    private void filterCtrlInputIds(Map<String, Integer> inPartConMap) {
    	Map<String, Integer> tmpInPartConMap = new HashMap<String, Integer>(inPartConMap);
    	Map<String, Map<String, Integer>> ctrlIdToInPartConMap = PWayMasterAgent.getCtrlIdToInPartConMap();
    	Collection<String> inParts = tmpInPartConMap.keySet();
    	boolean ctrlIdFound = false;
    	for (String inPart : inParts) {
    		if (ctrlIdToInPartConMap.containsKey(inPart)) {
    			ctrlIdFound = true;
    			inPartConMap.remove(inPart);
    			Map<String, Integer> ctrlInPartConMap = ctrlIdToInPartConMap.get(inPart);
    			inPartConMap.putAll(ctrlInPartConMap);
    		}
    	}
    	if (ctrlIdFound) {
    		filterCtrlInputIds(inPartConMap);
    	}
    }
    
	private void handleEAEffectorEvent(BioPAXEvent EAEffectorEvent) {
   		// Only the master pathway must fill the local env. after receiving an EAEffectorEvent.
   		if (this instanceof PWayMasterAgent) {
   			interactionBioEnv.populateInteractionEnv(EAEffectorEvent);
   		}
   		
   		// EAEffector is propagated to all sub-pathways.
    	if (subPWayAgents != null) {
    		for (Iterator<PWayAgent> iterator = subPWayAgents.iterator(); iterator.hasNext();) {
    			PWayAgent pwayChild = iterator.next();
    			pwayChild.handleEAEffectorEvent(EAEffectorEvent);
    		}
    	}
    	
    	// EAEffector is propagated to all non-control interactions of this pathway if the pathway has interaction agents.
    	if (subIntAgents != null) {
	    	Collections.shuffle(subIntAgents);
	    	for (Iterator<GenericAgent> iterator = subIntAgents.iterator(); iterator.hasNext();) {
	    		GenericAgent intChild = iterator.next();
	    		if (intChild instanceof CtrlAgent) {
	    			// Each interaction is responsible for propagating EAEffector to its controls.
				}
	    		else {
	    			BioPAXEvent IAEffectorEvent = intChild.handleEAEffectorEvent(EAEffectorEvent);
	    	    	if (IAEffectorEvent.isIntPerformed()) {
	    	    		if (logger.isDebugEnabled()) {
	        				logger.debug(IAEffectorEvent.toString());
	        			}
	    	    		EAEffectorEvent = interactionBioEnv.consInteractionEAEffector(IAEffectorEvent);
	    	    		
	    	    		if (intChild.hasSensoryControls()) {
	    	    			filterCtrlInputIds(IAEffectorEvent.getInPartConMap());
		    	    		if (logger.isDebugEnabled()) {
		    	        		logger.debug("##Filtered IAEffectorEvent: " + IAEffectorEvent.toString());
		    	        	}
	    	    		}	
	    	    		
	    	    		// The IAEffectorEvent of only non-control interactions is reported to EA.
	    	    		PWayMasterAgent.getPwayMaster().generateIAEffectorEvent(IAEffectorEvent);
	    	    	}
	    		}
	   		}
    	}	
    	
   		// Here we are sure that we have traversed all interactions and we have collected statistics. Based on these statistics
   		// we inform the other parties (e.g., analyser) whether or not (nonControl) interactions have been performed.
   		if (this instanceof PWayMasterAgent) {
   			PWayMasterAgent.getCtrlIdToInPartConMap().clear();
   			((PWayMasterAgent)this).performDegradations(EAEffectorEvent);
   			((PWayMasterAgent)this).generatePWayAAStatEvent();
   		}
   	}
   	
	public String getAgentId() {
		return pway.getId();
	}
	

    private void startChilderen() {
    	if (subPWayAgents == null) {
    		if (logger.isDebugEnabled()) {
    			logger.debug("This agent '" + this.getAgentId() + "' has no pathway components.");
    		}	
    	}
    	else {
    		for (Iterator<PWayAgent> iterator = subPWayAgents.iterator(); iterator.hasNext();) {
    			PWayAgent child = iterator.next();
    			if (logger.isDebugEnabled()) {
    				logger.debug("Creating and starting childPW '" + child.getAgentId() +"' of '" + this.getAgentId() + "'.");
    			}	
    			child.start(this);
    		}
    	}
    	
    	
    	if (subIntAgents == null) {
    		if (logger.isDebugEnabled()) {
    			logger.debug("This agent '" + this.getAgentId() + "' has no interaction components. Strange!!!");
    		}	
    	}
    	else {
	    	for (Iterator<GenericAgent> iterator = subIntAgents.iterator(); iterator.hasNext();) {
	    		GenericAgent child = iterator.next();
	    		if (logger.isDebugEnabled()) {
	    			logger.debug("Creating and starting childIA '" + child.getAgentId() +"' of '" + this.getAgentId() + "'.");
	    		}	
				child.start(this);
			}
    	}	
    }
	
	protected void initAgent() {
		 pwayStatEvent = new PWayStatEvent();
		 pwayStatEvent.setPwayAgent(this);
			
		 if (logger.isDebugEnabled()) {
		   logger.debug("Initializing pathway agent: agentId='" + this.getAgentId() +  "'."); 
	     }
		 startChilderen();
    }
	
    private void logStatistics(IntStatEvent intStatEvent) {
    	if (logger.isDebugEnabled()) {
            logger.debug("PWayAgent '" + this.getAgentId() +  "' received event from its intr-agent: " + intStatEvent.toString());
		}
    }
    
    private void logStatistics(PWayStatEvent pwayStatEvent) {
    	if (logger.isDebugEnabled()) {
            logger.debug("PWayAgent '" + this.getAgentId() +  "' received event from its subpway-agent: " + pwayStatEvent.toString());
		}
    }
    
    private void handleIntAgentEvent(IntStatEvent intStatEvent) {
    	logStatistics(intStatEvent);
    	
    	pwayStatEvent.setIntPerformed(intStatEvent.isIntPerformed()); 
    	pwayStatEvent.setIntType(intStatEvent.getIntType());
    	
    	setChanged();
  		notifyObservers(pwayStatEvent);
    }
    
    private void handlePWayAgentEvent(PWayStatEvent pwayAgentEvent) {
    	logStatistics(pwayAgentEvent);
    	
    	pwayStatEvent.setIntPerformed(pwayAgentEvent.isIntPerformed());
    	pwayStatEvent.setIntType(pwayAgentEvent.getIntType());
    	
    	if (this instanceof PWayMasterAgent) {
			((PWayMasterAgent)this).handleStatistics();
		} 
    	else {
	    	setChanged();
	  		notifyObservers(pwayStatEvent);
    	}	
    }
    
    public void update(Observable observable, Object event) {
  		if (event == null) {
      		logger.warn("PWay: event should not be Null.");
      		return;
      	}
  		
  		try {
	  		if (event instanceof IntStatEvent) {
	  			handleIntAgentEvent((IntStatEvent)event);
	  		}
	  		else if (event instanceof PWayStatEvent) {
	  			handlePWayAgentEvent((PWayStatEvent)event);
	  		}
	  		else {
				BioPAXEvent biopaxEvent = (BioPAXEvent)event;
				String eventType = biopaxEvent.getEventType();
				switch (eventType) {
					case Constants.EA_EFFECTOR:
						handleEAEffectorEvent(biopaxEvent);
						break;
					case Constants.AA_PWAY_INST_STOP:
						if (this instanceof PWayMasterAgent) {
				    		((PWayMasterAgent)this).handleAAPWayInstEvent(biopaxEvent);
						}	
						break;		
					default:
						logger.warn("PWay cannot handle this event: " + biopaxEvent.getEventType());
						break;
				}
	  		}
  		}
  		catch (Throwable t) {
  			logger.error("@@@@@@@@@@@@@@@@ Unexpected error in PWayAgent Update: ", t);
  		}
  	}
    
    public void start(PWayAgent parentPWA) {
    	// We don't want to start an agent again which has already been started.
    	if (thisAgentStarted) {
    		addObserver(parentPWA);
    		return;
    	}
    	else {
    		thisAgentStarted = true;
    	}
    	
    	this.initAgent();
    	try {
    		addObserver(parentPWA);
    	}
    	catch (Exception e) {
    		if (logger.isDebugEnabled()) {
    			logger.debug("This is probably the MasterPWA which has no parent."); 
    		}	
		}
    	
    	// Only the master pway must subscribe itself to the incoming EAEffectors. The master propagates them to all pathways.
    	if (this instanceof PWayMasterAgent) {
    		((PWayMasterAgent)this).initAMQCommunication();
    	}
    }
}

