package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Dna;
import nl.vu.bioinf.ebf.metamodel.DnaRegion;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.Protein;
import nl.vu.bioinf.ebf.metamodel.Rna;
import nl.vu.bioinf.ebf.metamodel.RnaRegion;
import nl.vu.bioinf.ebf.metamodel.TemplateReaction;
import nl.vu.bioinf.ebf.metamodel.TemplateReactionRegulation;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class TmpReacAgent extends IntAgent {
	private static Logger logger = Logger.getLogger(TmpReacAgent.class);
	
	protected TemplateReaction tmpReac = null;
	protected ArrayList<TemplateReactionRegulation> tmpRegs = null;
	protected ArrayList<TmpRegAgent> tmpRegAgents = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.TmpReacs;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();
		
		// There is a OR-restriction on the "template" property. Only one of them is allowed.
		RnaRegion tmpRnaRegion = tmpReac.getTemplate_RnaRegion();
		if (tmpRnaRegion != null) {
			inputParticipants.add(tmpRnaRegion.getId());
		}
		else {
			Rna tmpRna = tmpReac.getTemplate_Rna();
			if (tmpRna != null) {
				inputParticipants.add(tmpRna.getId());
			}
			else {
				DnaRegion tmpDnaRegion = tmpReac.getTemplate_DnaRegion();
				if (tmpDnaRegion != null) {
					inputParticipants.add(tmpDnaRegion.getId());
				}
				else {
					Dna tmpDna = tmpReac.getTemplate_Dna();
					if (tmpDna != null) {
						inputParticipants.add(tmpDna.getId());
					}
				}	
			}
		}	
		
		// What to do with ArrayList<PhysicalEntity> participant_TemplateReaction? For the time being, we consider it as part of input.
		ArrayList<PhysicalEntity> pIns = tmpReac.getParticipant_TemplateReaction();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		
		// There is a OR-restriction on the "product" property. Only one of them is allowed.
		ArrayList<Rna> rnaProducts = tmpReac.getProduct_Rna(); 
		if (rnaProducts != null) {
			for (Rna rnaProduct : rnaProducts) {
				outputParticipants.add(rnaProduct.getId());
			}
		}
		else {
			ArrayList<Dna> dnaProducts = tmpReac.getProduct_Dna(); 
			if (dnaProducts != null) {
				for (Dna dnaProduct : dnaProducts) {
					outputParticipants.add(dnaProduct.getId());
				}
			}
			else {
				ArrayList<Protein> protProducts = tmpReac.getProduct_Protein(); 
				if (protProducts != null) {
					for (Protein protProduct : protProducts) {
						outputParticipants.add(protProduct.getId());
					}
				}
			}
		}	
		
		return outputParticipants;
	}
	
	public Collection<String> getSensoryControlParticipants() {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		// Add TemplateReactionRegulation which controls this TemplateReaction.
		if (tmpRegs != null) {
			for (TemplateReactionRegulation tmpReg : tmpRegs) {
				controlParticipants.add(tmpReg.getId());
			}
		}
		
		// Add Control interaction which controls this generic interaction.
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.add(ctrl.getId());
			}
		}
		
		return controlParticipants;
	}
	
	public boolean hasSensoryControls() {
		return ctrls != null || tmpRegs != null;
	}
	
	public Collection<Control> getSensoryControls() {
		Collection<Control> sensoryControls = new ArrayList<Control>();
		
		if (ctrls != null) {
			sensoryControls.addAll(ctrls);
		}
		
		if (tmpRegs != null) {
			sensoryControls.addAll(tmpRegs);
		}
		
		return sensoryControls;
	}
	
	public Collection<CtrlAgent> getSensoryControlAgents() {
		Collection<CtrlAgent> sensoryControlAgents = new ArrayList<CtrlAgent>();
		
		if (tmpRegAgents != null) {
			sensoryControlAgents.addAll(tmpRegAgents);
		}
		
		if (ctrlAgents != null) {
			sensoryControlAgents.addAll(ctrlAgents);
		}
		
		return sensoryControlAgents;
	}
	
	public Collection<String> getEffectorControlParticipants(BioPAXEvent event) {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		if (tmpRegs != null) {
			for (TemplateReactionRegulation tmpReg : tmpRegs) {
				controlParticipants.addAll(getEffectorControlParticipants(event, tmpReg, tmpReg.getController()));
			}
		}
		
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.addAll(getEffectorControlParticipants(event, ctrl, ctrl.getController_PhysicalEntity()));
			}
		}
		
		return controlParticipants;
	}
}

