package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.TemplateReactionRegulation;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class TmpRegAgent extends CtrlAgent {
	private static Logger logger = Logger.getLogger(TmpRegAgent.class);
	
	protected TemplateReactionRegulation tmpReg = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.TmpRegs;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();		
		ArrayList<PhysicalEntity> pIns = tmpReg.getController();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		ctrl = tmpReg;
		return super.getOutputParticipants();
	}
	
}

