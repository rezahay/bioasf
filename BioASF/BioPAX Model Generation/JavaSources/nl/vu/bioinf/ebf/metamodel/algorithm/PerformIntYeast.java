package nl.vu.bioinf.ebf.metamodel.algorithm;

import java.util.Collection;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.Enum_controlType;
import nl.vu.bioinf.ebf.metamodel.TemplateReaction;
import nl.vu.bioinf.ebf.metamodel.TemplateReactionRegulation;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;

import org.apache.log4j.Logger;

public class PerformIntYeast implements PerformIntAlgorithm {
	private static Logger logger = Logger.getLogger(PerformIntYeast.class);
	
	public void performDegradations(BioPAXEvent event) {
	}
	
	public boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event) {
		return true;
	}
	
	/*
	 * effectorPartcipantId of a ControlInt = id of itself
	 * effectorConcentration of a ControlInt is sum of the following calculation for all controllers:
	 *   if controlType == ACTIVATION { 
	 *     return (+1) x (sum of concentration of all its activation-controllers)
	 *   }  
	 *   if controlType == INHIBITION { 
	 *     return (-1) x (sum of concentration of all its inhibition-controllers)
	 *   }
	 */
	public void performTmpRegInteraction(GenericAgent thisInt, BioPAXEvent event) {
		TemplateReactionRegulation controlledInt = (TemplateReactionRegulation)thisInt.getControlledInt();
		Enum_controlType ctrlType = controlledInt.getControlType();
		int impactFactor = 1;
		switch (ctrlType) {
		case ACTIVATION: 
			break;
		case INHIBITION: 
			impactFactor = -1;
			break;
		default:
    		break;
		}
		
		int sumOfInConcentrations = 0;
		Collection<Integer> inConcentrations = event.getInPartConMap().values();
		for (int inConcentration : inConcentrations) {
			sumOfInConcentrations = sumOfInConcentrations + inConcentration;
		}
		sumOfInConcentrations = sumOfInConcentrations * impactFactor;
		
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		String outPart = outPartConMap.keySet().iterator().next();
		outPartConMap.put(outPart, sumOfInConcentrations);
	}
	
	/*
	 * effectorPartcipantId of a NonControlInt = id of its product
	 * effectorConcentration of a NonControlInt = 0 or 1 depending on sum of sensoryConcentrations (both 
	 * activation and inhibition controls) and the value of Theta:
	 * -- sumOfControlInts > Theta ==> effectorConcentration = 0
	 * -- sumOfControlInts < Theta ==> effectorConcentration = 1
	 * -- sumOfControlInts == Theta ==> effectorConcentration = previous effectorConcentration
	 * 
	 * Theta(all-NonControlInts) = 0  except for Theta(Cdc2/Cdc13*) = 1 and Theta(Cdc2/Cdc13) = -1
	 */
	public void performTmpReacInteraction(GenericAgent thisInt, BioPAXEvent event) {
		int theta = 0;
		TemplateReaction tmpReac = (TemplateReaction)thisInt.getControlledInt();
		String geneName = tmpReac.getRdfId();
		if (geneName.endsWith("Cdc2Cdc13Star")) {
			theta = 1;
		}
		else if (geneName.endsWith("Cdc2Cdc13")) {
			theta = -1;
		}
		int sumOfInConcentrations = 0;
		int outConcentration = 0;
		Collection<Integer> inConcentrations = event.getInPartConMap().values();
		for (int inConcentration : inConcentrations) {
			sumOfInConcentrations = sumOfInConcentrations + inConcentration;
		}
		
		if (sumOfInConcentrations > theta) {
			outConcentration = 0;
		}
		else if (sumOfInConcentrations < theta) {
			outConcentration = 1;
		}
		else if (sumOfInConcentrations == theta) {
			// get previous concentration of the product of this tmpReac interaction.
			String productId = tmpReac.getProduct_Protein().get(0).getId();
			outConcentration = event.getOutConcentation(productId);
		}
		
		Map<String, Integer> outPartConMap = event.getOutPartConMap();
		String outPart = outPartConMap.keySet().iterator().next();
		outPartConMap.put(outPart, outConcentration);
	}
	
	/*
	 * inParticipants == filled or empty but never null
	 * inConcentrations = filled or empty but never null
	 * outParticipants == filled or empty but never null
	 * outConcentrations == filled or empty but never null
	 * sctrls == tmpRegs
	 */
	public void performInteraction(GenericAgent thisInt, BioPAXEvent event) {
		BioPAXInteactionType intType = event.getIntType();
    	switch (intType) {
    	case TmpRegs: 
    		performTmpRegInteraction(thisInt, event);
    		break;
    	case TmpReacs:	
    		performTmpReacInteraction(thisInt, event);
    		break;
    	default:
    		break;
    	}	
	}
}

