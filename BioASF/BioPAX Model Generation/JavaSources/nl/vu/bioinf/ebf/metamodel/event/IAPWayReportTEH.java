package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class IAPWayReportTEH extends BioPAXTopicEventHandler {
	private static Logger logger = Logger.getLogger(IAPWayReportTEH.class);

    public IAPWayReportTEH() {   
    	super.init(Constants.IA_PWAY_REPORT_TOPIC);
    }
}

