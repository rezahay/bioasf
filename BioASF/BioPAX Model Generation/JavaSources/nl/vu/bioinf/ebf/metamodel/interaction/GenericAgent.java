package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Interaction;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.pathway.IntStatEvent;
import nl.vu.bioinf.ebf.metamodel.pathway.InteractionBioEnv;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayAgent;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayMasterAgent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public abstract class GenericAgent extends Observable implements Observer {
	private static Logger logger = Logger.getLogger(GenericAgent.class);
	
	//protected PerformIntAlgorithm performIntAlgorithm = null;
	
	// All control interactions share this local environment.
	private static InteractionBioEnv interactionBioEnv = InteractionBioEnv.getInstance();
	
	private boolean thisAgentStarted = false;
	
	// The value of these attributes must be set by the specific child-instance (individual) of this class.
	private BioPAXInteactionType intType = null;
	private Collection<String> sensoryParticipants = null;
	private Collection<String> outputParticipants = null;
	
	private Collection<CtrlAgent> sensoryControlAgents = null;
	
	protected Interaction interaction = null;
	
	// It will be used to send the result of the execution of this interaction-agent to its parent pathway-agent.
	private IntStatEvent intStatEvent = null;
	
	public abstract BioPAXInteactionType getBioPAXIntType();
	
	// It only returns real inputs.
	public abstract Collection<String> getInputParticipants();
	public abstract Collection<String> getOutputParticipants();
	
	// It only returns controls used as a sensor for the interaction.
	public abstract Collection<String> getSensoryControlParticipants();
	
	// It returns a collection containing both real inputs and controls.
	public abstract Collection<String> getSensoryParticipants();
	
	public abstract Collection<Control> getSensoryControls();
	public abstract boolean hasSensoryControls();
	public abstract Collection<CtrlAgent> getSensoryControlAgents();
	protected abstract boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event);
	protected abstract void performInteraction(GenericAgent thisInt, BioPAXEvent event);
	
	public String getAgentId() {
		return interaction.getId();
	}
	
	public BioPAXInteactionType getIntType() {
		return intType;
	}
	
	public Interaction getControlledInt() {
		return interaction;
	}
	
	private String printParticipantIds(Collection<String> participantIds) {
		if (participantIds == null || participantIds.isEmpty()) {
			return Constants.EMPTY_PART_CON_LIST_STR;
		}	
		StringBuffer result = new StringBuffer();
		result.append("[");
		for (String participantId : participantIds) {
			result.append(participantId + ",");
		}
		result.setLength(result.length() - 1);
		result.append("]");
		return result.toString();
	}
	
    protected void initAgent() {
    	intType = getBioPAXIntType();
    	
    	sensoryParticipants = getSensoryParticipants();
    	outputParticipants = getOutputParticipants();
    	sensoryControlAgents = getSensoryControlAgents();
    	
    	intStatEvent = new IntStatEvent();
    	intStatEvent.setIntAgent(this);
    	
    	if (logger.isDebugEnabled()) {
    		logger.debug("Initializing agent: agentId='" + this.getAgentId() + "', intType: '" + intType + 
    				    "', sensoryParticipants='" + printParticipantIds(sensoryParticipants) + 
    				    "', outputParticipants='" + printParticipantIds(outputParticipants) +
    				    "'.");
    	}
    }
    
    /*
     * Inform parent pathways about the success or failure of this IA.
     */
    private void generateIntStatEvent(boolean intPerformed) {
    	intStatEvent.setIntPerformed(intPerformed);
    	intStatEvent.setIntType(intType);
    	setChanged();
  		notifyObservers(intStatEvent);
    }
    
    private void saveCtrlControllers(BioPAXEvent IAEffectorEvent) {
    	Map<String, Map<String, Integer>> ctrlIdToInPartConMap = PWayMasterAgent.getCtrlIdToInPartConMap();
    	ctrlIdToInPartConMap.put(IAEffectorEvent.getAgentId(), IAEffectorEvent.getInPartConMap());
    }
    
    /*
     * As EA does not know about Ins and Outs of each IA, we have to split one-list to two distinct lists. So, we must split
     * inParticipantIds to inParticipantIds and outParticipantIds. We must also split inConcentrations to inConcentrations and
     * outConcentrations.
     */
    private BioPAXEvent SplitInsFromOuts(BioPAXEvent EAEffectorEvent) {
    	Map<String, Integer> envPartConMap = EAEffectorEvent.getInPartConMap();
    	Map<String, Integer> agentInPartConMap = new HashMap<String, Integer>();
    	for (String sensoryParticipant : sensoryParticipants) {
    		int inConcentration = 0;
    		if (envPartConMap.containsKey(sensoryParticipant)) {
    			inConcentration = envPartConMap.get(sensoryParticipant);
    		}	
    		agentInPartConMap.put(sensoryParticipant, inConcentration);
    	}
    	
    	Map<String, Integer> agentOutPartConMap = new HashMap<String, Integer>();
    	for (String outputParticipant : outputParticipants) {
    		int outConcentration = 0;
    		if (envPartConMap.containsKey(outputParticipant)) {
    			outConcentration = envPartConMap.get(outputParticipant);
    		}	
    		agentOutPartConMap.put(outputParticipant, outConcentration);
    	}
    	
    	BioPAXEvent IAEffectorEvent = new BioPAXEvent();
    	IAEffectorEvent.setEventType(Constants.IA_EFFECTOR);
    	IAEffectorEvent.setAgentId(this.getAgentId());
    	IAEffectorEvent.setIntType(intType);
    	IAEffectorEvent.setInPartConMap(agentInPartConMap);
    	IAEffectorEvent.setOutPartConMap(agentOutPartConMap);
    	
    	if (logger.isDebugEnabled()) {
    		logger.debug("##Split result: " + IAEffectorEvent.toString());
    	}	
    	
    	return IAEffectorEvent;
    }
    
    public BioPAXEvent handleEAEffectorEvent(BioPAXEvent EAEffectorEvent) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("$$Agent '" + getAgentId() + "' received EAEffector: " + EAEffectorEvent.toString());
    	}	
    	
    	Collections.shuffle((List<?>) sensoryControlAgents);
    	for (CtrlAgent sensoryControlAgent : sensoryControlAgents) {
    		BioPAXEvent IAEffectorEvent = sensoryControlAgent.handleEAEffectorEvent(EAEffectorEvent);
    		if (IAEffectorEvent.isIntPerformed()) {
    			if (logger.isDebugEnabled()) {
    				logger.debug(IAEffectorEvent.toString());
    			}	
    			saveCtrlControllers(IAEffectorEvent);
	    		EAEffectorEvent = interactionBioEnv.consInteractionEAEffector(IAEffectorEvent);
    		}
    	}
    	
    	BioPAXEvent IAEffectorEvent = SplitInsFromOuts(EAEffectorEvent);
    	boolean intCanBePerformed = canIntBePerformed(this, IAEffectorEvent);
    	if (intCanBePerformed) {
    		if (logger.isDebugEnabled()) {
    			logger.debug("##Interaction performed by agent: '" + this.getAgentId() + "'.");
    		}	
    		
    		// The result of performInteraction, which contains changes in the concentrations of inputs and outputs, is included
    		// in the IAEffectorEvent. So, IAEffectorEvent is a mutable parameter. Note that after performing an interaction, the
    		// inPartConMap and outPartConMap contain delta-concentrations (e.g., +2 or -3). The environment will be the adjusted
    		// by these values.
    		performInteraction(this, IAEffectorEvent);
    	}
    	else {
    		if (logger.isDebugEnabled()) {
    			logger.debug("##Interaction cannot be performed by agent: '" + this.getAgentId() + "'.");
    		}	
    	}
    	IAEffectorEvent.setIntPerformed(intCanBePerformed);
    	
    	generateIntStatEvent(intCanBePerformed);
    	
    	return IAEffectorEvent;
    }
    
    public void update(Observable observable, Object event) {
		if (event == null) {
    		logger.warn("IA: EA Event should not be Null.");
    		return;
    	}
	}

    public void start(PWayAgent parentPWA) {
    	// We don't want to start an agent again which has already been started.
    	if (thisAgentStarted) {
    		addObserver(parentPWA);
    		return;
    	}
    	else {
    		thisAgentStarted = true;
    	}
    	
    	this.initAgent();
    	try {
    		addObserver(parentPWA);
    	}
    	catch (Exception e) {
    		logger.info("Apparently, interaction agent '" + this.getAgentId() + "' does not have any parent.");
		}
    }
}
