package nl.vu.bioinf.ebf.metamodel.event;

import javax.jms.DeliveryMode;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

public abstract class BioPAXTopicEventHandler {
	private static Logger logger = Logger.getLogger(BioPAXTopicEventHandler.class);

    private static final long SLEEP_TIME = 100;
    private static final boolean TRANSACTED = false;
    private static final boolean PERSISTENT = false;
	
    // Should not be static for Local broker (VM).
	private TopicConnection connection = null;
	
	private TopicSession session = null;
    private Topic topic = null;

    protected void init(String destination) {
        try {
        	if (logger.isDebugEnabled()) {
        		logger.debug("Connecting to URL from BioPAXTopicEventHandler: " + Constants.BROKER_URL);
        	}	
        	
            // Create a shared connection.
            ActiveMQConnectionFactory connectionFactory = 
            		new ActiveMQConnectionFactory(Constants.BROKER_USER, Constants.BROKER_PASSWORD, Constants.BROKER_URL);
            if (connection == null) {
            	connection = connectionFactory.createTopicConnection();
            	connection.start();
            	if (logger.isDebugEnabled()) {
            		logger.debug("connection created for: " + destination);
            	}	
            }	

            // Create a session per thread.
            session = connection.createTopicSession(TRANSACTED, Session.AUTO_ACKNOWLEDGE);
            topic = session.createTopic(destination);
        } 
        catch (Exception e) {
            logger.error("Exception durinig init(): ", e);
        } 
    }   
    
    public TopicPublisher createPublisher() {
    	TopicPublisher publisher = null;
    	try {
    		publisher = session.createPublisher(topic);

            if (PERSISTENT) {
                publisher.setDeliveryMode(DeliveryMode.PERSISTENT);
            } 
            else {
                publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            }
    	}
    	catch (Throwable t) {
            logger.error("Failed during createPublisher: ", t);
        } 
    	return publisher;
    }
    
    public void publish(BioPAXEvent event, TopicPublisher publisher) {
    	try {
    		TextMessage message = null;
            message = session.createTextMessage();
            message.setStringProperty(Constants.EVENT_TYPE, event.getEventType());
            message.setStringProperty(Constants.AGENT_ID, event.getAgentId());
            message.setStringProperty(Constants.INT_TYPE, event.getIntTypeAsString());
            message.setBooleanProperty(Constants.INT_PERFORMED, event.isIntPerformed());
            /*
            message.setStringProperty(Constants.IN_PARTICIPANT_IDS, event.getInParticipantIdsAsString());
            message.setStringProperty(Constants.IN_CONCENTRATIONS, event.getInConcentrationsAsString());
            message.setStringProperty(Constants.OUT_PARTICIPANT_IDS, event.getOutParticipantIdsAsString());
            message.setStringProperty(Constants.OUT_CONCENTRATIONS, event.getOutConcentrationsAsString());
            */
            message.setStringProperty(Constants.IN_PART_CON_MAP, event.getInPartConMapAsString());
            message.setStringProperty(Constants.OUT_PART_CON_MAP, event.getOutPartConMapAsString());
            
            if (logger.isDebugEnabled()) {
            	logger.debug("Sending event: " + event.toString());
            }	
            
            publisher.publish(message);

            Thread.sleep(SLEEP_TIME);
            
        } 
    	catch (Throwable t) {
            logger.error("Publish failed because: ", t);
        }
    }
    
    public void createSubscriber(MessageListener listner, String messageSelector) {
    	try {
    		TopicSubscriber subscriber = null;
    		if (messageSelector == null) {
    			subscriber = session.createSubscriber(topic);
    		}
    		else {	
    			subscriber = session.createSubscriber(topic, messageSelector, true);
    		}
    		subscriber.setMessageListener(listner);
    		if (logger.isDebugEnabled()) {
                 logger.debug("Listening (to Topic=" + topic.getTopicName() + ")\n");
             }
    	}
    	catch (Throwable t) {
            logger.error("Failed during createSubscriber: ", t);
        } 
    }

    
    public void closeAll() {
    	try {
	        if (session != null) {
	            session.close();
	        }
	        if (connection != null) {
	            connection.close();
	        }
    	}
        catch (Throwable ignore) {
        	logger.error("Exception during closing JMS session: ", ignore);
        }
    }
}

