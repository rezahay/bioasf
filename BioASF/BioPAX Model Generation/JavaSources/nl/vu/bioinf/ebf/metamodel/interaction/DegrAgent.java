package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Degradation;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class DegrAgent extends ConvAgent {
	private static Logger logger = Logger.getLogger(DegrAgent.class);
	
	protected Degradation degr = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Degrs;
	}
	
	public Collection<String> getInputParticipants() {
		conv = degr;
		return super.getInputParticipants();
	}
	
	public Collection<String> getOutputParticipants() {
		conv = degr;
		return super.getOutputParticipants();
	}
}

