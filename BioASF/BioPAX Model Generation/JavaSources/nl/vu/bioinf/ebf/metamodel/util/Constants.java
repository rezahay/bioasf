package nl.vu.bioinf.ebf.metamodel.util;

import org.apache.activemq.ActiveMQConnection;

public abstract class Constants {
	public static final boolean MULTI_PROCESS = false;
	public static final String BROKER_URL = MULTI_PROCESS? 
			ActiveMQConnection.DEFAULT_BROKER_URL : 
			"vm://localhost?broker.persistent=false&broker.deleteAllMessagesOnStartup=true&broker.useJmx=true";
	public static final String BROKER_USER = ActiveMQConnection.DEFAULT_USER;
	public static final String BROKER_PASSWORD = ActiveMQConnection.DEFAULT_PASSWORD;
	
	public static final String MAX_PARALLEL_STEP = "mps";
	public static final String SYNCHRONOUS_STEP = "sync";
	public static final String ASYNCHRONOUS_STEP = "async";
	
	public static final String PWAYA_MASTER_CLASS = "nl.vu.bioinf.ebf.model.pways.PWayA_Master";
	public static final String CONFIG_FILE = "ebf-config.properties";
	public static final String JAVA_SOURCE_DIR = "JavaSources";
	public static final String JAVA_RESOURCE_DIR = "JavaResources";
	public static final String EXPRESSION_FILE = "concentrations.txt";
	
	public static final String KEY_INPUT_FILE = "biopax_model";
	public static final String KEY_OUTPUT_DIR = "output_dir";
	public static final String KEY_INITIAL_CONCENTRATIONS = "initial_concentrations";
	public static final String KEY_EXPRESSION_CLASS = "expression_class";
	public static final String KEY_ALGORITHM_CLASS = "algorithm_class";
	public static final String KEY_STEP_TYPE = "step_type";
	
	// ============================ Event fields names =============================
	public static final String EVENT_TYPE = "eventType";
	public static final String INT_TYPE = "intType";
	public static final String AGENT_INST = "agentInst";
	public static final String AGENT_ID = "agentId";
	public static final String STAT_TYPE = "statType";
	public static final String INT_PERFORMED = "intPerformed";
	
	public static final String IN_PART_CON_MAP = "inPartConMap";
	public static final String OUT_PART_CON_MAP = "outPartConMap";

	// ============================ Queue and topic names =============================
    public static final String IA_EFFECTOR_QUEUE = "IAEffectorQueue";
    public static final String IA_REQUEST_QUEUE = "IARequestQueue";
    public static final String EA_RESPONAE_QUEUE = "EAResponseQueue";
    public static final String EA_EFFECTOR_TOPIC = "EAEffectorTopic";
    public static final String IA_PWAY_REPORT_TOPIC = "IAPWayReportTopic";
    public static final String AA_EA_INST_QUEUE = "AAEAInstQueue";
    public static final String AA_PWAY_INST_QUEUE = "AAPWayInstQueue";
    public static final String EA_AA_STAT_QUEUE = "EAAAStatQueue";
    public static final String PWAY_AA_STAT_QUEUE = "PWayAAStatQueue";
    
    // ============================ Event types ========================================
    public static final String IA_REQUEST = "IARequest";
    public static final String IA_EFFECTOR = "IAEffector";
    public static final String EA_EFFECTOR = "EAEffector";
    public static final String EA_RESPONAE = "EAResponse";
    public static final String EA_AA_STAT_CUR_ENTITIES = "CurrentEntities";
    public static final String EA_AA_STAT_CONS_ENTITIES = "ConsumedEntities";
    public static final String EA_AA_STAT_PROD_ENTITIES = "ProducedEntities";
    public static final String EA_AA_NO_MORE_ENV_CHANGE = "NoMoreEnvChange";
    public static final String PWAY_AA_NO_INT_PERFORMED = "NoIntPerformed";
    public static final String PWAY_AA_ALL_INTS_PERFORMED = "AllIntsPerformed";
    public static final String AA_PWAY_INST_STOP = "AAPWayStop";
    public static final String AA_EA_INST_STOP = "AAEAStop";
    public static final String MASTER_AGENT_ID = "PWayA_Master";
    public static final String ANALYSIS_AGENT_ID = "Analyser";
    
    public static final String NULL_PART_CON_LIST_STR = "??";
    public static final String EMPTY_PART_CON_LIST_STR = "[]";
    public static final String EMPTY_AGENT_STR = "?";
    public static final String EMPTY_STATTYPE_STR = "?";
    public static final String EMPTY_INTTYPE_STR = "?";
    public static final String EMPTY_AGENTINST_STR = "?";
    
    public static final int INVALID_STATE = -1;
    
    // How long (in milliseconds) should EA wait to the response of an agent (who has locked a pa) before going into timeout?    
    public static final long AGENT_FEEDBACK_TIME = 100;
    
    // How long (in milliseconds) should EA wait to send the next EAEffectorEvent?    
    public static final long EA_EFFECTOR_FREQUENCY_TIME = 1000;
}

