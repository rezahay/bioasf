package nl.vu.bioinf.ebf.metamodel.pathway;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;


public class InteractionBioEnv {
	private static Logger logger = Logger.getLogger(InteractionBioEnv.class);

	private static InteractionBioEnv interactionBioEnv = InteractionBioEnv.getInstance();
	
	// This map contains the concentration value for each participant.
	private static Map<String, Integer> concentrationStorage = new HashMap<String, Integer>();
	

	public static InteractionBioEnv getInstance() {
		if (interactionBioEnv == null) {
			interactionBioEnv = new InteractionBioEnv();
		}
		return interactionBioEnv;
	}

	private void updateConcentration(String participantId, int deltaConcentration) {
		int envConcentration = 0;
		if (concentrationStorage.containsKey(participantId)) {
			envConcentration = concentrationStorage.get(participantId);
		}
		
		envConcentration = envConcentration + deltaConcentration;
		concentrationStorage.put(participantId, new Integer(envConcentration));
	}
	
	/*
	 * Whenever the Master pathway receives an EAEffector, the values of the EAEffector must be populated in the local interaction 
	 * environment (concentrationStorage).
	 */
	void populateInteractionEnv(BioPAXEvent EAEffectorEvent) {
		// Whenever the Master pathway receives an EAEffector, the local interaction environment (concentrationStorage) and the integratedIAEffector 
		// must be reset.
		concentrationStorage.clear();
		Map<String, Integer> inPartConMap = EAEffectorEvent.getInPartConMap();
		concentrationStorage.putAll(inPartConMap);
	}

	private void updateInteractionEnvWithSensors(BioPAXEvent IAEffectorEvent) {
		Map<String, Integer> inPartConMap = IAEffectorEvent.getInPartConMap();
		Collection<String> sensoryParticipants = inPartConMap.keySet();
		for (String participantId : sensoryParticipants) {
			int inDeltaConcentration = inPartConMap.get(participantId);
			updateConcentration(participantId, inDeltaConcentration);
		}
	}

	private void updateInteractionEnvWithEffectors(BioPAXEvent IAEffectorEvent) {
		Map<String, Integer> outPartConMap = IAEffectorEvent.getOutPartConMap();
		Collection<String> effectorParticipants = outPartConMap.keySet();
		for (String participantId : effectorParticipants) {
			int outDeltaConcentration = outPartConMap.get(participantId);
			updateConcentration(participantId, outDeltaConcentration);
		}
	}
	
	/*
	 * All interactions have the same view to the environment, meaning:
	 * - execution of this interaction does not have any impact on other interactions,
	 * - no changes must occur in the environment (concentrationStorage) regarding the consumed (sensory) values,
	 * - no changes must occur in the environment (concentrationStorage) regarding the produced (effector) values,
	 * - the other interactions are going to use the values in the original EAEffector. 
	 * Exception:
	 * - Effector of control interactions (the id of a control interaction) must be used by normal interactions and therefore
	 *   they must be populated in the environment (concentrationStorage).
	 */
	private void updateInteractionEnvSynchronous(BioPAXEvent IAEffectorEvent) {
		BioPAXInteactionType intType = IAEffectorEvent.getIntType();
    	switch (intType) {
		case Ctrls: case TmpRegs: case Cats: case Mods: 
			updateInteractionEnvWithEffectors(IAEffectorEvent);
			break;	
		default:
			break;
		}
	}
	
	/*
	 * Each interaction has different view to the environment, meaning:
	 * - execution of this interaction impacts other interactions,
	 * - the sensory concentrations in the environment (concentrationStorage) must be decreased,
	 * - no changes must occur in the environment (concentrationStorage) regarding the produced (effector) values, 
	 * Exception:
	 * - Effector of control interactions (the id of a control interaction) must be used by normal interactions and therefore
	 *   they must be populated in the environment (concentrationStorage).
	 */
	private void updateInteractionEnvMaxParallel(BioPAXEvent IAEffectorEvent) {
		updateInteractionEnvWithSensors(IAEffectorEvent);
		
		BioPAXInteactionType intType = IAEffectorEvent.getIntType();
    	switch (intType) {
		case Ctrls: case TmpRegs: case Cats: case Mods: 
			updateInteractionEnvWithEffectors(IAEffectorEvent);
			break;	
		default:
			break;
		}
	}
	
	/*
	 * Each interaction has different view to the environment, meaning:
	 * - execution of this interaction impacts other interactions,
	 * - the sensory concentrations in the environment (concentrationStorage) must be decreased,
	 * - the effector concentrations in the environment (concentrationStorage) must be increased, 
	 */
	private void updateInteractionEnvAsynchronous(BioPAXEvent IAEffectorEvent) {
		updateInteractionEnvWithSensors(IAEffectorEvent);
		updateInteractionEnvWithEffectors(IAEffectorEvent);
	}
	
	/*
	 * Pre-condition: interaction has been performed.
	 */
	public BioPAXEvent consInteractionEAEffector(BioPAXEvent IAEffectorEvent) {
		String stepType = PWayMasterAgent.getStepType();
		switch (stepType) {
    	case Constants.SYNCHRONOUS_STEP:
    		updateInteractionEnvSynchronous(IAEffectorEvent);
    		break;
	   	case Constants.ASYNCHRONOUS_STEP:
	   		updateInteractionEnvAsynchronous(IAEffectorEvent);
			break;
	   	case Constants.MAX_PARALLEL_STEP:
	   		updateInteractionEnvMaxParallel(IAEffectorEvent);
			break;	
		}	
		
    	BioPAXEvent EAEffectorEvent = new BioPAXEvent();
    	EAEffectorEvent.setAgentId(null);
    	EAEffectorEvent.setIntType(null);
    	EAEffectorEvent.setIntPerformed(false);
    	EAEffectorEvent.setEventType(Constants.EA_EFFECTOR);
    	
    	Map<String, Integer> envInPartConMap = new HashMap<String, Integer>(concentrationStorage);
    	EAEffectorEvent.setInPartConMap(envInPartConMap);
    	
    	return EAEffectorEvent;
	}
}
