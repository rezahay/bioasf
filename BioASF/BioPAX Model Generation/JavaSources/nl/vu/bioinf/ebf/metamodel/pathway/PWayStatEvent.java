package nl.vu.bioinf.ebf.metamodel.pathway;

import org.apache.log4j.Logger;

public class PWayStatEvent  extends CommonStatEvent {
	private static Logger logger = Logger.getLogger(PWayStatEvent.class);
	
	private PWayAgent pwayAgent = null;
	
	public PWayAgent getPwayAgent() {
		return pwayAgent;
	}

	public void setPwayAgent(PWayAgent pwayAgent) {
		this.pwayAgent = pwayAgent;
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("PWayStatEvent {");
		result.append(pwayAgent.getAgentId());
		result.append(", ");
		result.append(intPerformed);
		result.append(", ");
		result.append(getIntTypeAsString());
		result.append("}");
		return result.toString();
	}
}

