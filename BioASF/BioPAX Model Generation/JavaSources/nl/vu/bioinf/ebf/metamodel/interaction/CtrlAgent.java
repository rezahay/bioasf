package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Pathway;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class CtrlAgent extends IntAgent {
	private static Logger logger = Logger.getLogger(CtrlAgent.class);
	
	protected Control ctrl = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Ctrls;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();		
		ArrayList<PhysicalEntity> pIns = ctrl.getController_PhysicalEntity();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		
		// Add pathway which acts as the controller of this generic control interaction.
		ArrayList<Pathway> controllerPways = ctrl.getController_Pathway();
		if (controllerPways != null) {
			for (Pathway pway : controllerPways) {
				inputParticipants.add(pway.getId());
			}
		}
		
		return inputParticipants;
	}
	
	/*
	 * outputParticipants == id of this control agent itself.
	 */
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		outputParticipants.add(ctrl.getId());
		
		return outputParticipants;
	}
	
}

