package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class EAResponseQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(EAResponseQEH.class);

    public EAResponseQEH() {   
    	super.init(Constants.EA_RESPONAE_QUEUE);
    }
}

