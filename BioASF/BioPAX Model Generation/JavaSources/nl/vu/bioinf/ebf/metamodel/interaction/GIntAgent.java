package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Gene;
import nl.vu.bioinf.ebf.metamodel.GeneticInteraction;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class GIntAgent extends IntAgent {
	private static Logger logger = Logger.getLogger(GIntAgent.class);
	
	protected GeneticInteraction gintr = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.GInts;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>(); 
		ArrayList<Gene> pIns = gintr.getParticipant_GeneticInteraction();
		
		if (pIns != null) {
			for (Gene gene : pIns) {
				inputParticipants.add(gene.getId());
			}
		}
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		return outputParticipants;
	}
	
}

