package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Transport;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class TransAgent extends ConvAgent {
	private static Logger logger = Logger.getLogger(TransAgent.class);
	
	protected Transport trans = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Trans;
	}
	
	public Collection<String> getInputParticipants() {
		conv = trans;
		return super.getInputParticipants();
	}
	
	public Collection<String> getOutputParticipants() {
		conv = trans;
		return super.getOutputParticipants();
	}
}

