package nl.vu.bioinf.ebf.metamodel.util;

import org.biopax.paxtools.pattern.Match;
import org.biopax.paxtools.model.BioPAXElement;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.ConversionDirectionType;
import org.biopax.paxtools.model.level3.Entity;
import org.biopax.paxtools.model.level3.GeneticInteraction;
import org.biopax.paxtools.model.level3.Interaction;
import org.biopax.paxtools.model.level3.MolecularInteraction;
import org.biopax.paxtools.model.level3.NucleicAcid;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.TemplateReaction;
import org.biopax.paxtools.pattern.constraint.ConstraintAdapter;
import org.biopax.paxtools.pattern.util.Blacklist;
import org.biopax.paxtools.pattern.util.RelType;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Gets the related Conversion where the PhysicalEntity is input or output, whichever is desired.
 *
 * var0 is a PE
 * var1 is a Conversion
 *
 * @author Ozgun Babur: 
 * ##Reza added here a new functionality.
 */
public class ParticipatesInIntr extends ConstraintAdapter
{
	/**
	 * Input or output.
	 */
	private RelType type;

	/**
	 * Constructor with parameters.
	 * @param type input or output conversion
	 * @param blacklist for detecting blacklisted small molecules
	 */
	public ParticipatesInIntr(RelType type, Blacklist blacklist)
	{
		super(2, blacklist);
		this.type = type;
	}

	/**
	 * Constructor with parameters.
	 * @param type input or output
	 * conversion
	 */
	public ParticipatesInIntr(RelType type)
	{
		this(type, null);
	}

	/**
	 * This is a generative constraint.
	 * @return true
	 */
	@Override
	public boolean canGenerate()
	{
		return true;
	}

	/**
	 * Identifies the conversion direction and gets the related participants.
	 * @param match current pattern match
	 * @param ind mapped indices
	 * @return input or output participants
	 */
	@Override
	public Collection<BioPAXElement> generate(Match match, int... ind) {
		Collection<BioPAXElement> result = new HashSet<BioPAXElement>();

		PhysicalEntity pe = (PhysicalEntity) match.get(ind[0]);

		for (Interaction inter : pe.getParticipantOf()) {
			if (inter instanceof Conversion) {
				Conversion cnv = (Conversion) inter;
				ConversionDirectionType dir = getDirection(cnv);

				// do not get blacklisted small molecules
				if (blacklist != null && blacklist.isUbique(pe, cnv, dir, type)) continue;

				if (dir == ConversionDirectionType.REVERSIBLE) {
					result.add(cnv);
				}
				else if (dir == ConversionDirectionType.RIGHT_TO_LEFT &&
					(type == RelType.INPUT ? cnv.getRight().contains(pe) : cnv.getLeft().contains(pe))) {
					result.add(cnv);
				}
				// Note that null direction is treated as if LEFT_TO_RIGHT. This is not a best
				// practice, but it is a good approximation.
				else if ((dir == ConversionDirectionType.LEFT_TO_RIGHT || dir == null) &&
					(type == RelType.INPUT ? cnv.getLeft().contains(pe) : cnv.getRight().contains(pe))) {
					result.add(cnv);
				}
				/*
				else { 
					Set<Entity> participants = cnv.getParticipant();
					if (participants != null && participants.contains(pe)) {
						System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
						result.add(cnv);
					}
				}
				*/	
			}
			else if (inter instanceof Control) {
				Control ctrl = (Control) inter;
				
				// do not get blacklisted small molecules
				if (blacklist != null && blacklist.isUbique(pe)) continue;
				if (ctrl.getController().contains(pe)) {
					result.add(ctrl);
				}
				//else if (ctrl.getParticipant().contains(pe)) {
				//	result.add(ctrl);
				//}
			}
			else if (inter instanceof TemplateReaction) {
				TemplateReaction tmpreac = (TemplateReaction) inter;
				
				// do not get blacklisted small molecules
				if (blacklist != null && blacklist.isUbique(pe)) continue;
				if (tmpreac.getParticipant().contains(pe)) {
					result.add(tmpreac);
				}
			}
			else if (inter instanceof MolecularInteraction) {
				MolecularInteraction mint = (MolecularInteraction) inter;
				
				// do not get blacklisted small molecules
				if (blacklist != null && blacklist.isUbique(pe)) continue;
				if (mint.getParticipant().contains(pe)) {
					result.add(mint);
				}
			}
			else if (inter instanceof GeneticInteraction) {
				GeneticInteraction gint = (GeneticInteraction) inter;
				
				// do not get blacklisted small molecules
				if (blacklist != null && blacklist.isUbique(pe)) continue;
				if (gint.getParticipant().contains(pe)) {
					result.add(gint);
				}
			}
		}

		return result;
	}
}
