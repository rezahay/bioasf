package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class PWayAAStatQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(PWayAAStatQEH.class);

    public PWayAAStatQEH() {   
    	super.init(Constants.PWAY_AA_STAT_QUEUE);
    }
}

