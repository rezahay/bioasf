package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.Catalysis;
import nl.vu.bioinf.ebf.metamodel.Control;
import nl.vu.bioinf.ebf.metamodel.Modulation;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class CatAgent extends CtrlAgent {
	private static Logger logger = Logger.getLogger(CatAgent.class);
	
	
	protected Catalysis cat = null;
	protected ArrayList<Modulation> mods = null;
	protected ArrayList<ModAgent> modAgents = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.Cats;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>();		
		ArrayList<PhysicalEntity> pIns = cat.getController();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		pIns = cat.getCofactor();
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		ctrl = cat;
		return super.getOutputParticipants();
	}
	
	public Collection<String> getSensoryControlParticipants() {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		// Add Modulation which modulates this catalysis.
		if (mods != null) {
			for (Modulation mod : mods) {
				controlParticipants.add(mod.getId());
			}
		}
		
		// Add Control interaction which controls this generic interaction.
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.add(ctrl.getId());
			}
		}
		
		return controlParticipants;
	}
	
	public boolean hasSensoryControls() {
		return ctrls != null || mods != null;
	}
	
	public Collection<Control> getSensoryControls() {
		Collection<Control> sensoryControls = new ArrayList<Control>();
		
		if (ctrls != null) {
			sensoryControls.addAll(ctrls);
		}
		
		if (mods != null) {
			sensoryControls.addAll(mods);
		}
		
		return sensoryControls;
	}
	
	public Collection<CtrlAgent> getSensoryControlAgents() {
		Collection<CtrlAgent> sensoryControlAgents = new ArrayList<CtrlAgent>();
		
		if (modAgents != null) {
			sensoryControlAgents.addAll(modAgents);
		}
		
		if (ctrlAgents != null) {
			sensoryControlAgents.addAll(ctrlAgents);
		}
		
		return sensoryControlAgents;
	}
	
	public Collection<String> getEffectorControlParticipants(BioPAXEvent event) {
		Collection<String> controlParticipants = new ArrayList<String>();
		
		if (mods != null) {
			for (Modulation mod : mods) {
				controlParticipants.addAll(getEffectorControlParticipants(event, mod, mod.getController()));
			}
		}
		
		if (ctrls != null) {
			for (Control ctrl : ctrls) {
				controlParticipants.addAll(getEffectorControlParticipants(event, ctrl, ctrl.getController_PhysicalEntity()));
			}
		}
		
		return controlParticipants;
	}
}

