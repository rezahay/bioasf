package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class IARequestQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(IARequestQEH.class);

    public IARequestQEH() {   
    	super.init(Constants.IA_REQUEST_QUEUE);
    }
}

