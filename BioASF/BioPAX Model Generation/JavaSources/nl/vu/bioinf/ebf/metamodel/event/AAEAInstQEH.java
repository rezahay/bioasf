package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class AAEAInstQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(AAEAInstQEH.class);

    public AAEAInstQEH() {   
    	super.init(Constants.AA_EA_INST_QUEUE);
    }
}

