package nl.vu.bioinf.ebf.metamodel.pathway;

import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;
import org.apache.log4j.Logger;

public class IntStatEvent extends CommonStatEvent {
	private static Logger logger = Logger.getLogger(IntStatEvent.class);
	
	private GenericAgent intAgent = null;
	
	// ==========================================================================================================
	
	public GenericAgent getIntAgent() {
		return intAgent;
	}

	public void setIntAgent(GenericAgent intAgent) {
		this.intAgent = intAgent;
	}
	
	// ==========================================================================================================
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("IntStatEvent {");
		result.append(intAgent.getAgentId());
		result.append(", ");
		result.append(intPerformed);
		result.append(", ");
		result.append(getIntTypeAsString());
		result.append("}");
		return result.toString();
	}
}

