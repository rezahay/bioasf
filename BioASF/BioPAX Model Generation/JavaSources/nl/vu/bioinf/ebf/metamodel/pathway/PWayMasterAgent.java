package nl.vu.bioinf.ebf.metamodel.pathway;

import java.util.HashMap;
import java.util.Map;

import javax.jms.MessageProducer;

import nl.vu.bioinf.ebf.metamodel.algorithm.PerformIntAlgorithm;
import nl.vu.bioinf.ebf.metamodel.analysis.GenericAnalysisAgent;
import nl.vu.bioinf.ebf.metamodel.event.AAPWayInstQEH;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.event.EAEffectorTEH;
import nl.vu.bioinf.ebf.metamodel.event.EventConsumer;
import nl.vu.bioinf.ebf.metamodel.event.IAEffectorQEH;
import nl.vu.bioinf.ebf.metamodel.event.PWayAAStatQEH;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public abstract class PWayMasterAgent extends PWayAgent {
	protected static Logger logger = Logger.getLogger(PWayMasterAgent.class);

	private static PWayMasterAgent pwayMaster = null;
	private static PerformIntAlgorithm performIntAlgorithm = null;
	private static String algorithmClass = null;
	private static String stepType = null;
	
	private static Map<String, Map<String, Integer>> ctrlIdToInPartConMap = new HashMap<String, Map<String, Integer>>();
	
	private static EAEffectorTEH EAEffector = null;
	private static PWayAAStatQEH pwayAAStat = null;
	private static AAPWayInstQEH AAPWayInst = null;
	private static IAEffectorQEH IAEffector = null;
	
    private int numOfControlIntsPerformed = 0;
   	private int numOfNonControlIntsPerformed = 0;
   	
   	protected abstract int getTotalNumberOfInts();
   	//protected abstract int getTotalNumberOfCtrls();
   	//protected abstract int getTotalNumberOfNonCtrls();
   	public abstract void startPwayMaster(String [] args);
   	
   	protected void initAMQCommunication() {
   		if (!Constants.MULTI_PROCESS) {
   			return;
   		}
   		
   		EAEffector = new EAEffectorTEH();
   		pwayAAStat = new PWayAAStatQEH();
   		AAPWayInst = new AAPWayInstQEH();
   		IAEffector = new IAEffectorQEH();
   		consumeEAEffectorEvent();
   		consumeAAPWayInstEvent();
   	}
   	
   	private void generateEvent(BioPAXEvent event) {
		setChanged();
		notifyObservers(event);
	}
   	
	public void performDegradations(BioPAXEvent event) {
		performIntAlgorithm.performDegradations(event);
	}
   	
   	public void generateIAEffectorEvent(BioPAXEvent IAEffectorEvent) {
   		if (Constants.MULTI_PROCESS) {
   			MessageProducer producer = IAEffector.createProducer();
   			IAEffector.produce(IAEffectorEvent, producer);
   		}
    	else {
    		generateEvent(IAEffectorEvent);
    	}	
    }
   	
   	/*
     * MasterPWay sends statistics to the proper queue. 
     */
    protected void generatePWayAAStatEvent(BioPAXEvent PWayAAStatEvent) {
    	if (Constants.MULTI_PROCESS) {
    		MessageProducer producer = pwayAAStat.createProducer();
    		pwayAAStat.produce(PWayAAStatEvent, producer);
    	}
    	else {
    		generateEvent(PWayAAStatEvent);
    	}
    }
    
    /*
     * MasterPWay reads EA's effector from the proper topics.
     */
    private void consumeEAEffectorEvent() {
		EventConsumer consumer = new EventConsumer();
		consumer.addObserver(this);
		EAEffector.createSubscriber(consumer, null);
    }
    
    /*
     * MasterPWay reads AA's instruction from the proper queue. 
     */
    private void consumeAAPWayInstEvent() {
		EventConsumer consumer = new EventConsumer();
		consumer.addObserver(this);
		AAPWayInst.createConsumer(consumer, null);
    }
    
    protected void handleAAPWayInstEvent(BioPAXEvent AAPWayInstEvent) {
    	if (logger.isInfoEnabled()) {
    		logger.info("$$Stop instruction received for PWayMaster.");
    	}	
    	if (Constants.MULTI_PROCESS) {
    		System.exit (0);
    	}
    	else {
    		//Thread.currentThread().interrupt();
    	}
    }
    
    protected void generatePWayAAStatEvent() {
    	if (logger.isInfoEnabled()) {
    		logger.info("numOfNonControlIntsPerformed: " + numOfNonControlIntsPerformed);
    	}	
		
		BioPAXEvent PWayAAStatEvent = new BioPAXEvent();
		PWayAAStatEvent.setAgentId(this.getAgentId());
		
		if (numOfNonControlIntsPerformed > 0) {
			PWayAAStatEvent.setEventType(Constants.PWAY_AA_ALL_INTS_PERFORMED);
		}
		else {
			PWayAAStatEvent.setEventType(Constants.PWAY_AA_NO_INT_PERFORMED);
		}
		
		numOfNonControlIntsPerformed = 0;
		numOfControlIntsPerformed = 0;
		
		generatePWayAAStatEvent(PWayAAStatEvent);
    }
   	
   	protected void handleStatistics() {
   		if (pwayStatEvent.isIntPerformed()) {
   			BioPAXInteactionType intType = pwayStatEvent.getIntType();
   	    	switch (intType) {
   	    	case Ints:	
   	    		numOfNonControlIntsPerformed++;
   				break;
   	    	case MInts: case GInts:
   	    		numOfNonControlIntsPerformed++;
   	    		break;
   	    	case TmpReacs:	
   	    		numOfNonControlIntsPerformed++;
   				break;	
   			case Convs: case CmpAsms: case BioReacs: case Degrs: case Trans:
   				numOfNonControlIntsPerformed++;
   				break;
   			case Ctrls: case TmpRegs: case Cats: case Mods: 
   				numOfControlIntsPerformed++;
   				break;	
   			default:
   				break;
   			}
   		}
    }
   	
    private static void usageAndExit (String msg) {
        if (msg != null) {
            logger.error(msg);
        }    

        System.exit (0);
    }
    
    public static Map<String, Map<String, Integer>> getCtrlIdToInPartConMap() {
		return ctrlIdToInPartConMap;
	}
    
	public static PerformIntAlgorithm getPerformIntAlgorithm() {
		return performIntAlgorithm;
	}
    
	public static String getAlgorithmClass() {
    	return algorithmClass;
    }
    
    public static String getStepType() {
    	return stepType;
    }
    
    public static PWayMasterAgent getPwayMaster() {
		return pwayMaster;
	}
   	
   	protected void handleArgs(String [] args) {
   		if (args.length != 2) { 
    		usageAndExit("Usage (order is important): PWayA_Master <algorithm_class> <step_type>");
    	}
    	algorithmClass = args[0];
    	stepType = args[1];
    	switch (stepType) {
    	case Constants.SYNCHRONOUS_STEP:
    		break;
	   	case Constants.ASYNCHRONOUS_STEP:
			break;
	   	case Constants.MAX_PARALLEL_STEP:
			break;	
	   	default:
	   		usageAndExit("<step_type> must be one of " + Constants.SYNCHRONOUS_STEP + ", " + Constants.ASYNCHRONOUS_STEP + ", or " + Constants.MAX_PARALLEL_STEP + ".");
			break;
		}
    	
    	try {
    		performIntAlgorithm = (PerformIntAlgorithm) Class.forName(algorithmClass).newInstance();
    	}
    	catch (Exception e) {
    		logger.error("No PerformIntAlgorithm class can be created from the class-name: " + algorithmClass, e);
    		System.exit(-1);
    	}
    	
    	pwayMaster = this;
   	}
   	
   	public void setAnalysisAgentAsObserver(GenericAnalysisAgent analysisAgent) {
   		addObserver(analysisAgent);
    	analysisAgent.setPWayAgentAsOnserver(this);
    }
}

