package nl.vu.bioinf.ebf.metamodel.event;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

public class BioPAXEvent {
	private static Logger logger = Logger.getLogger(BioPAXEvent.class);
	private static final int INVALID_CON = -1000;
	
	private String eventType = null;
	private String agentId = null;
	private BioPAXInteactionType intType = null;
	private boolean intPerformed = false;
	
	private Map<String, Integer> inPartConMap = new HashMap<String, Integer>();
	private Map<String, Integer> outPartConMap = new HashMap<String, Integer>();
	
	// ==========================================================================================================
	
	public Map<String, Integer> getInPartConMap() {
		return inPartConMap;
	}

	public void setInPartConMap(Map<String, Integer> inPartConMap) {
		this.inPartConMap = inPartConMap;
	}

	public Map<String, Integer> getOutPartConMap() {
		return outPartConMap;
	}

	public void setOutPartConMap(Map<String, Integer> outPartConMap) {
		this.outPartConMap = outPartConMap;
	}

	private String getPartConMapAsString(Map<String, Integer> partConMap) {
		if (partConMap == null) {
			return Constants.NULL_PART_CON_LIST_STR;
		}	
		if (partConMap.isEmpty()) {
			return Constants.EMPTY_PART_CON_LIST_STR;
		}	
		
		StringBuffer result = new StringBuffer();
		result.append("[");
		Collection<String> participantIds = partConMap.keySet();
		for (String participantId : participantIds) {
			result.append(participantId);
			result.append("=");
			result.append(partConMap.get(participantId) + ",");
		}
		result.setLength(result.length() - 1);
		result.append("]");
		return result.toString();
	}
	
	public String getInPartConMapAsString() {
		return getPartConMapAsString(inPartConMap);
	}
	
	public String getOutPartConMapAsString() {
		return getPartConMapAsString(outPartConMap);
	}
	
	public void setInPartConMapFromString(String strPartConMap) {
		if (strPartConMap.equalsIgnoreCase(Constants.NULL_PART_CON_LIST_STR)) {
			inPartConMap = null;
			return;
		}
		
		inPartConMap = new HashMap<String, Integer>();
		if (strPartConMap.equalsIgnoreCase(Constants.EMPTY_PART_CON_LIST_STR)) {
			return;
		}
		
		// [p1=1,p2=2]
		strPartConMap = strPartConMap.substring(1, strPartConMap.length());
		strPartConMap = strPartConMap.substring(0, strPartConMap.length() - 1);
		String[] arrPartConMap = strPartConMap.split(",");
		
		try {
			for (int i = 0; i < arrPartConMap.length; i++) {
				String[] arrEntry = arrPartConMap[i].split("=");
				String participantId = arrEntry[0];
				int concentration = Integer.valueOf(arrEntry[1]);
				inPartConMap.put(participantId, concentration);
			}
		}
		catch (NumberFormatException nfe) {
			logger.error("No proper concentration number: ", nfe);
		}
	}
	
	public void setOutPartConMapFromString(String strPartConMap) {
		if (strPartConMap.equalsIgnoreCase(Constants.NULL_PART_CON_LIST_STR)) {
			outPartConMap = null;
			return;
		}
		
		outPartConMap = new HashMap<String, Integer>();
		if (strPartConMap.equalsIgnoreCase(Constants.EMPTY_PART_CON_LIST_STR)) {
			return;
		}
		
		// [p1=1,p2=2]
		strPartConMap = strPartConMap.substring(1, strPartConMap.length());
		strPartConMap = strPartConMap.substring(0, strPartConMap.length() - 1);
		String[] arrPartConMap = strPartConMap.split(",");
		
		try {
			for (int i = 0; i < arrPartConMap.length; i++) {
				String[] arrEntry = arrPartConMap[i].split("=");
				String participantId = arrEntry[0];
				int concentration = Integer.valueOf(arrEntry[1]);
				outPartConMap.put(participantId, concentration);
			}
		}
		catch (NumberFormatException nfe) {
			logger.error("No proper concentration number: ", nfe);
		}
	}
	
	private int getConcentation(String queriedPartId, Map<String, Integer> partConMap) {
		if (queriedPartId == null || partConMap == null || partConMap.isEmpty()) {
			return INVALID_CON;
		}
		
		int concentation = 0;
		if (partConMap.containsKey(queriedPartId)) {
			concentation = partConMap.get(queriedPartId);
		}
		
		return concentation;
	}
	
	private void setConcentation(String queriedPartId, int concentation, Map<String, Integer> partConMap) {
		if (queriedPartId == null || partConMap == null || partConMap.isEmpty()) {
			return;
		}
		
		if (partConMap.containsKey(queriedPartId)) {
			partConMap.put(queriedPartId, concentation);
		}
	}
	
	public int getInConcentation(String queriedPartId) {
		return getConcentation(queriedPartId, inPartConMap);
	}
	
	public void setInConcentation(String queriedPartId, int concentration) {
		setConcentation(queriedPartId, concentration, inPartConMap);
	}
	
	public int getOutConcentation(String queriedPartId) {
		return getConcentation(queriedPartId, outPartConMap);
	}
	
	public void setOutConcentation(String queriedPartId, int concentration) {
		setConcentation(queriedPartId, concentration, outPartConMap);
	}
	
	// ==========================================================================================================
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	// ==========================================================================================================
	
	public String getAgentId() {
		if (agentId == null) {
			agentId = Constants.EMPTY_AGENT_STR;
		}
		return agentId;
	}
	
	
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
		
	// ==========================================================================================================
	
	public BioPAXInteactionType getIntType() {
		return intType;
	}

	public void setIntType(BioPAXInteactionType intType) {
		this.intType = intType;
	}
	
	public String getIntTypeAsString() {
		if (intType == null) {
			return Constants.EMPTY_INTTYPE_STR;
		}	
		return intType.toString();
	}
	
	public void setIntTypeFromString(String strIntType) {
		if (strIntType.equalsIgnoreCase(Constants.EMPTY_INTTYPE_STR)) {
			intType = null;
			return;
		}
		intType = BioPAXInteactionType.valueOf(strIntType);
	}
	
	// ==========================================================================================================
	
	public boolean isIntPerformed() {
		return intPerformed;
	}

	public void setIntPerformed(boolean intPerformed) {
		this.intPerformed = intPerformed;
	}

	// ==========================================================================================================
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("BioPAXEvent {");
		
		result.append(eventType);
		result.append(", ");
		result.append(agentId);
		result.append(", ");
		result.append(getIntTypeAsString());
		result.append(", ");
		result.append(intPerformed);
		result.append(", ");
		result.append(getInPartConMapAsString());
		result.append(", ");
		result.append(getOutPartConMapAsString());
		
		result.append("}");
		return result.toString();
	}
}

