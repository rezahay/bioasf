package nl.vu.bioinf.ebf.metamodel.analysis;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.jms.MessageProducer;
import javax.jms.TopicPublisher;

import nl.vu.bioinf.ebf.metamodel.event.AAPWayInstQEH;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.event.EAEffectorTEH;
import nl.vu.bioinf.ebf.metamodel.event.EventConsumer;
import nl.vu.bioinf.ebf.metamodel.event.IAEffectorQEH;
import nl.vu.bioinf.ebf.metamodel.event.PWayAAStatQEH;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayAgent;
import nl.vu.bioinf.ebf.metamodel.pathway.PWayMasterAgent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public abstract class GenericAnalysisAgent extends Observable implements Observer {
	private static Logger logger = Logger.getLogger(GenericAnalysisAgent.class);
	
	private static final String ANALYSER_AGENT_ID = "Analyser";
	
	protected static AnalysisBioEnv analysisBioEnv = AnalysisBioEnv.getInstance();
	
	private static EAEffectorTEH EAEffector = null;
	private static IAEffectorQEH IAEffector = null;
	private static PWayAAStatQEH PWayAAStat = null;
	private static AAPWayInstQEH AAPWayInst = null;
	
	private static final String AMQ_CLEAN_TARGET = "runAMQJMXOpsNoComp";
	private static final String RUN_SIMULATION_TARGET = "runSimulation";
	
	private static PWayMasterAgent pwayMaster = null;
	private static String algorithmClass = null;
	private static String stepType = null;
	protected static String resourceOutputDir = null;
	private static String outputDir = null;
	
	protected abstract void init() throws Exception;
	protected abstract void effectuateNextState();
	protected abstract void handleIAEffectorEvent(BioPAXEvent IAEffectorEvent);
	protected abstract void handlePWayAANoIntPerformedEvent(BioPAXEvent pwayAAStatEvent);
	protected abstract void handlePWayAAAllIntsPerformedEvent(BioPAXEvent pwayAAStatEvent);
	
	private void fireMultiProcessVersion(String targetToExecute) {
		// Logger to log output generated while executing ant script in console
		DefaultLogger consoleLogger = new DefaultLogger();
        consoleLogger.setErrorPrintStream(System.err);
        consoleLogger.setOutputPrintStream(System.out);
        consoleLogger.setMessageOutputLevel(Project.MSG_INFO);
		
        File antBuildFile = new File("build.xml");
        Project project = new Project();
        project.setUserProperty("ant.file", antBuildFile.getAbsolutePath());
        project.addBuildListener(consoleLogger);
 
        // Capture event for Ant script build start / stop / failure
        try {
            project.fireBuildStarted();
            project.init();
            ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
            project.addReference("ant.projectHelper", projectHelper);
            projectHelper.parse(project, antBuildFile);
             
            project.executeTarget(targetToExecute);
            project.fireBuildFinished(null);
        } 
        catch (BuildException buildException) {
            project.fireBuildFinished(buildException);
            logger.error("Unable to run the ANT target '" + targetToExecute + "'!!", buildException);
        }
	}
	
	protected void runAMQCleanup() {
		if (Constants.MULTI_PROCESS) {
			fireMultiProcessVersion(AMQ_CLEAN_TARGET);
    	}
	}
	
	private void fireMultiThreadVersion(String targetToExecute) {
		switch (targetToExecute) {
		case RUN_SIMULATION_TARGET:
			String[] args = {algorithmClass, stepType};
			pwayMaster.startPwayMaster(args);
			pwayMaster.setAnalysisAgentAsObserver(this);
			break;
		default:
			break;
		}
	}
	
	protected void startSimulation(boolean isRestart) {
		if (Constants.MULTI_PROCESS) {
			fireMultiProcessVersion(RUN_SIMULATION_TARGET);
    	}
		else {
			// We will start the thread version but we will not restart it.
			if (!isRestart) {
				fireMultiThreadVersion(RUN_SIMULATION_TARGET);
			}	
		}
	}	
	
	protected void stopSimulation() {
    	BioPAXEvent instEvent = new BioPAXEvent();
		instEvent.setAgentId(ANALYSER_AGENT_ID);
		instEvent.setEventType(Constants.AA_PWAY_INST_STOP);
		generateAAPWayInsEvent(instEvent);
    }
	
	protected void restartSimulation() {
		effectuateNextState();
		runAMQCleanup();
    	startSimulation(true);
    	generateEAEffectorEvent();
	}
	
	
    protected void consumePWayAAStatEvent() {
		EventConsumer consumer = new EventConsumer();
		consumer.addObserver(this);
		PWayAAStat.createConsumer(consumer, null);
    }
    
    private void consumeIAEffectorEvent() {
		EventConsumer consumer = new EventConsumer();
		consumer.addObserver(this);
		IAEffector.createConsumer(consumer, null);
    }
    
    protected void generateAAPWayInsEvent(BioPAXEvent AAPWayInstEvent) {
    	if (Constants.MULTI_PROCESS) {
    		MessageProducer AAPWayInstProducer = AAPWayInst.createProducer();
    		AAPWayInst.produce(AAPWayInstEvent, AAPWayInstProducer);
    	}
    	else {
    		setChanged();
      		notifyObservers(AAPWayInstEvent);
    	}
    }
    
    protected void generateEAEffectorEvent() {
    	BioPAXEvent EAEffectorEvent = analysisBioEnv.consAnalysisEAEffector();
    	if (Constants.MULTI_PROCESS) {
    		TopicPublisher EAEffectorPublisher = EAEffector.createPublisher();
    		EAEffector.publish(EAEffectorEvent, EAEffectorPublisher);
    	}
    	else {
    		setChanged();
      		notifyObservers(EAEffectorEvent);
    	}	
    }
    
    
    public void update(Observable observable, Object event) {
		if (event == null) {
    		logger.warn("Analysis-Agent: IA or PWay Event should not be Null.");
    		return;
    	}
    	
		BioPAXEvent biopaxEvent = (BioPAXEvent)event;
		String eventSource = biopaxEvent.getEventType();
		switch (eventSource) {
			case Constants.IA_EFFECTOR:
				handleIAEffectorEvent(biopaxEvent);
				break;
			case Constants.PWAY_AA_ALL_INTS_PERFORMED:
				handlePWayAAAllIntsPerformedEvent(biopaxEvent);
				break;
			case Constants.PWAY_AA_NO_INT_PERFORMED:
				handlePWayAANoIntPerformedEvent(biopaxEvent);
				break;	
			default:
				logger.warn("Analysis-Agent cannot handle this event: " + biopaxEvent.getEventType());
				break;
		}
	}
    
    private static void usageAndExit (String msg) {
        if (msg != null) {
            logger.error(msg);
        }    

        System.exit(0);
    }
    
    private void createPWayMaster() {
		if (Constants.MULTI_PROCESS) {
			return;
    	}
		
		try {
			pwayMaster = (PWayMasterAgent) Class.forName(Constants.PWAYA_MASTER_CLASS).newInstance();
    	}
    	catch (Exception e) {
    		logger.error("No PWayA_Master class can be created from the class-name: " + Constants.PWAYA_MASTER_CLASS, e);
    		System.exit(-1);
    	}
	}
	
    private void initAMQCommunication() {
    	if (!Constants.MULTI_PROCESS) {
    		return;
    	}
    	
    	EAEffector = new EAEffectorTEH();
    	AAPWayInst = new AAPWayInstQEH();
    	IAEffector = new IAEffectorQEH();
    	PWayAAStat = new PWayAAStatQEH();
    	
    	
    	runAMQCleanup();
    	if (logger.isInfoEnabled()) {
    		logger.info("Analysis-Agent: Waiting for events from PWayAAStat channel ...");
    	}	
    	consumePWayAAStatEvent();
    	if (logger.isInfoEnabled()) {
    		logger.info("Analysis-Agent: Waiting for events from IAEffector channel ...");
    	}	
    	consumeIAEffectorEvent();
    }
    
    protected void start(String[] args) throws Exception {
    	if (args.length != 3) { 
    		usageAndExit("Usage: AnalysisAgent <model-output-dir> <algorithm_class> <step_type>");
    	}
    	
    	outputDir = args[0];
		resourceOutputDir = outputDir + "/" + Constants.JAVA_RESOURCE_DIR;
		algorithmClass = args[1];
		stepType = args[2];
		
    	analysisBioEnv.createAnalysisEnv(resourceOutputDir);
    	init();
    	initAMQCommunication();
    	
    	createPWayMaster();
    	startSimulation(false);
    	
    	generateEAEffectorEvent();
    }
    
    public void setPWayAgentAsOnserver(PWayAgent pwayAgent) {
    	addObserver(pwayAgent);
    }
}

