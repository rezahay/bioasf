package nl.vu.bioinf.ebf.metamodel.event;

import java.util.Observable;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

public class EventConsumer extends Observable implements MessageListener {
	private static Logger logger = Logger.getLogger(EventConsumer.class);
	
	public void onMessage(Message message) { 
		try {
			BioPAXEvent event = new BioPAXEvent();
			event.setEventType(message.getStringProperty(Constants.EVENT_TYPE));
			event.setAgentId(message.getStringProperty(Constants.AGENT_ID));
			event.setIntTypeFromString(message.getStringProperty(Constants.INT_TYPE));
			event.setIntPerformed(message.getBooleanProperty(Constants.INT_PERFORMED));
			/*
			event.setInParticipantIdsFromString(message.getStringProperty(Constants.IN_PARTICIPANT_IDS));
			event.setInConcentrationsFromString(message.getStringProperty(Constants.IN_CONCENTRATIONS));
			event.setOutParticipantIdsFromString(message.getStringProperty(Constants.OUT_PARTICIPANT_IDS));
			event.setOutConcentrationsFromString(message.getStringProperty(Constants.OUT_CONCENTRATIONS));
			*/
			event.setInPartConMapFromString(message.getStringProperty(Constants.IN_PART_CON_MAP));
			event.setOutPartConMapFromString(message.getStringProperty(Constants.OUT_PART_CON_MAP));
			
			if (logger.isDebugEnabled()) {
	            logger.debug("Receving event: " + event.toString());
			}
			setChanged();
			notifyObservers(event);
	    }
		catch (Throwable t) {
			logger.error("Consume failed because: ", t);
	    }
	}
}
