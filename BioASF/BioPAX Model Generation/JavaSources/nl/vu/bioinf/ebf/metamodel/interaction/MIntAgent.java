package nl.vu.bioinf.ebf.metamodel.interaction;

import java.util.ArrayList;
import java.util.Collection;

import nl.vu.bioinf.ebf.metamodel.MolecularInteraction;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;

import org.apache.log4j.Logger;


public abstract class MIntAgent extends IntAgent {
	private static Logger logger = Logger.getLogger(MIntAgent.class);
	
	protected MolecularInteraction mintr = null;
	
	public BioPAXInteactionType getBioPAXIntType() {
		return BioPAXInteactionType.MInts;
	}
	
	public Collection<String> getInputParticipants() {
		Collection<String> inputParticipants = new ArrayList<String>(); 
		ArrayList<PhysicalEntity> pIns = mintr.getParticipant_MolecularInteraction();
		
		if (pIns != null) {
			for (PhysicalEntity physicalEntity : pIns) {
				inputParticipants.add(physicalEntity.getId());
			}
		}
		return inputParticipants;
	}
	
	public Collection<String> getOutputParticipants() {
		Collection<String> outputParticipants = new ArrayList<String>(); 
		ArrayList<PhysicalEntity> pOuts = mintr.getParticipant_MolecularInteraction();
		
		if (pOuts != null) {
			for (PhysicalEntity physicalEntity : pOuts) {
				outputParticipants.add(physicalEntity.getId());
			}
		}
		
		return outputParticipants;
	}
	
}

