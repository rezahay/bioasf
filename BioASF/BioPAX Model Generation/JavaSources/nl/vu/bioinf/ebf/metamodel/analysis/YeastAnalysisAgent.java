package nl.vu.bioinf.ebf.metamodel.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.util.AMQJMXOperations;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class YeastAnalysisAgent extends GenericAnalysisAgent {
	private static Logger logger = Logger.getLogger(YeastAnalysisAgent.class);
	
	// The current state of the state space. We skip the '0' state. Every time if a state is used we push it in the usedStates.
	private int curState = Constants.INVALID_STATE; 
	
	// Each element of this set is a bitVector represented as an integer. In the beginning, this set contains all possible states,
	// i.e., 2^numOfPhysicalEntities. Every time, a state is picked up from this set to initiate concentrations. The chosen state
	// will be removed from the set immediately. A state will also be removed from the set when it is reported by BioEnvAgent. 
	private List<Integer> stateSpace = new ArrayList<Integer>();
	
	// This contains the state space graph which is updated every time if a new state is received from the BioEnvAgent. The graph is used
	// to calculate the strongly connected components.
	private StateSpaceGraph grnGraph = new StateSpaceGraph();
	
	private void consTestStateSpace() {
		stateSpace.clear();
		for (int i = 1210; i < 1211; i++) {
			stateSpace.add(i);
		}
	}
	
	private void consStateSpace() {
		int numOfPhysicalEntities = analysisBioEnv.getNumOfPhysicalEntities();
		int numOfStates = (int)Math.pow(2, numOfPhysicalEntities); 
		//for (int i = 0; i < numOfStates; i++) {
		//	stateSpace.add(i);
		//}
		for (int i = numOfStates-1; i >= 0; i--) {
			stateSpace.add(i);
		}
		
		// Just for test purposes.
		//consTestStateSpace();
	}
	
	protected void effectuateNextState() {
		curState = stateSpace.remove(stateSpace.size()-1);
		analysisBioEnv.populateAnalysisEnvFromState(curState);
		if (logger.isInfoEnabled()) {
			logger.info("##Initial state calculated by AA: " + analysisBioEnv.printState(curState));
		}
	}
	
	protected void init() throws Exception {
		consStateSpace();
		effectuateNextState();
    }
	
	private void considerStoppingSimulation() {
    	stopSimulation();
    	if (stateSpace.isEmpty()) {
    		stopAnalysis();
		}
		else {
			restartSimulation();
		}
    }
	
	private void stopAnalysis() {
		if (logger.isInfoEnabled()) {
			logger.info("##State space is empty.");
		}	
		
		/*
		grnGraph.findSCC();
		if (logger.isInfoEnabled()) {
			logger.info(grnGraph.printSCCNodes());
		}
		*/
		
		grnGraph.findTSCC();
		if (logger.isInfoEnabled()) {
			logger.info(grnGraph.printTSCC(analysisBioEnv));
		}	
		//grnGraph.displayGraph();
		
		System.exit(0);
	}
	
	protected synchronized void handleIAEffectorEvent(BioPAXEvent IAEffectorEvent) {
		analysisBioEnv.storeAnalysisEnvToBackup();
		analysisBioEnv.updateAnalysisEnvWithEffectors(IAEffectorEvent);
		int targetState = analysisBioEnv.consStateFromAnalysisEnv();
		if (logger.isInfoEnabled()) {
			logger.info(analysisBioEnv.printState(targetState));
		}	
		analysisBioEnv.retrieveAnalysisEnvFromBackup();
		grnGraph.updateGraph(curState, targetState);
    }

	protected synchronized void handlePWayAAAllIntsPerformedEvent(BioPAXEvent pwayAAStatEvent) {
		if (logger.isDebugEnabled()) {
			logger.debug("$$All_Interactions_Performed message received from PWay.");
		}
		
    	considerStoppingSimulation();
    }
	
    protected synchronized void handlePWayAANoIntPerformedEvent(BioPAXEvent pwayAAStatEvent) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("$$No_Interaction_Performed message received from PWay.");
    	}	
    	considerStoppingSimulation();
    }
    
    public static void main(String[] args) {
    	logger.info("========================================");
    	logger.info("YeastAgent: Starting & initialising ...");
    	logger.info("========================================");
        try {
        	new YeastAnalysisAgent().start(args);
        }
        catch (Exception e) {
            logger.error("Exception in YeastAgent: " + e);
            System.exit (1);
        }
    }
}

