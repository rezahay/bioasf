package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class AAPWayInstQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(AAPWayInstQEH.class);

    public AAPWayInstQEH() {   
    	super.init(Constants.AA_PWAY_INST_QUEUE);
    }
}

