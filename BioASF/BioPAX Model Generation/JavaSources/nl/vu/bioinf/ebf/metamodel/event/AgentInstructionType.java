package nl.vu.bioinf.ebf.metamodel.event;

public enum AgentInstructionType {
	Restart, Stop;
}

