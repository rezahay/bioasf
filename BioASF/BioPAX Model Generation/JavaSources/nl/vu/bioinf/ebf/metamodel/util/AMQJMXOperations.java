package nl.vu.bioinf.ebf.metamodel.util;

import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.QueueViewMBean;
import org.apache.activemq.broker.jmx.TopicViewMBean;
import org.apache.log4j.Logger;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

public class AMQJMXOperations {
	private static Logger logger = Logger.getLogger(AMQJMXOperations.class);
	
	private static final String AMQ_JMX_URL = "service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi";
	
	private static MBeanServerConnection connection = null;
	
    
	private static MBeanServerConnection connect() {
        JMXConnector connector = null;
        String username = "";
        String password = "";

        try {
	        Map env = new HashMap();
	        String[] credentials = new String[] { username, password };
	        env.put(JMXConnector.CREDENTIALS, credentials);
	
	        connector = JMXConnectorFactory.newJMXConnector(new JMXServiceURL(AMQ_JMX_URL), env);
	        connector.connect();
	        connection = connector.getMBeanServerConnection();
        }
        catch (Exception e) {
        	logger.error("Connection problem to ActiveMQ during cleaning up.", e);
        }
        
        return connection; 
    }
	
    private static void removeQueue(String queueName) {
    	if (connection == null) {
    		connection = connect();
    	}
    	
    	try {
	    	String brokerNameQuery = "org.apache.activemq:type=Broker,brokerName=localhost";
	        String purgeQueueOperationName = "removeQueue";
	        Object[] params = { queueName };
	        String[] sig = { "java.lang.String" };
	        ObjectName brokerObjName = new ObjectName(brokerNameQuery); 
	        connection.invoke(brokerObjName, purgeQueueOperationName, params, sig);
	        logger.info("Queue [" + queueName + "] has been removed.");
    	}
        catch (Exception e) {
          	logger.error("Problem during removing queue '" + queueName + "'.", e);
          }
    }
    
    public static void startBroker() {
    	try {
    		if (connection == null) {
        		connection = connect();
        	}
	    	ObjectName activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=localhost");
	    	BrokerViewMBean mbean = (BrokerViewMBean) MBeanServerInvocationHandler.newProxyInstance(connection, activeMQ,BrokerViewMBean.class, true);
	    	logger.info("Stopping ActiveMQ broker ...");
	    	mbean.restart();
    	}
    	catch (Exception e) {
          	logger.error("Problem during purging queues.", e);
    	}  	
    }
    
    public static void stopBroker() {
    	try {
    		if (connection == null) {
        		connection = connect();
        	}
	    	ObjectName activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=localhost");
	    	BrokerViewMBean mbean = (BrokerViewMBean) MBeanServerInvocationHandler.newProxyInstance(connection, activeMQ,BrokerViewMBean.class, true);
	    	logger.info("Stopping ActiveMQ broker ...");
	    	mbean.stop();
    	}
    	catch (Throwable t) {
          	logger.error("Problem during stopping ActiveMQ Broker.");
    	}  	
    }

    
    public static void purgeQueues() {
    	try {
    		if (connection == null) {
        		connection = connect();
        	}
	    	ObjectName activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=localhost");
	    	BrokerViewMBean mbean = (BrokerViewMBean) MBeanServerInvocationHandler.newProxyInstance(connection, activeMQ,BrokerViewMBean.class, true);
	    	mbean.removeTopic(Constants.EA_EFFECTOR_TOPIC);
	    	
	    	for (ObjectName name : mbean.getQueues()) {
			    QueueViewMBean queueMbean = (QueueViewMBean)
			           MBeanServerInvocationHandler.newProxyInstance(connection, name, QueueViewMBean.class, true);
			    queueMbean.purge();
			    //logger.info("Queue [" + queueMbean.getName() + "] has been purged.");
			} 
    	}
    	catch (Exception e) {
          	logger.error("Problem during purging queues.", e);
    	}  	
    }
    
    public static void removeQueues() {
    	removeQueue(Constants.IA_REQUEST_QUEUE);
    	removeQueue(Constants.IA_EFFECTOR_QUEUE);
    	removeQueue(Constants.EA_AA_STAT_QUEUE);
    	removeQueue(Constants.EA_RESPONAE_QUEUE);
    	removeQueue(Constants.AA_EA_INST_QUEUE);
    	removeQueue(Constants.AA_PWAY_INST_QUEUE);
    	removeQueue(Constants.PWAY_AA_STAT_QUEUE);
    }
    
    public static void main(String[] args) {
    	purgeQueues();
    	//removeQueues();
    }
}

