package nl.vu.bioinf.ebf.metamodel.algorithm;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXEvent;
import nl.vu.bioinf.ebf.metamodel.interaction.GenericAgent;

public interface PerformIntAlgorithm {
	public boolean canIntBePerformed(GenericAgent thisInt, BioPAXEvent event);
	public void performInteraction(GenericAgent thisInt, BioPAXEvent event);
	public void performDegradations(BioPAXEvent event);
}

