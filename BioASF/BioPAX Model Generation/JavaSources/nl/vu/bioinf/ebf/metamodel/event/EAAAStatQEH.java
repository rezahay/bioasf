package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class EAAAStatQEH extends BioPAXQueueEventHandler {
	private static Logger logger = Logger.getLogger(EAAAStatQEH.class);

    public EAAAStatQEH() {   
    	super.init(Constants.EA_AA_STAT_QUEUE);
    }
}

