package nl.vu.bioinf.ebf.metamodel.pathway;

import nl.vu.bioinf.ebf.metamodel.event.BioPAXInteactionType;
import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class CommonStatEvent {
	private static Logger logger = Logger.getLogger(CommonStatEvent.class);
	
	protected boolean intPerformed = false;
	protected BioPAXInteactionType intType = null;
	
	// ==========================================================================================================
	
	public boolean isIntPerformed() {
		return intPerformed;
	}

	public void setIntPerformed(boolean intPerformed) {
		this.intPerformed = intPerformed;
	}
	
	// ==========================================================================================================
	
		public BioPAXInteactionType getIntType() {
			return intType;
		}

		public void setIntType(BioPAXInteactionType intType) {
			this.intType = intType;
		}
		
		public String getIntTypeAsString() {
			if (intType == null) {
				return Constants.EMPTY_INTTYPE_STR;
			}	
			return intType.toString();
		}
		
		public void setIntTypeFromString(String strIntType) {
			if (strIntType.equalsIgnoreCase(Constants.EMPTY_INTTYPE_STR)) {
				intType = null;
				return;
			}
			intType = BioPAXInteactionType.valueOf(strIntType);
		}
}

