package nl.vu.bioinf.ebf.metamodel.event;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;

public class EAEffectorTEH extends BioPAXTopicEventHandler {
	private static Logger logger = Logger.getLogger(EAEffectorTEH.class);

    public EAEffectorTEH() {   
    	super.init(Constants.EA_EFFECTOR_TOPIC);
    }
}

