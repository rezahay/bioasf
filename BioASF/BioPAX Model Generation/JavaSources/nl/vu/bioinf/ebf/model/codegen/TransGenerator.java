package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Transport;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

// BiochemicalReaction: display-name
public class Trans_000001 extends BiochemicalReaction {

    private static Trans_000001 trans_000001 = null;

    private Trans_000001() {
    }

    public static Trans_000001 getInstance() {
        if (trans_000001 == null) {
        	trans_000001 = new Trans_000001();
        	trans_000001.init();
        }
        return trans_000001;
    }
    
    private void init() {
    	id = "Trans_000001";
    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
    	left = new ArrayList<PhysicalEntity>();
    	left.add(Prot_000001.getInstance());
    	left.add(SmlM_000001.getInstance());
    	right = new ArrayList<PhysicalEntity>();
    	right.add(Prot_000002.getInstance());
    	participant_Conversion = null;
   }
}
=====================================================
 */
public class TransGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(TransGenerator.class);
	
	private Transport trans = null;
	private List<String> leftList = new ArrayList<String>();
	private List<String> rightList = new ArrayList<String>();
	private List<String> stoiList = new ArrayList<String>();
	
	public TransGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_TRANS;
		trans = (Transport)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		childrenClassNames.put(ASSOC_CATS, cats);
		childrenClassNames.put(ASSOC_CAT_AGENTS, catAgents);
		new TransAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(trans.getLeft());
		leftList.addAll(entityList);
		entityList.clear();
		consChildrenFromControllerSet(trans.getRight());
		rightList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(trans.getControlledOf());
		consChildrenFromUtilitySet(trans.getParticipantStoichiometry());
		stoiList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     private void init() {
	    	id = "Trans_000001";
	    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
	    	left = new ArrayList<PhysicalEntity>();
	    	left.add(Prot_000001.getInstance());
	    	left.add(SmlM_000001.getInstance());
	    	right = new ArrayList<PhysicalEntity>();
	    	right.add(Prot_000002.getInstance());
	    	participant_Conversion = null;
	   }
	 */
	
	private String generateTransStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenConvStmts(trans));
    	
    	if (!leftList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(leftList, SUPER_CLASS_PHYE, "left"));
        }
        if (!rightList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(rightList, SUPER_CLASS_PHYE, "right"));
        }
        if (!stoiList.isEmpty()) {
	     	text.append(INDENT2);
	     	text.append(generateListItemsByListAddOp(stoiList, SUPER_CLASS_STOI, "participantStoichiometry"));
	 	}
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateTransStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

