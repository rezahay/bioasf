package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Degradation;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.PhysicalEntity;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

public class Degr_000001 extends BiochemicalReaction {

    private static Degr_000001 degr_000001 = null;

    private Degr_000001() {
    }

    public static Degr_000001 getInstance() {
        if (degr_000001 == null) {
        	degr_000001 = new Degr_000001();
        	degr_000001.init();
        }
        return degr_000001;
    }
    
    private void init() {
    	id = "Degr_000001";
    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
    	left = new ArrayList<PhysicalEntity>();
    	left.add(Prot_000001.getInstance());
    	left.add(SmlM_000001.getInstance());
    	right = new ArrayList<PhysicalEntity>();
    	right.add(Prot_000002.getInstance());
    	participant_Conversion = null;
   }
}
=====================================================
 */
public class DegrGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(DegrGenerator.class);
	
	private Degradation degr = null;
	private List<String> leftList = new ArrayList<String>();
	private List<String> rightList = new ArrayList<String>();
	private List<String> stoiList = new ArrayList<String>();
	
	public DegrGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_DEGR;
		degr = (Degradation)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		childrenClassNames.put(ASSOC_CATS, cats);
		childrenClassNames.put(ASSOC_CAT_AGENTS, catAgents);
		new DegrAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(degr.getLeft());
		leftList.addAll(entityList);
		entityList.clear();
		consChildrenFromControllerSet(degr.getRight());
		rightList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(degr.getControlledOf());
		consChildrenFromUtilitySet(degr.getParticipantStoichiometry());
		stoiList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     private void init() {
	    	id = "Degr_000001";
	    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
	    	left = new ArrayList<PhysicalEntity>();
	    	left.add(Prot_000001.getInstance());
	    	left.add(SmlM_000001.getInstance());
	    	right = new ArrayList<PhysicalEntity>();
	    	right.add(Prot_000002.getInstance());
	    	participant_Conversion = null;
	   }
	 */
	
	private String generateDegrStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenConvStmts(degr));
    	
    	if (!leftList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(leftList, SUPER_CLASS_PHYE, "left"));
        }
        if (!rightList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(rightList, SUPER_CLASS_PHYE, "right"));
        }
        if (!stoiList.isEmpty()) {
	     	text.append(INDENT2);
	     	text.append(generateListItemsByListAddOp(stoiList, SUPER_CLASS_STOI, "participantStoichiometry"));
	 	}
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateDegrStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

