package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.ControlType;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.Degradation;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Modulation;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Transport;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.cats;

import java.util.ArrayList;

import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.Conversion;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.model.ints.*;

public class Mod_000011 extends Modulation {

    private static Mod_000011 mod_000011 = null;

    private Mod_000011() {
    }

    public static Mod_000011 getInstance() {
        if (mod_000011 == null) {
        	mod_000011 = new Mod_000011();
        	mod_000011.init();
        }
        return mod_000011;
    }
    
    private void init() {
    	id = "Mod_000011";
    	controller = new ArrayList<PhysicalEntity>();
    	controller.add(Prot_000011.getInstance());
    	controlled = new ArrayList<Catalysis>();
    	controlled.add(Cat_000011.getInstance());
   }
}
=====================================================
 */
public class ModGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(ModGenerator.class);
	
	private Modulation mod = null;
	private List<String> controllerList = new ArrayList<String>();
	private List<String> controlledList = new ArrayList<String>();
	
	public ModGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_TMPREG;
		mod = (Modulation)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		new ModAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(mod.getController());
		controllerList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(mod.getControlled());
		controlledList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	   private void init() {
	    	id = "Mod_000011";
	    	controller = new ArrayList<PhysicalEntity>();
	    	controller.add(Prot_000011.getInstance());
	    	controlled = new ArrayList<Catalysis>();
	    	controlled.add(Cat_000011.getInstance());
	   }
	 */
	
	private String generateModStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenCtrlStmts(mod));
    	
    	if (!controllerList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controllerList, SUPER_CLASS_PHYE, "controller"));
        }
        if (!controlledList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controlledList, SUPER_CLASS_CAT, "controlled"));
        }	
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateModStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

