package nl.vu.bioinf.ebf.model.codegen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Catalysis;
import org.biopax.paxtools.model.level3.ComplexAssembly;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.ControlType;
import org.biopax.paxtools.model.level3.Degradation;
import org.biopax.paxtools.model.level3.GeneticInteraction;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Modulation;
import org.biopax.paxtools.model.level3.MolecularInteraction;
import org.biopax.paxtools.model.level3.NucleicAcid;
import org.biopax.paxtools.model.level3.Pathway;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Complex;
import org.biopax.paxtools.model.level3.Controller;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.ConversionDirectionType;
import org.biopax.paxtools.model.level3.Dna;
import org.biopax.paxtools.model.level3.DnaRegion;
import org.biopax.paxtools.model.level3.Entity;
import org.biopax.paxtools.model.level3.Interaction;
import org.biopax.paxtools.model.level3.InteractionVocabulary;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Protein;
import org.biopax.paxtools.model.level3.Rna;
import org.biopax.paxtools.model.level3.RnaRegion;
import org.biopax.paxtools.model.level3.SmallMolecule;
import org.biopax.paxtools.model.level3.Stoichiometry;
import org.biopax.paxtools.model.level3.TemplateReaction;
import org.biopax.paxtools.model.level3.TemplateReactionRegulation;
import org.biopax.paxtools.model.level3.Transport;
import org.biopax.paxtools.model.level3.UtilityClass;


public abstract class ModelElementGenerator {
	private static Logger logger = Logger.getLogger(ModelElementGenerator.class);
	
	protected static int totalNumberOfInts = 0;
	
	private static final int INDENT = 4;
    protected static final String INDENT1 = generateIndentString (1 * INDENT);
    protected static final String INDENT2 = generateIndentString (2 * INDENT);
    protected static final String INDENT3 = generateIndentString (3 * INDENT);
    protected static final String INDENT4 = generateIndentString (4 * INDENT);
    
    protected static String JAVA_PACKAGE_LIST = "java.util.ArrayList"; 
    protected static String JAVA_LOGGER_LIST = "org.apache.log4j.Logger";
    
    // Packages for pathway and interaction classes where the generated code should be put. 
    protected static String MODEL_ENTITY_PACKAGE_PWAYS = GenerateModel.BASE_PACKAGE + ".pways";  
    protected static String MODEL_ENTITY_PACKAGE_INTS = GenerateModel.BASE_PACKAGE + ".ints"; 
    
    // Packages for the custome pathway and interaction agent classes which are extended by an OWL individual pathway and interaction agent. 
    protected static final String META_AGENT_PACKAGE_PWAYS = GenerateModel.META_PACKAGE + ".pathway.*";
    protected static final String META_AGENT_PACKAGE_INTS = GenerateModel.META_PACKAGE + ".interaction.*";
    
    // Packages for all already generated biopax entities/classes.
    protected static final String META_BIOPAX_PACKAGE_ALL = GenerateModel.META_PACKAGE + ".*";	  
    
    protected static final String SUB_PWAY_AGENTS = "subPWayAgents";
    protected static final String SUB_INT_AGENTS = "subIntAgents";
    
    protected static final String ASSOC_CTRLS = "ctrls";
    protected static final String ASSOC_CATS = "cats";
    protected static final String ASSOC_MODS = "mods";
    protected static final String ASSOC_TMPREGS = "tmpRegs";
    protected static final String ASSOC_PWAYS = "pways";
    
    protected static final String ASSOC_CTRL_AGENTS = "ctrlAgents";
    protected static final String ASSOC_CAT_AGENTS = "catAgents";
    protected static final String ASSOC_MOD_AGENTS = "modAgents";
    protected static final String ASSOC_TMPREG_AGENTS = "tmpRegAgents";
    protected static final String ASSOC_PWAY_AGENTS = "pwayAgents";
    
    
    protected static String GENERIC_AGENT = "GenericAgent";
    protected static final String SUPER_CLASS_ENTITY = "Entity";
    
    protected static final String PRE_CLASS_PWAYA = "PWayA_";
    protected static final String SUPER_CLASS_PWAYA = "PWayAgent";
    protected static final String SUPER_CLASS_MASTER_PWAYA = "PWayMasterAgent";
    protected static final String PRE_CLASS_PWAY = "PWay_";
    protected static final String SUPER_CLASS_PWAY = "Pathway";
    
    protected static final String PRE_CLASS_INT = "Int_";
    protected static final String SUPER_CLASS_INT = "Interaction";
    protected static final String PRE_CLASS_INTA = "IntA_";
    protected static final String SUPER_CLASS_INTA = "IntAgent";
    
    //===========================================================================================
    
    protected static final String PRE_CLASS_TMPREAC = "TmpReac_";
    protected static final String SUPER_CLASS_TMPREAC = "TemplateReaction";
    protected static final String PRE_CLASS_TMPREACA = "TmpReacA_";
    protected static final String SUPER_CLASS_TMPREACA = "TmpReacAgent";
    
    protected static final String PRE_CLASS_GINT = "GInt_";
    protected static final String SUPER_CLASS_GINT = "GeneticInteraction";
    protected static final String PRE_CLASS_GINTA = "GIntA_";
    protected static final String SUPER_CLASS_GINTA = "GIntAgent";
    
    protected static final String PRE_CLASS_MINT = "MInt_";
    protected static final String SUPER_CLASS_MINT = "MolecularInteraction";
    protected static final String PRE_CLASS_MINTA = "MIntA_";
    protected static final String SUPER_CLASS_MINTA = "MIntAgent";
    
    //===========================================================================================
    
    protected static final String PRE_CLASS_CONV = "Conv_";
    protected static final String SUPER_CLASS_CONV = "Conversion";
    protected static final String PRE_CLASS_CONVA = "ConvA_";
    protected static final String SUPER_CLASS_CONVA = "ConvAgent";
    
    protected static final String PRE_CLASS_BIOREAC = "BioReac_";
    protected static final String SUPER_CLASS_BIOREAC = "BiochemicalReaction";
    protected static final String PRE_CLASS_BIOREACA = "BioReacA_";
    protected static final String SUPER_CLASS_BIOREACA = "BioReacAgent";
    
    protected static final String PRE_CLASS_CMPASM = "CmpAsm_";
    protected static final String SUPER_CLASS_CMPASM = "ComplexAssembly";
    protected static final String PRE_CLASS_CMPASMA = "CmpAsmA_";
    protected static final String SUPER_CLASS_CMPASMA = "CmpAsmAgent";
    
    protected static final String PRE_CLASS_DEGR = "Degr_";
    protected static final String SUPER_CLASS_DEGR = "Degradation";
    protected static final String PRE_CLASS_DEGRA = "DegrA_";
    protected static final String SUPER_CLASS_DEGRA = "DegrAgent";
    
    protected static final String PRE_CLASS_TRANS = "Trans_";
    protected static final String SUPER_CLASS_TRANS = "Transport";
    protected static final String PRE_CLASS_TRANSA = "TransA_";
    protected static final String SUPER_CLASS_TRANSA = "TransAgent";
    
    //===========================================================================================
    
    protected static final String PRE_CLASS_CTRL = "Ctrl_";
    protected static final String SUPER_CLASS_CTRL = "Control";
    protected static final String PRE_CLASS_CTRLA = "CtrlA_";
    protected static final String SUPER_CLASS_CTRLA = "CtrlAgent";
    
    protected static final String PRE_CLASS_CAT = "Cat_";
    protected static final String SUPER_CLASS_CAT = "Catalysis";
    protected static final String PRE_CLASS_CATA = "CatA_";
    protected static final String SUPER_CLASS_CATA = "CatAgent";
    
    protected static final String PRE_CLASS_MOD = "Mod_";
    protected static final String SUPER_CLASS_MOD = "Modulation";
    protected static final String PRE_CLASS_MODA = "ModA_";
    protected static final String SUPER_CLASS_MODA = "ModAgent";
    
    protected static final String PRE_CLASS_TMPREG = "TmpReg_";
    protected static final String SUPER_CLASS_TMPREG = "TemplateReactionRegulation";
    protected static final String PRE_CLASS_TMPREGA = "TmpRegA_";
    protected static final String SUPER_CLASS_TMPREGA = "TmpRegAgent";
    
    //===========================================================================================
    
    protected static final String PRE_CLASS_PHYE = "Phye";
    protected static final String SUPER_CLASS_PHYE = "PhysicalEntity";
    protected static final String PRE_CLASS_PROT = "Prot_";
    protected static final String SUPER_CLASS_PROT = "Protein";
    protected static final String PRE_CLASS_CMP = "Cmp_";
    protected static final String SUPER_CLASS_CMP = "Complex";
    protected static final String PRE_CLASS_DNA = "Dna_";
    protected static final String SUPER_CLASS_DNA = "Dna";
    protected static final String PRE_CLASS_DNAREG = "DnaReg_";
    protected static final String SUPER_CLASS_DNAREG = "DnaRegion";
    protected static final String PRE_CLASS_RNA = "Rna_";
    protected static final String SUPER_CLASS_RNA = "Rna";
    protected static final String PRE_CLASS_RNAREG = "RnaReg_";
    protected static final String SUPER_CLASS_RNAREG = "RnaRegion";
    protected static final String PRE_CLASS_SMLM = "Smlm_";
    protected static final String SUPER_CLASS_SMLM = "SmallMolecule";
    
    //===========================================================================================
    
    protected static final String PRE_CLASS_STOI = "Stoi_";
    protected static final String SUPER_CLASS_STOI = "Stoichiometry";
    
    //===========================================================================================
    
    protected static final String VAR_PWAY = "pway";
    protected static final String VAR_GEN_INTR = "interaction";
    protected static final String VAR_INTR = "intr";
    protected static final String VAR_CTRL = "ctrl";
    protected static final String VAR_CAT = "cat";
    protected static final String VAR_MOD = "mod";
    protected static final String VAR_TMPREG = "tmpReg";
    protected static final String VAR_CONV = "conv";
    protected static final String VAR_BIOREAC = "bioReac";
    protected static final String VAR_DEGR = "degr";
    protected static final String VAR_TRANS = "trans";
    protected static final String VAR_TMPREAC = "tmpReac";
    protected static final String VAR_CMPASM = "cmpAsm";
    protected static final String VAR_ENTITY_ID = "id";
    protected static final String VAR_ENTITY_RDFID = "rdfId";
    protected static final String VAR_DISPLAY_NAME = "displayName";
    protected static final String VAR_STANDARD_NAME = "standardName";
    protected static final String VAR_SPONTANEOUS = "spontaneous";
    protected static final String VAR_CONV_DIR = "conversionDirection";
    protected static final String VAR_CAT_DIR = "catalysisDirection";
    protected static final String VAR_CTRL_TYPE = "controlType";
    
    protected class EntityIdContainer {
    	public String rdfIdForEntity = null;
    	public String displayNameForEntity = null;
    	public String classNumForEntity = null;
    	public String classNameForEntity = null;
    	public boolean doesExist = false;
    }
    private static Map<String, EntityIdContainer> entityIdMap = new HashMap<String, EntityIdContainer>();
    private static Map<String, Integer> classNumCounter = new HashMap<String, Integer>();
    static {
    	classNumCounter.put(PRE_CLASS_PWAY, 0);
    	classNumCounter.put(PRE_CLASS_INT, 0);
    	
    	classNumCounter.put(PRE_CLASS_TMPREAC, 0);
    	classNumCounter.put(PRE_CLASS_GINT, 0);
    	classNumCounter.put(PRE_CLASS_MINT, 0);
    	
    	classNumCounter.put(PRE_CLASS_CONV, 0);
    	classNumCounter.put(PRE_CLASS_BIOREAC, 0);
    	classNumCounter.put(PRE_CLASS_CMPASM, 0);
    	classNumCounter.put(PRE_CLASS_DEGR, 0);
    	classNumCounter.put(PRE_CLASS_TRANS, 0);
    	
    	classNumCounter.put(PRE_CLASS_CTRL, 0);
    	classNumCounter.put(PRE_CLASS_CAT, 0);
    	classNumCounter.put(PRE_CLASS_MOD, 0);
    	classNumCounter.put(PRE_CLASS_TMPREG, 0);
    	
    	classNumCounter.put(PRE_CLASS_PHYE, 0);
    	classNumCounter.put(PRE_CLASS_PROT, 0);
    	classNumCounter.put(PRE_CLASS_CMP, 0);
    	classNumCounter.put(PRE_CLASS_DNA, 0);
    	classNumCounter.put(PRE_CLASS_DNAREG, 0);
    	classNumCounter.put(PRE_CLASS_RNA, 0);
    	classNumCounter.put(PRE_CLASS_RNAREG, 0);
    	classNumCounter.put(PRE_CLASS_SMLM, 0);
    	
    	classNumCounter.put(PRE_CLASS_STOI, 0);
    }
    
    protected List<String> entityList = new ArrayList<String>();
    protected List<String> subPwayAgents = new ArrayList<String>();
    protected List<String> subIntAgents = new ArrayList<String>();
    
    protected List<String> ctrls = new ArrayList<String>();
    protected List<String> cats = new ArrayList<String>();
    protected List<String> mods = new ArrayList<String>();
    protected List<String> tmpRegs = new ArrayList<String>();
    //protected List<String> pways = new ArrayList<String>();
    
    protected List<String> ctrlAgents = new ArrayList<String>();
    protected List<String> catAgents = new ArrayList<String>();
    protected List<String> modAgents = new ArrayList<String>();
    protected List<String> tmpRegAgents = new ArrayList<String>();
    //protected List<String> pwayAgents = new ArrayList<String>();
    
	protected ArrayList<String> importStmts = new ArrayList<String>(); 
	protected String className = null;
	protected String superClassName = null;
	
	protected boolean implementsInt = false;
	protected String modelPackage = null;
	
	protected abstract void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception;
	
	public void generate(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		prepareGeneration(indType, entityClassName, childrenClassNames);
		calcTotalNumOfInts();
		generateJavaFile();
	}
	
	public void calcTotalNumOfInts() {
		totalNumberOfInts = 
				classNumCounter.get(PRE_CLASS_INT) +
				classNumCounter.get(PRE_CLASS_TMPREAC) +
				classNumCounter.get(PRE_CLASS_GINT) +
				classNumCounter.get(PRE_CLASS_MINT) +
				classNumCounter.get(PRE_CLASS_CONV) +
				classNumCounter.get(PRE_CLASS_BIOREAC) +
				classNumCounter.get(PRE_CLASS_CMPASM) +
				classNumCounter.get(PRE_CLASS_DEGR) +
				classNumCounter.get(PRE_CLASS_TRANS) +
				classNumCounter.get(PRE_CLASS_CTRL) +
				classNumCounter.get(PRE_CLASS_CAT) +
				classNumCounter.get(PRE_CLASS_MOD) +
				classNumCounter.get(PRE_CLASS_TMPREG);
	}
	
	public void logStatistics() {
	     logger.info("Number of generated Pathways: " + classNumCounter.get(PRE_CLASS_PWAY));
	     logger.info("Number of generated Interactions: " + classNumCounter.get(PRE_CLASS_INT));
	     
	     logger.info("Number of generated TemplateReactions: " + classNumCounter.get(PRE_CLASS_TMPREAC));
	     logger.info("Number of generated Genetic Interactions: " + classNumCounter.get(PRE_CLASS_GINT));
	     logger.info("Number of generated Molecular Interactions: " + classNumCounter.get(PRE_CLASS_MINT));
	     
	     logger.info("Number of generated Conversions: " + classNumCounter.get(PRE_CLASS_CONV));
	     logger.info("Number of generated BiochemicalReactions: " + classNumCounter.get(PRE_CLASS_BIOREAC));
	     logger.info("Number of generated ComplexAssemblies: " + classNumCounter.get(PRE_CLASS_CMPASM));
	     logger.info("Number of generated Degradations: " + classNumCounter.get(PRE_CLASS_DEGR));
	     logger.info("Number of generated Transports: " + classNumCounter.get(PRE_CLASS_TRANS));
	     
	     logger.info("Number of generated Controls: " + classNumCounter.get(PRE_CLASS_CTRL));
	     logger.info("Number of generated Catalysis: " + classNumCounter.get(PRE_CLASS_CAT));
	     logger.info("Number of generated Modulations: " + classNumCounter.get(PRE_CLASS_MOD));
	     logger.info("Number of generated TemplateReactionRegulations: " + classNumCounter.get(PRE_CLASS_TMPREG));
	     
	     logger.info("Number of generated PhysicalEntities: " + classNumCounter.get(PRE_CLASS_PHYE));
	     logger.info("Number of generated Proteins: " + classNumCounter.get(PRE_CLASS_PROT));
	     logger.info("Number of generated Complexes: " + classNumCounter.get(PRE_CLASS_CMP));
	     logger.info("Number of generated DNA's: " + classNumCounter.get(PRE_CLASS_DNA));
	     logger.info("Number of generated DNARegions: " + classNumCounter.get(PRE_CLASS_DNAREG));
	     logger.info("Number of generated RNA's: " + classNumCounter.get(PRE_CLASS_RNA));
	     logger.info("Number of generated RNARegions: " + classNumCounter.get(PRE_CLASS_RNAREG));
	     logger.info("Number of generated SmallMolecules: " + classNumCounter.get(PRE_CLASS_SMLM));
	     
	     logger.info("Number of generated Stoichiometries: " + classNumCounter.get(PRE_CLASS_STOI));
	}
	
	protected EntityIdContainer getEntityIdContainer(String preClassName, String rdfId, String displayName) throws Exception {
		EntityIdContainer entityIdContainer = entityIdMap.get(rdfId);
		if (entityIdContainer == null) {
			entityIdContainer = new EntityIdContainer();
			entityIdContainer.rdfIdForEntity = rdfId;
			entityIdContainer.displayNameForEntity = displayName;
			int classNum = classNumCounter.get(preClassName);
			classNum++;
			classNumCounter.put(preClassName, classNum);
			entityIdContainer.classNumForEntity = String.format("%06d", classNum);
			entityIdContainer.classNameForEntity = preClassName +  entityIdContainer.classNumForEntity;
			entityIdMap.put(rdfId, entityIdContainer);
		}
		else {
			entityIdContainer.doesExist = true;
		}
		return entityIdContainer;
	}
	
	public void initiateExpressions(Properties initialExpr, String exprFileName) throws Exception {
		Properties concentrationProperties = new Properties();
		for (EntityIdContainer entityIdContainer: entityIdMap.values()) {
			String key = entityIdContainer.classNameForEntity;
			if (key.startsWith(PRE_CLASS_PROT) || key.startsWith(PRE_CLASS_CMP) ||
				key.startsWith(PRE_CLASS_DNA) || key.startsWith(PRE_CLASS_DNAREG) || 
				key.startsWith(PRE_CLASS_RNA) || key.startsWith(PRE_CLASS_RNA)) {
				
				String displayName = entityIdContainer.displayNameForEntity;
				String fullyQualifiedKey = key + "(" + displayName + ")";
				String concentrationValue = String.valueOf(0);
				try {
					concentrationValue = initialExpr.get(displayName).toString();
				}
				catch (Exception e) {
					// Never mind. Either there is no initial concentrations or the current physical entity is missing in the initial concentrations. 
				}
				concentrationProperties.put(fullyQualifiedKey, concentrationValue);
			}	
		}		
		concentrationProperties.store(new FileOutputStream(exprFileName), null);
	}
	
	protected void consChildEntity(Level3Element childEntity, String preClass, String preClassAgent) throws Exception {
		// Determine the unique name of the child (class).
		String rdfId = childEntity.getRDFId();
		String displayName = "";
		
		if (childEntity instanceof Entity) {
			displayName = ((Entity)childEntity).getDisplayName();
		}
		
		EntityIdContainer entityIdContainer = getEntityIdContainer(preClass, rdfId, displayName);
		String classNumForEntity = entityIdContainer.classNumForEntity;
		String childClassName = entityIdContainer.classNameForEntity;
		
		// Use the same class-number for both entity and its agent. Note that the entity has no agent if it is a physical entity.
		String childAgentName = preClassAgent + classNumForEntity;
		entityList.add(childClassName);
		
		switch (preClass) {
		case PRE_CLASS_PWAY:
			subPwayAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new PWayGenerator().generate(childEntity, childClassName, null);
			}	
			break;
		case PRE_CLASS_INT: 
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new IntGenerator().generate(childEntity, childClassName, null);
			}	
			break;
		//============================================================================================	
		case PRE_CLASS_TMPREAC:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new TmpReacGenerator().generate(childEntity, childClassName, null);
			}	
			break;
		case PRE_CLASS_GINT:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				//new GIntGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_MINT:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new MIntGenerator().generate(childEntity, childClassName, null);
			}	
			break;		
		//============================================================================================	
		case PRE_CLASS_CONV:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new ConvGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_BIOREAC:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new BioReacGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_CMPASM:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new CmpAsmGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_DEGR:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new DegrGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_TRANS:
			subIntAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new TransGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		//=============================================================================================	
		case PRE_CLASS_CTRL:
			subIntAgents.add(childAgentName);
			ctrls.add(childClassName);
			ctrlAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new CtrlGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_CAT:
			subIntAgents.add(childAgentName);
			cats.add(childClassName);
			catAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new CatGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_MOD:
			subIntAgents.add(childAgentName);
			mods.add(childClassName);
			modAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new ModGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_TMPREG:
			subIntAgents.add(childAgentName);
			tmpRegs.add(childClassName);
			tmpRegAgents.add(childAgentName);
			if (!entityIdContainer.doesExist) {
				new TmpRegGenerator().generate(childEntity, childClassName, null);
			}	
			break;		
		//=============================================================================================	
		case PRE_CLASS_PHYE:
			if (!entityIdContainer.doesExist) {
				new PhyeGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_PROT:
			if (!entityIdContainer.doesExist) {
				new ProtGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_CMP:
			if (!entityIdContainer.doesExist) {
				new CmpGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_DNA:
			if (!entityIdContainer.doesExist) {
				new DnaGenerator().generate(childEntity, childClassName, null);
			}
			break;	
		case PRE_CLASS_DNAREG:
			if (!entityIdContainer.doesExist) {
				new DnaRegGenerator().generate(childEntity, childClassName, null);
			}	
			break;
		case PRE_CLASS_RNA:
			if (!entityIdContainer.doesExist) {
				new RnaGenerator().generate(childEntity, childClassName, null);
			}
			break;	
		case PRE_CLASS_RNAREG:
			if (!entityIdContainer.doesExist) {
				new RnaRegGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		case PRE_CLASS_SMLM:
			if (!entityIdContainer.doesExist) {
				new SmlmGenerator().generate(childEntity, childClassName, null);
			}	
			break;	
		//=============================================================================================	
		case PRE_CLASS_STOI:
			if (!entityIdContainer.doesExist) {
				new StoiGenerator().generate(childEntity, childClassName, null);
			}	
			break;
		default:
			break;
		}
	}
	
	protected void consNucleicAcidChild(NucleicAcid nucleicAcid) throws Exception {
		if (nucleicAcid instanceof Dna) {
			consChildEntity(nucleicAcid, PRE_CLASS_DNA, null);
		}
		else if (nucleicAcid instanceof DnaRegion) {
			consChildEntity(nucleicAcid, PRE_CLASS_DNAREG, null);
		}
		else if (nucleicAcid instanceof Rna) {
			consChildEntity(nucleicAcid, PRE_CLASS_RNA, null);
		}
		else if (nucleicAcid instanceof RnaRegion) {
			consChildEntity(nucleicAcid, PRE_CLASS_RNAREG, null);
		}
	}
	
	protected void consChildrenFromControllerSet(Set<? extends Controller> controllers) throws Exception {
		for (Controller controller: controllers) {
			if (controller instanceof Protein) {
				consChildEntity(controller, PRE_CLASS_PROT, null);
			}
			else if (controller instanceof Complex) {
				consChildEntity(controller, PRE_CLASS_CMP, null);
			}
			else if (controller instanceof NucleicAcid) {
				consNucleicAcidChild((NucleicAcid)controller);
			}
			else if (controller instanceof SmallMolecule) {
				consChildEntity(controller, PRE_CLASS_SMLM, null);
			}
			else if (controller instanceof PhysicalEntity) {
				consChildEntity(controller, PRE_CLASS_PHYE, null);
			}
			//else if (controller instanceof Pathway) {
			//	consChildEntity(controller, PRE_CLASS_PWAY, null);
			//}
		}
	}
	
	protected void consChildrenFromProcessSet(Set<? extends Process> processes) throws Exception {
		for (Process process: processes) {
			//logger.debug("%%Type of interaction (process): " + process.getClass().getSimpleName());
			if (process instanceof Catalysis) {
				consChildEntity(process, PRE_CLASS_CAT, PRE_CLASS_CATA);
			}
			else if (process instanceof TemplateReactionRegulation) {
				consChildEntity(process, PRE_CLASS_TMPREG, PRE_CLASS_TMPREGA);
			}
			else if (process instanceof Modulation) {
				consChildEntity(process, PRE_CLASS_MOD, PRE_CLASS_MODA);
			}
			//===========================================================================
			else if (process instanceof BiochemicalReaction) {
				consChildEntity(process, PRE_CLASS_BIOREAC, PRE_CLASS_BIOREACA);
			}
			else if (process instanceof ComplexAssembly) {
				consChildEntity(process, PRE_CLASS_CMPASM, PRE_CLASS_CMPASMA);
			}
			else if (process instanceof Degradation) {
				consChildEntity(process, PRE_CLASS_DEGR, PRE_CLASS_DEGRA);
			}
			else if (process instanceof Transport) {
				consChildEntity(process, PRE_CLASS_TRANS, PRE_CLASS_TRANSA);
			}
			//===========================================================================
			else if (process instanceof TemplateReaction) {
				consChildEntity(process, PRE_CLASS_TMPREAC, PRE_CLASS_TMPREACA);
			}
			else if (process instanceof GeneticInteraction) {
				consChildEntity(process, PRE_CLASS_GINT, PRE_CLASS_GINTA);
			}
			else if (process instanceof MolecularInteraction) {
				consChildEntity(process, PRE_CLASS_MINT, PRE_CLASS_MINTA);
			}
			//===========================================================================
		    else if (process instanceof Conversion) {
				consChildEntity(process, PRE_CLASS_CONV, PRE_CLASS_CONVA);
			}
			else if (process instanceof Control) {
				consChildEntity(process, PRE_CLASS_CTRL, PRE_CLASS_CTRLA);
			}
			else if (process instanceof Interaction) {
				consChildEntity(process, PRE_CLASS_INT, PRE_CLASS_INTA);
			}
			else if (process instanceof Pathway) {
				consChildEntity(process, PRE_CLASS_PWAY, PRE_CLASS_PWAYA);
			}
		}
	}
	
	protected void consChildrenFromUtilitySet(Set<? extends UtilityClass> utilities) throws Exception {
		for (UtilityClass utility: utilities) {
			if (utility instanceof Stoichiometry) {
				consChildEntity(utility, PRE_CLASS_STOI, null);
			}
		}
	}
	
	protected String generateInitMethod() {
		return "";
	}
	
	protected String generateCustomMethod() {
		return "";
	}
	
	protected String generateMainMethod() {
		return "";
	}
	
	protected String getPlainClassName (String name) {
        String[] tokens = name.split(":");
        return tokens[tokens.length-1];
    }
	
	private static String generateIndentString (int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            sb.append (' ');
        }    
        return sb.toString();
    }
	
	protected static String upperCaseFirst(String s) {
        return "" + Character.toUpperCase(s.charAt(0)) + s.substring (1);
    }
	
	protected static String lowerCaseFirst(String s) {
        return "" + Character.toLowerCase(s.charAt(0)) + s.substring (1);
    }
	
	protected String generateGIntrInitMethod(String entityClassName, String intrVarName) {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(intrVarName + " = " + entityClassName +  ".getInstance();\n");
        text.append(INDENT2);
        text.append(VAR_GEN_INTR + " = " + intrVarName +  ";\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
	
	protected String generateListItemsByListAddOp(List<String> list, String listClass, String listName) {
    	StringBuffer text = new StringBuffer ();

    	text.append(listName + " = new ArrayList<" + listClass +  ">();\n");
    	for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
			String item = iterator.next();
			text.append(INDENT2);
	        text.append(listName + ".add(" + item + ".getInstance());\n");
		}
    	
        return text.toString ();
	}
	
	protected String generateSetItemsByListAddOp(Set<String> set, String setClass, String setName) {
    	StringBuffer text = new StringBuffer ();

    	text.append(setName + " = new ArrayList<" + setClass +  ">();\n");
    	for (Iterator<String> iterator = set.iterator(); iterator.hasNext();) {
			String item = iterator.next();
			text.append(INDENT2);
	        text.append(setName + ".add(\"" + item + "\");\n");
		}
    	
        return text.toString ();
	}
	
	/*
	 * controlled_Interaction = TmpReac_000001.getInstance();
	 * controlled_Interaction = Cat_000001.getInstance();
	 * controlled_Interaction = Conv_000001.getInstance();
	 */
	protected String generateListItems(List<String> list, String listName) {
    	StringBuffer text = new StringBuffer ();

    	for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
			String item = iterator.next();
			text.append(INDENT2);
			text.append(listName + " = " + item +  ".getInstance();\n");
		}
    	
        return text.toString ();
	}
	
	/*
	 * Both org.biopax.paxtools.model.level3.Entity and org.biopax.paxtools.model.level3.UtilityClass are sub-classes of
	 * org.biopax.paxtools.model.level3.Level3Element.
	 */
	protected String generateBasicEntityStmts(Level3Element indType) {
    	StringBuffer text = new StringBuffer();
    	text.append(VAR_ENTITY_ID + " = \"" + className +  "\";\n");
    	if (indType != null) {
	    	text.append(INDENT2);
	    	text.append(VAR_ENTITY_RDFID + " = \"" + indType.getRDFId() +  "\";\n");
    	}	
        return text.toString();
	}
	
	protected String generateEntityStmts(Entity indType) {
    	StringBuffer text = new StringBuffer();
    	text.append(generateBasicEntityStmts(indType));
    	
    	String displayName = indType.getDisplayName();
    	if (displayName != null) {
    		text.append(INDENT2);
    		text.append(VAR_DISPLAY_NAME + " = \"" + displayName +  "\";\n");
    	}
    	
    	String standardName = indType.getStandardName();
    	if (standardName != null) {
    		text.append(INDENT2);
    		text.append(VAR_STANDARD_NAME + " = \"" + standardName +  "\";\n");
    	}
    	
        return text.toString ();
	}
	
	protected String generateGenIntStmts(Interaction intr) {
    	StringBuffer text = new StringBuffer();
    	text.append(generateEntityStmts(intr));
    	
    	Set<InteractionVocabulary> interactionTypeSet = intr.getInteractionType();
    	if (interactionTypeSet != null) {
    		// text.append(INDENT2);
    		// ???
    	}
    	
        return text.toString();
	}
	
	protected String generateGenCtrlStmts(Control ctrl) {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenIntStmts(ctrl));
    	
    	ControlType ctrlType = ctrl.getControlType();
    	if (ctrlType != null) {
    		text.append(INDENT2);
    		text.append(VAR_CTRL_TYPE + " = Enum_controlType." + ctrlType +  ";\n");
    	}
    	
        return text.toString();
	}
	
	protected String generateGenConvStmts(Conversion conv) {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenIntStmts(conv));
    	boolean spontaneous = Boolean.TRUE.equals(conv.getSpontaneous()) ? true : false;
    	if (spontaneous) {
    		text.append(INDENT2);
    		text.append(VAR_SPONTANEOUS + " = " + spontaneous +  ";\n");
    	}	
    	
        ConversionDirectionType conversionDirection = conv.getConversionDirection();
        if (conversionDirection != null) {
	        text.append(INDENT2);
	        text.append(VAR_CONV_DIR + " = Enum_conversionDirection." + conversionDirection +  ";\n");
        }    
    	
        return text.toString ();
	}
	
	protected String generateGenPhysEntityStmts(PhysicalEntity physEntity) {
    	StringBuffer text = new StringBuffer();
    	text.append(generateEntityStmts(physEntity));
    	
    	/*
    	Set<PhysicalEntity> memberPhysicalEntitySet = physEntity.getMemberPhysicalEntity(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_PHYE, "memberPhysicalEntity"));
    	}
    	*/	
    	
    	
        return text.toString ();
	}
	
    /*
     * Generate this:
       package nl.vu.bioinf.ebf.model.test.ints;
     */
    private String generatePackageStmt() {
    	return "package " + modelPackage + ";\n\n";
    }

    /*
     * Generate this:
       import nl.vu.bioinf.ebf.model.test.ints.INT_000001;
     */
    private String generateImports() {
        StringBuffer text = new StringBuffer();

        if (!importStmts.isEmpty()) {
            for (String s : importStmts) {
                text.append ("import ");
                text.append (s);
                text.append (";\n");
            }
            text.append ("\n");
        }

        return text.toString();
    }

    private String generateClassHeading() {
        StringBuffer text = new StringBuffer();
        
        String javaClassName = upperCaseFirst(className);

        text.append("public class ");
        if (implementsInt) {
        	text.append(javaClassName + " implements " + superClassName + " {\n\n");
        }
        else {
        	text.append(javaClassName + " extends " + superClassName + " {\n\n");
        }
        return text.toString();
    }
    
    protected String generateStaticFields() {
    	return "";
    }
    
    /*
     * Generate this:
	    private static PWay_000001 pway_000001 = null;

	    private PWay_000001() {
	    }
	
	    public static PWay_000001 getInstance() {
	        if (pway_000001 == null) {
	        	pway_000001 = new PWay_000001();
	        	pway_000001.init();
	        }
	        return pway_000001;
	    }
     */
    protected String generateSingleton() {
        StringBuffer text = new StringBuffer ();

        String javaClassName = upperCaseFirst(className);
        String javaVarName = lowerCaseFirst(className);
        
        text.append(INDENT1);
        text.append ("private static ");
        text.append (javaClassName + " " + javaVarName + " = null;\n\n");
        
        text.append(INDENT1);
        text.append ("private " + javaClassName + "() {\n");
        text.append(INDENT1);
        text.append ("}\n\n");
        
        text.append(INDENT1);
        text.append ("public static " + javaClassName + " getInstance() {\n");
        text.append(INDENT2);
        text.append ("if (" + javaVarName + " == null) {\n");
        text.append(INDENT3);
        text.append (javaVarName + " = new " + javaClassName + "();\n");
        text.append(INDENT3);
        text.append (javaVarName + ".init();\n");
        text.append(INDENT2);
        text.append ("}\n");
        text.append(INDENT2);
        text.append ("return " + javaVarName + ";\n");
        text.append(INDENT1);
        text.append ("}\n\n");
        
        return text.toString ();
    }

    private String generateJavaClass() {
        StringBuffer text = new StringBuffer();
        
        text.append ( "/**\n" + 
                " * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).\n"+
                " * DO NOT EDIT this Base class.\n */\n\n");
        
        text.append(generatePackageStmt());
        text.append(generateImports());
        text.append(generateClassHeading());
        text.append(generateStaticFields());
        text.append(generateSingleton());
        text.append(generateInitMethod());
        text.append(generateCustomMethod());
        text.append(generateMainMethod());
        
        text.append ("}\n");
        return text.toString();
    }
    
    private File generateDirFromPackageName (File basedir, String name) throws Exception {
	    // substitute package separator char (.) with file separator char
	    StringBuffer dirName = new StringBuffer(name);
	    for (int i = 0; i < dirName.length(); i++) {
	        if (dirName.charAt(i) == '.') {
	            dirName.setCharAt(i, File.separatorChar);
	        }    
	    }
	
	    // create the new directory
	    File dir = new File (basedir.getPath() + File.separator + dirName.toString());
	    dir.mkdirs();
	    return dir;
	}

    
    private void generateJavaFile() throws Exception {
    	logger.info("Generating Java class for the individual '" + className + "'...");
    	
        File dir = new File (GenerateModel.sourceOutputDir);
        if (!dir.isDirectory()) {
            logger.error("Cannot output to directory " + GenerateModel.sourceOutputDir);
            throw new Exception ("Cannot output to directory " + GenerateModel.sourceOutputDir);
        }
        dir = generateDirFromPackageName(dir, modelPackage);

        File javaFile = new File (dir.getPath() + File.separator + upperCaseFirst(className) + ".java");
        
        // generate file contents
        String fileContent = generateJavaClass();

        PrintWriter out = new PrintWriter(javaFile);
        out.println(fileContent);
        out.flush();
        out.close();
    }
}
