package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.pways;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.interaction.*;
import nl.vu.bioinf.ebf.metamodel.pathway.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

// Pathway: display-name
public class PWayA_000001 extends PWayAgent {

    private static PWayA_000001 pwayA_000001 = null;

    private PWayA_000001() {
    }

    public static PWayA_000001 getInstance() {
        if (pwayA_000001 == null) {
        	pwayA_000001 = new PWayA_000001();
        	pwayA_000001.init();
        }
        return pwayA_000001;
    }
    
    private void init() {
    	pway = PWay_000001.getInstance();
    }

    protected void initAgent() {
        subPWayAgents = new ArrayList<PWayAgent>();
        subPWayAgents.add(PWayA_000011.getInstance());
        subPWayAgents.add(PWayA_000012.getInstance());
        subIntAgents = new ArrayList<GenericAgent>();
        subIntAgents.add(BioReacA_000001.getInstance());
        subIntAgents.add(TmpReacA_000001.getInstance());
        subIntAgents.add(TmpRegA_000001.getInstance());
        subIntAgents.add(CtrlA_000001.getInstance());
        super.initAgent();
    }
}
=====================================================
 */
public class PWayAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(PWayAGenerator.class);
	
	private String entityClassName = null;
	
	public PWayAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_PWAYS;
		
		// Get classNum from the end of entity name (e.g., PWay_000001) and append it at the end of its agent (e.g., PWayA_000001).
		String[] tokens = entityClassName.split("_");
		String entityClassNum = tokens[1];
		className = PRE_CLASS_PWAYA + entityClassNum;
		
		superClassName = SUPER_CLASS_PWAYA;
		subPwayAgents = childrenClassNames.get(SUB_PWAY_AGENTS);
		subIntAgents = childrenClassNames.get(SUB_INT_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_AGENT_PACKAGE_PWAYS);
		importStmts.add(META_AGENT_PACKAGE_INTS);
		importStmts.add(MODEL_ENTITY_PACKAGE_INTS + ".*");
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
    		pway = PWay_000001.getInstance();
    	}
	 */
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(VAR_PWAY + " = " + entityClassName +  ".getInstance();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     protected void initAgent() {
        	subPWayAgents = new ArrayList<PWayAgent>();
        	subPWayAgents.add(PWayA_000011.getInstance());
        	subPWayAgents.add(PWayA_000012.getInstance());
        	subIntAgents = new ArrayList<GenericAgent>();
        	subIntAgents.add(BioReacA_000001.getInstance());
        	subIntAgents.add(TmpReacA_000001.getInstance());
        	subIntAgents.add(TmpRegA_000001.getInstance());
        	subIntAgents.add(CtrlA_000001.getInstance());
        	super.initAgent();
    }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        if (!subPwayAgents.isEmpty()) {
        	 text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subPwayAgents, SUPER_CLASS_PWAYA, SUB_PWAY_AGENTS));
        }
        if (!subIntAgents.isEmpty()) {
        	 text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subIntAgents, GENERIC_AGENT, SUB_INT_AGENTS));
        }	
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

