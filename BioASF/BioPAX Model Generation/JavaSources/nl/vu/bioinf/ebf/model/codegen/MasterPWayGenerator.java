package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Pathway;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.pways;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

// Pathway: display-name
public class PWay_Master extends Pathway {

    private static PWay_Master pway_Master = null;

    private PWay_Master() {
    }

    public static PWay_Master getInstance() {
        if (pway_Master == null) {
        	pway_Master = new PWay_Master();
        	pway_Master.init();
        }
        return pway_Master;
    }
    
    private void init() {
    	 id = "PWay_Master";
    	 pathwayComponent_Pathway = new ArrayList<Pathway>();
    	 pathwayComponent_Pathway.add(PWay_000001.getInstance());
    }
}
=====================================================
 */
public class MasterPWayGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(MasterPWayGenerator.class);
	
	private List<String> subPwayList = new ArrayList<String>();
	
	public MasterPWayGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_PWAYS;
		className = entityClassName;
		superClassName = SUPER_CLASS_PWAY;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(SUB_PWAY_AGENTS, subPwayAgents);
		new MasterPWayAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	private void consChildren() throws Exception {
		// All top level pathways in a biopax model will be children of the Master pathway.
		Set<Pathway> childrenPways = GenerateModel.biopaxModel.getObjects(Pathway.class);
    	consChildrenFromProcessSet(childrenPways);
    	subPwayList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("public void init() {\n");
        text.append(INDENT2);
        text.append(generateBasicEntityStmts(null));
        if (!subPwayList.isEmpty()) {
        	 text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subPwayList, SUPER_CLASS_PWAY, "pathwayComponent_Pathway"));
        }
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

