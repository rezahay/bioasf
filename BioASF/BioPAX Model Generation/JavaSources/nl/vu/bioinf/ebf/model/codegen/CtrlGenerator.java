package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.Level3Element;


/*
 * This class generates the following sample code:
=====================================================
public class Ctrl_000001 extends Control {

    private static Ctrl_000001 ctrl_000001 = null;

    private Ctrl_000001() {
    }

    public static Ctrl_000001 getInstance() {
        if (ctrl_000001 == null) {
        	ctrl_000001 = new Ctrl_000001();
        	ctrl_000001.init();
        }
        return ctrl_000001;
    }
    
    private void init() {
    	id = "ctrl_000001";
    	controller_PhysicalEntity = new ArrayList<PhysicalEntity>();
    	controller_PhysicalEntity.add(Prot_000004.getInstance());
    	controlled_Interaction = TmpReac_000001.getInstance();
    	controlType = Enum_controlType.INHIBITION;
   }
}
=====================================================
 */
public class CtrlGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(CtrlGenerator.class);
	
	private Control ctrl = null;
	private List<String> controllerList = new ArrayList<String>();
	private List<String> controlledList = new ArrayList<String>();
	
	public CtrlGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_CTRL;
		ctrl = (Control)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		//childrenClassNames.put(ASSOC_PWAYS, pways);
		new CtrlAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(ctrl.getController());
		controllerList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(ctrl.getControlledOf());
		consChildrenFromProcessSet(ctrl.getControlled());
		controlledList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "ctrl_000001";
	    	controller_PhysicalEntity = new ArrayList<PhysicalEntity>();
	    	controller_PhysicalEntity.add(Prot_000004.getInstance());
	    	controlled_Interaction = TmpReac_000001.getInstance();
	    	controlType = Enum_controlType.INHIBITION;
	   }
	 */
	
	private String generateCtrlStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenCtrlStmts(ctrl));
    	if (!controllerList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controllerList, SUPER_CLASS_PHYE, "controller_PhysicalEntity"));
        }
        if (!controlledList.isEmpty()) {
        	//text.append(INDENT2);
        	text.append(generateListItems(controlledList, "controlled_Interaction"));
        }	
    	
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateCtrlStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

