package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Rna;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.prots;

import nl.vu.bioinf.ebf.metamodel.*;

public class Rna_000001 extends Rnaion {

    private static Rna_000001 Rna_000001 = null;

    private Rna_000001() {
    }

    public static Rna_000001 getInstance() {
        if (Rna_000001 == null) {
        	Rna_000001 = new Rna_000001();
        	Rna_000001.init();
        }
        return Rna_000001;
    }
    
    private void init() {
    	id = "Rna_000001";
    	memberPhysicalEntity_Rnaion = null;
        entityReference = null;
        cellularLocation = null;
        displayName = "bcat";
   }
}
=====================================================
 */
public class RnaGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(RnaGenerator.class);
	
	private Rna rna = null;
	
	public RnaGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_RNA;
		rna = (Rna)indType;
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "rna_000001";
	    	memberPhysicalEntity_Rnaion = null;
	        entityReference = null;
	        cellularLocation = null;
	        displayName = "bcat";
	   }
	 */
	
	private String generateRnaStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenPhysEntityStmts(rna));
    	
    	/*
    	// Strange thing is that paxtools does not return a set for getEntityReference. However, it is not restricted by cardinality and
    	// therefore we have generated code which expects a set. 
    	Set<ProteinReference> entityReference = rna.getEntityReference(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_??, "memberPhysicalEntity"));
    	}
    	*/
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateRnaStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

