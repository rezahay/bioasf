package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Dna;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.prots;

import nl.vu.bioinf.ebf.metamodel.*;

public class Dna_000001 extends Dnaion {

    private static Dna_000001 Dna_000001 = null;

    private Dna_000001() {
    }

    public static Dna_000001 getInstance() {
        if (Dna_000001 == null) {
        	Dna_000001 = new Dna_000001();
        	Dna_000001.init();
        }
        return Dna_000001;
    }
    
    private void init() {
    	id = "Dna_000001";
    	memberPhysicalEntity_Dnaion = null;
        entityReference = null;
        cellularLocation = null;
        displayName = "bcat";
   }
}
=====================================================
 */
public class DnaGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(DnaGenerator.class);
	
	private Dna dna = null;
	
	public DnaGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_DNA;
		dna = (Dna)indType;
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "dna_000001";
	    	memberPhysicalEntity_Dnaion = null;
	        entityReference = null;
	        cellularLocation = null;
	        displayName = "bcat";
	   }
	 */
	
	private String generateDnaStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenPhysEntityStmts(dna));
    	
    	/*
    	// Strange thing is that paxtools does not return a set for getEntityReference. However, it is not restricted by cardinality and
    	// therefore we have generated code which expects a set. 
    	Set<ProteinReference> entityReference = dna.getEntityReference(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_??, "memberPhysicalEntity"));
    	}
    	*/
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateDnaStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

