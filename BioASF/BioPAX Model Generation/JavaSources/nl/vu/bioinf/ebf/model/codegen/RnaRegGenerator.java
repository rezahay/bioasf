package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.RnaRegion;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.prots;

import nl.vu.bioinf.ebf.metamodel.*;

public class RnaReg_000001 extends RnaRegion {

    private static RnaReg_000001 RnaReg_000001 = null;

    private RnaReg_000001() {
    }

    public static RnaReg_000001 getInstance() {
        if (RnaReg_000001 == null) {
        	RnaReg_000001 = new RnaReg_000001();
        	RnaReg_000001.init();
        }
        return RnaReg_000001;
    }
    
    private void init() {
    	id = "RnaReg_000001";
    	memberPhysicalEntity_RnaRegion = null;
        entityReference = null;
        cellularLocation = null;
        displayName = "bcat";
   }
}
=====================================================
 */
public class RnaRegGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(RnaRegGenerator.class);
	
	private RnaRegion rnaReg = null;
	
	public RnaRegGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_RNAREG;
		rnaReg = (RnaRegion)indType;
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "rnaReg_000001";
	    	memberPhysicalEntity_RnaRegion = null;
	        entityReference = null;
	        cellularLocation = null;
	        displayName = "bcat";
	   }
	 */
	
	private String generateRnaRegStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenPhysEntityStmts(rnaReg));
    	
    	/*
    	// Strange thing is that paxtools does not return a set for getEntityReference. However, it is not restricted by cardinality and
    	// therefore we have generated code which expects a set. 
    	Set<ProteinReference> entityReference = rnaReg.getEntityReference(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_??, "memberPhysicalEntity"));
    	}
    	*/
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateRnaRegStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

