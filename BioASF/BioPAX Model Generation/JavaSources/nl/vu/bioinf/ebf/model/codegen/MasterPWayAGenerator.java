package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import nl.vu.bioinf.ebf.metamodel.pathway.PWayMasterAgent;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.pways;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.pathway.*;

// Pathway: Master
public class PWayA_Master extends PWayAgent {

	public static final int totalNumberOfInts = 50;
    private static PWayA_Master pwayA_Master = null;

    private PWayA_Master() {
    }

    public static PWayA_Master getInstance() {
        if (pwayA_Master == null) {
        	pwayA_Master = new PWayA_Master();
        	pwayA_Master.init();
        }
        return pwayA_Master;
    }
    
    private void init() {
    	pway = PWay_Master.getInstance();
    }
    
    protected void reset() {
    	pWayA_Master = null;
    }

    protected void initAgent() {
        subPWayAgents = new ArrayList<PWayAgent>();
        subPWayAgents.add(PWayA_000001.getInstance());
        subIntAgents = null;
        super.initAgent();
    }

    public static void main(String [] args) {
        try {
        	PWayA_Master.getInstance().handleArgs(args);
            PWayA_Master.getInstance().start(null);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
}
=====================================================
 */
public class MasterPWayAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(MasterPWayAGenerator.class);
	
	private String entityClassName = null;
	
	public MasterPWayAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_PWAYS;
		className = GenerateModel.MASTER_PWAYA;
		superClassName = SUPER_CLASS_MASTER_PWAYA;
		subPwayAgents = childrenClassNames.get(SUB_PWAY_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_AGENT_PACKAGE_PWAYS);
		importStmts.add(JAVA_LOGGER_LIST);
	}
	
	/*
	 * public static final int totalNumberOfInts = 50;
	 */
	protected String generateStaticFields() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append ("public static final int totalNumberOfInts = ");
        text.append (totalNumberOfInts + ";\n");

        return text.toString ();
    }
	
	protected String generateSingleton() {
        StringBuffer text = new StringBuffer ();

        String javaClassName = upperCaseFirst(className);
        String javaVarName = lowerCaseFirst(className);
        
        text.append(INDENT1);
        text.append ("private static ");
        text.append (javaClassName + " " + javaVarName + " = null;\n\n");
        
        // We don't generate private constructor for PWayA-Master because analysers use "classForName" to create an instance of this class.  
        
        text.append(INDENT1);
        text.append ("public static " + javaClassName + " getInstance() {\n");
        text.append(INDENT2);
        text.append ("if (" + javaVarName + " == null) {\n");
        text.append(INDENT3);
        text.append (javaVarName + " = new " + javaClassName + "();\n");
        text.append(INDENT3);
        text.append (javaVarName + ".init();\n");
        text.append(INDENT2);
        text.append ("}\n");
        text.append(INDENT2);
        text.append ("return " + javaVarName + ";\n");
        text.append(INDENT1);
        text.append ("}\n\n");
        
        return text.toString ();
    }
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
    		pway = PWay_Master.getInstance();
    	}
	 */
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(VAR_PWAY + " = " + entityClassName +  ".getInstance();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
	
	/*
	 *  This method generates the following sample code:
	 
	 	  protected int getTotalNumberOfInts() {
	        return totalNumberOfInts;
	      }
	      
	      public void startPwayMaster(String [] args) {
	    	init();
	    	handleArgs(args);
	    	start(null);
	      }
	      
	      protected void initAgent() {
	        subPWayAgents = new ArrayList<PWayAgent>();
	        subPWayAgents.add(PWayA_000001.getInstance());
	        subIntAgents = null;
	        super.initAgent();
	      }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

		text.append(INDENT1);
        text.append("protected int getTotalNumberOfInts() {\n");
        text.append(INDENT2);
        text.append("return totalNumberOfInts;\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        text.append(INDENT1);
        text.append("public void startPwayMaster(String [] args) {\n");
        text.append(INDENT2);
        text.append("init();\n");
        text.append(INDENT2);
        text.append("handleArgs(args);\n");
        text.append(INDENT2);
        text.append("start(null);\n");
        text.append(INDENT1);
        text.append("}\n\n");
		
		
        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        text.append(INDENT2);
        text.append("logger.info(\"========================================\");\n");
        text.append(INDENT2);
        text.append("logger.info(\"PwayMasterAgent: Starting & initialising ...\");\n");
        text.append(INDENT2);
        text.append("logger.info(\"========================================\");\n");
        if (!subPwayAgents.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subPwayAgents, SUPER_CLASS_PWAYA, SUB_PWAY_AGENTS));
        }
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}

	/*
	 *  This method generates the following sample code:
	 
	     public static void main(String [] args) {
	        try {
	            PWayA_Master.getInstance().handleArgs(args);
	            PWayA_Master.getInstance().start(null);
	        }
	        catch (Throwable t) {
	            t.printStackTrace();
	        }
	    }
	 */
	
	protected String generateMainMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("public static void main(String [] args) {\n");
        
		text.append(INDENT2);
	    text.append("try {\n");
	    text.append(INDENT3);
	    text.append(className + ".getInstance().handleArgs(args);\n");
	    text.append(INDENT3);
	    text.append(className + ".getInstance().start(null);\n");
	    text.append(INDENT2);
	    text.append("}\n");
	    text.append(INDENT2);
	    text.append("catch (Throwable t) {\n");
	    text.append(INDENT3);
	    text.append("logger.error(\"@@@@@@@@@@@@@@@@@@@@@@@@@ Unexpected error for PWayMaster: \", t);\n");
	    text.append(INDENT2);
	    text.append("}\n");
        
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

