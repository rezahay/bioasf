package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;

import nl.vu.bioinf.ebf.metamodel.interaction.ConvAgent;

public class ConvA_000001 extends ConvAgent {

    private static ConvA_000001 convA_000001 = null;

    private ConvA_000001() {
    }

    public static ConvA_000001 getInstance() {
        if (convA_000001 == null) {
        	convA_000001 = new ConvA_000001();
        	convA_000001.init();
        }
        return convA_000001;
    }
    
    protected void init() {
    	conv = Conv_000001.getInstance();
    	interaction = conv;
    }
    
    protected void initAgent() {
    	cats = new ArrayList<Catalysis>();
    	cats.add(Cat_000011.getInstance());
    	super.initAgent();
   }
}
=====================================================
 */
public class ConvAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(ConvAGenerator.class);
	
	private String entityClassName = null;
	
	public ConvAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		
		// Get classNum from the end of entity name (e.g., PWay_000001) and append it at the end of its agent (e.g., PWayA_000001).
		String[] tokens = entityClassName.split("_");
		String entityClassNum = tokens[1];
		className = PRE_CLASS_CONVA + entityClassNum;
		
		superClassName = SUPER_CLASS_CONVA;
		ctrls = childrenClassNames.get(ASSOC_CTRLS);
		ctrlAgents = childrenClassNames.get(ASSOC_CTRL_AGENTS);
		cats = childrenClassNames.get(ASSOC_CATS);
		catAgents = childrenClassNames.get(ASSOC_CAT_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
		importStmts.add(META_AGENT_PACKAGE_INTS);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    protected void init() {
	    	Conv = Conv_000001.getInstance();
	    	interaction = Conv;
	    }
	 */
	protected String generateInitMethod() {
		return generateGIntrInitMethod(entityClassName, VAR_CONV);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     protected void initAgent() {
	    	cats = new ArrayList<Catalysis>();
	    	cats.add(Cat_000011.getInstance());
	    	super.initAgent();
	     }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        if (!ctrls.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrls, SUPER_CLASS_CTRL, ASSOC_CTRLS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrlAgents, SUPER_CLASS_CTRLA, ASSOC_CTRL_AGENTS));
        }
        if (!cats.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(cats, SUPER_CLASS_CAT, ASSOC_CATS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(catAgents, SUPER_CLASS_CATA, ASSOC_CAT_AGENTS));
        }	
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

