package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Dna;
import org.biopax.paxtools.model.level3.DnaRegion;
import org.biopax.paxtools.model.level3.Entity;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.NucleicAcid;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Protein;
import org.biopax.paxtools.model.level3.Rna;
import org.biopax.paxtools.model.level3.RnaRegion;
import org.biopax.paxtools.model.level3.TemplateReaction;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

public class TmpReac_000001 extends TemplateReaction {

    private static TmpReac_000001 tmpReac_000001 = null;

    private TmpReac_000001() {
    }

    public static TmpReac_000001 getInstance() {
        if (tmpReac_000001 == null) {
        	tmpReac_000001 = new TmpReac_000001();
        	tmpReac_000001.init();
        }
        return tmpReac_000001;
    }
    
    private void init() {
	    	id = "tmpReac_000001";
	    	template_Dna = Dna_000011.getInstance();
	    	template_DnaRegion = DnaReg_000011.getInstance(); 
	    	template_Rna = Rna_000011.getInstance();  
	    	template_RnaRegion = RnaReg_000011.getInstance();
	    	product_Dna = new ArrayList<Dna>();
	    	product_Dna.add(Dna_000001.getInstance());
	    	product_Rna = new ArrayList<Rna>();
	    	product_Rna.add(Rna_000001.getInstance());
	    	product_Protein = new ArrayList<Protein>();
	    	product_Protein.add(Prot_000001.getInstance());
	}
}
=====================================================
 */
public class TmpReacGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(TmpReacGenerator.class);
	
	private TemplateReaction tmpReac = null;
	private String templateDna = null;
	private String templateDnaRegion = null;
	private String templateRna = null;
	private String templateRnaRegion = null;
	private List<String> productProtList = new ArrayList<String>();
	private List<String> productDnaList = new ArrayList<String>();
	private List<String> productRnaList = new ArrayList<String>();
	private List<String> participantList = new ArrayList<String>();
	
	public TmpReacGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_TMPREAC;
		tmpReac = (TemplateReaction)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		childrenClassNames.put(ASSOC_TMPREGS, tmpRegs);
		childrenClassNames.put(ASSOC_TMPREG_AGENTS, tmpRegAgents);
		new TmpReacAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consTemplateChild(NucleicAcid template) throws Exception {
		if (template instanceof Dna) {
			consChildEntity(template, PRE_CLASS_DNA, null);
			templateDna = entityList.get(0);
		}
		else if (template instanceof DnaRegion) {
			consChildEntity(template, PRE_CLASS_DNAREG, null);
			templateDnaRegion = entityList.get(0);
		}
		else if (template instanceof Rna) {
			consChildEntity(template, PRE_CLASS_RNA, null);
			templateRna = entityList.get(0);
		}
		else if (template instanceof RnaRegion) {
			consChildEntity(template, PRE_CLASS_RNAREG, null);
			templateRnaRegion = entityList.get(0);
		}
		entityList.clear();
	}
	
	private void consProductChildren(Set<PhysicalEntity> products) throws Exception {
		for (PhysicalEntity template: products) {
			if (template instanceof Protein) {
				consChildEntity(template, PRE_CLASS_PROT, null);
				productProtList.addAll(entityList);
			}
			else if (template instanceof Dna) {
				consChildEntity(template, PRE_CLASS_DNA, null);
				productDnaList.addAll(entityList);
			}
			else if (template instanceof Rna) {
				consChildEntity(template, PRE_CLASS_RNA, null);
				productRnaList.addAll(entityList);
			}
			entityList.clear();
		}
	}
	
	private void consChildren() throws Exception {
		NucleicAcid template = tmpReac.getTemplate();
		consTemplateChild(template);
		
		Set<PhysicalEntity> products = tmpReac.getProduct();
		consProductChildren(products);	
		
		Set<Entity> allParticipants = tmpReac.getParticipant();
		allParticipants.remove(template);
		allParticipants.removeAll(products);
		
		consChildrenFromControllerSet((Set<PhysicalEntity>)(Set<?>)allParticipants);
		participantList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(tmpReac.getControlledOf());
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     private void init() {
	    	id = "tmpReac_000001";
	    	template_Dna = Dna_000011.getInstance();
	    	template_DnaRegion = DnaReg_000011.getInstance(); 
	    	template_Rna = Rna_000011.getInstance();  
	    	template_RnaRegion = RnaReg_000011.getInstance();
	    	product_Dna = new ArrayList<Dna>();
	    	product_Dna.add(Dna_000001.getInstance());
	    	product_Rna = new ArrayList<Rna>();
	    	product_Rna.add(Rna_000001.getInstance());
	    	product_Protein = new ArrayList<Protein>();
	    	product_Protein.add(Prot_000001.getInstance());
	   }
	 */
	
	private String generateTmpReacStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenIntStmts(tmpReac));
    	
    	// There is a OR-restriction on the "template" property. Only one of them is allowed.
    	if (templateDna != null) {
    		text.append(INDENT2);
    		text.append("template_Dna" + " = " + templateDna +  ".getInstance();\n");
    	}
    	else if (templateDnaRegion != null) {
    		text.append(INDENT2);
    		text.append("template_DnaRegion" + " = " + templateDnaRegion +  ".getInstance();\n");
    	}
    	else if (templateRna != null) {
    		text.append(INDENT2);
    		text.append("template_Rna" + " = " + templateRna +  ".getInstance();\n");
    	}
    	else if (templateRnaRegion != null) {
    		text.append(INDENT2);
    		text.append("template_RnaRegion" + " = " + templateRnaRegion +  ".getInstance();\n");
    	}
    	
    	// There is a OR-restriction on the "product" property. Only one of them is allowed.
    	if (!productDnaList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(productDnaList, SUPER_CLASS_DNA, "product_Dna"));
        }
    	else if (!productRnaList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(productRnaList, SUPER_CLASS_RNA, "product_Rna"));
        }
    	else if (!productProtList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(productProtList, SUPER_CLASS_PROT, "product_Protein"));
        }
    	
    	if (!participantList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(participantList, SUPER_CLASS_PHYE, "participant_TemplateReaction"));
        }
    	
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateTmpReacStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

