package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.ComplexAssembly;
import org.biopax.paxtools.model.level3.Catalysis;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

// BiochemicalReaction: display-name
public class CmpAsm_000011 extends ComplexAssembly {

    private static CmpAsm_000011 cmpasm_000011 = null;

    private CmpAsm_000011() {
    }

    public static CmpAsm_000011 getInstance() {
        if (cmpasm_000011 == null) {
        	cmpasm_000011 = new CmpAsm_000011();
        	cmpasm_000011.init();
        }
        return cmpasm_000011;
    }
    
    private void init() {
    	id = "CmpAsm_000011";
    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
    	left = new ArrayList<PhysicalEntity>();
    	left.add(Prot_000002.getInstance());
    	left.add(Prot_000012.getInstance());
    	right = new ArrayList<PhysicalEntity>();
    	right.add(Cmp_000011.getInstance());
    	participant_Conversion = null;
   }
}
=====================================================
 */
public class CmpAsmGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(CmpAsmGenerator.class);
	
	private ComplexAssembly cmpAsm = null;
	private List<String> leftList = new ArrayList<String>();
	private List<String> rightList = new ArrayList<String>();
	private List<String> stoiList = new ArrayList<String>();
	
	public CmpAsmGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_CMPASM;
		cmpAsm = (ComplexAssembly)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		childrenClassNames.put(ASSOC_CATS, cats);
		childrenClassNames.put(ASSOC_CAT_AGENTS, catAgents);
		new CmpAsmAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(cmpAsm.getLeft());
		leftList.addAll(entityList);
		entityList.clear();
		consChildrenFromControllerSet(cmpAsm.getRight());
		rightList.addAll(entityList);
		entityList.clear();
		consChildrenFromUtilitySet(cmpAsm.getParticipantStoichiometry());
		stoiList.addAll(entityList);
		entityList.clear();
		for (Control control: cmpAsm.getControlledOf()) {
			if (control instanceof Catalysis) {
				consChildEntity(control, PRE_CLASS_CAT, PRE_CLASS_CATA);
			}
			else if (control instanceof Control) {
				consChildEntity(control, PRE_CLASS_CTRL, PRE_CLASS_CTRLA);
			}
		}
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "CmpAsm_000011";
	    	conversionDirection = Enum_conversionDirection.LEFT_TO_RIGHT;
	    	left = new ArrayList<PhysicalEntity>();
	    	left.add(Prot_000002.getInstance());
	    	left.add(Prot_000012.getInstance());
	    	right = new ArrayList<PhysicalEntity>();
	    	right.add(Cmp_000011.getInstance());
	    	participantStoichiometry = new ArrayList<Stoichiometry>();
        	participantStoichiometry.add(Stoi_000001.getInstance());
	    	participant_Conversion = null;
	   }
	 */
	
	private String generateCmpAsmStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenConvStmts(cmpAsm));
		 if (!leftList.isEmpty()) {
	     	text.append(INDENT2);
	     	text.append(generateListItemsByListAddOp(leftList, SUPER_CLASS_PHYE, "left"));
	     }
	     if (!rightList.isEmpty()) {
	     	text.append(INDENT2);
	     	text.append(generateListItemsByListAddOp(rightList, SUPER_CLASS_PHYE, "right"));
	     }	
	     if (!stoiList.isEmpty()) {
	     	text.append(INDENT2);
	     	text.append(generateListItemsByListAddOp(stoiList, SUPER_CLASS_STOI, "participantStoichiometry"));
	 	 }
		
	     return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateCmpAsmStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

