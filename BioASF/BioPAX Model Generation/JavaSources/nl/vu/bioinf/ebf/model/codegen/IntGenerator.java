package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Interaction;
import org.biopax.paxtools.model.level3.PhysicalEntity;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

public class Int_000001 extends BiochemicalReaction {

    private static Int_000001 int_000001 = null;

    private Int_000001() {
    }

    public static Int_000001 getInstance() {
        if (int_000001 == null) {
        	int_000001 = new Int_000001();
        	int_000001.init();
        }
        return int_000001;
    }
    
    private void init() {
    	id = "Int_000001";
    	participant = new ArrayList<Entity>();
    	participant.add(Prot_000001.getInstance());
    	participant.add(Pway_000001.getInstance());
    	participant.add(BioReac_000001.getInstance());
    	participant.add(Cat_000001.getInstance());
    	participant.add(Gen_000001.getInstance());
   }
}
=====================================================
 */
public class IntGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(IntGenerator.class);
	
	private Interaction intr = null;
	private List<String> participantList = new ArrayList<String>();
	
	public IntGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_INT;
		intr = (Interaction)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		new IntAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consChildren() throws Exception {
		consChildrenFromProcessSet((Set<Process>)(Set<?>)intr.getParticipant());
		participantList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(intr.getControlledOf());
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	        private void init() {
		    	id = "Int_000001";
		    	participant = new ArrayList<Entity>();
		    	participant.add(Prot_000001.getInstance());
		    	participant.add(Pway_000001.getInstance());
		    	participant.add(BioReac_000001.getInstance());
		    	participant.add(Cat_000001.getInstance());
		    	participant.add(Gen_000001.getInstance());
		   }
	 */
	
	private String generateIntStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenIntStmts(intr));
        
        if (!participantList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(participantList, SUPER_CLASS_ENTITY, "participant"));
        }
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateIntStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

