package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;

import nl.vu.bioinf.ebf.metamodel.interaction.IntAgent;

public class IntA_000001 extends IntAgent {

    private static IntA_000001 intA_000001 = null;

    private IntA_000001() {
    }

    public static IntA_000001 getInstance() {
        if (intA_000001 == null) {
        	intA_000001 = new IntA_000001();
        	intA_000001.init();
        }
        return intA_000001;
    }
    
    protected void init() {
    	intr = Int_000001.getInstance();
    	interaction = intr;
    }
    
    protected void initAgent() {
    	ctrls = new ArrayList<Catalysis>();
    	ctrls.add(Ctrl_000011.getInstance());
    	super.initAgent();
   }
}
=====================================================
 */
public class IntAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(IntAGenerator.class);
	
	private String entityClassName = null;
	
	public IntAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		
		// Get classNum from the end of entity name (e.g., PWay_000001) and append it at the end of its agent (e.g., PWayA_000001).
		String[] tokens = entityClassName.split("_");
		String entityClassNum = tokens[1];
		className = PRE_CLASS_INTA + entityClassNum;
		
		superClassName = SUPER_CLASS_INTA;
		ctrls = childrenClassNames.get(ASSOC_CTRLS);
		ctrlAgents = childrenClassNames.get(ASSOC_CTRL_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
		importStmts.add(META_AGENT_PACKAGE_INTS);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    protected void init() {
	    	intr = Int_000001.getInstance();
	    	interaction = intr;
	    }
	 */
	protected String generateInitMethod() {
		return generateGIntrInitMethod(entityClassName, VAR_INTR);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     protected void initAgent() {
	    	ctrls = new ArrayList<Catalysis>();
	    	ctrls.add(Ctrl_000011.getInstance());
	    	super.initAgent();
	     }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        if (!ctrls.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrls, SUPER_CLASS_CTRL, ASSOC_CTRLS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrlAgents, SUPER_CLASS_CTRLA, ASSOC_CTRL_AGENTS));
        }
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

