package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Catalysis;
import org.biopax.paxtools.model.level3.CatalysisDirectionType;
import org.biopax.paxtools.model.level3.ComplexAssembly;
import org.biopax.paxtools.model.level3.Control;
import org.biopax.paxtools.model.level3.Conversion;
import org.biopax.paxtools.model.level3.Degradation;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Modulation;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.Transport;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.cats;

import java.util.ArrayList;

import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.Conversion;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.model.ints.*;

public class Cat_000011 extends Catalysis {

    private static Cat_000011 cat_000011 = null;

    private Cat_000011() {
    }

    public static Cat_000011 getInstance() {
        if (cat_000011 == null) {
        	cat_000011 = new Cat_000011();
        	cat_000011.init();
        }
        return cat_000011;
    }
    
    private void init() {
    	id = "cat_000011";
    	controller = new ArrayList<PhysicalEntity>();
    	controller.add(Prot_000011.getInstance());
    	controlled = new ArrayList<Conversion>();
    	controlled.add(CmpAsm_000011.getInstance());
    	cofactor = null;
   }
}
=====================================================
 */
public class CatGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(CatGenerator.class);
	
	private Catalysis cat = null;
	private List<String> cofactorList = new ArrayList<String>();
	private List<String> controllerList = new ArrayList<String>();
	private List<String> controlledList = new ArrayList<String>();
	
	public CatGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_CAT;
		cat = (Catalysis)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		childrenClassNames.put(ASSOC_MODS, mods);
		childrenClassNames.put(ASSOC_MOD_AGENTS, modAgents);
		new CatAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(cat.getController());
		controllerList.addAll(entityList);
		entityList.clear();
		consChildrenFromControllerSet(cat.getCofactor());
		cofactorList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(cat.getControlledOf());
		consChildrenFromProcessSet(cat.getControlled());
		controlledList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	   private void init() {
	    	id = "cat_000011";
	    	controller = new ArrayList<PhysicalEntity>();
	    	controller.add(Prot_000011.getInstance());
	    	controlled = new ArrayList<Conversion>();
	    	controlled.add(CmpAsm_000011.getInstance());
	    	cofactor = null;
	   }
	 */
	
	private String generateCatStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenCtrlStmts(cat));
    	
        CatalysisDirectionType catalysisDirection = cat.getCatalysisDirection();
    	if (catalysisDirection != null) {
	        text.append(INDENT2);
	        text.append(VAR_CAT_DIR + " = Enum_catalysisDirection." + catalysisDirection +  ";\n");
        }
    	if (!controllerList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controllerList, SUPER_CLASS_PHYE, "controller"));
        }
        if (!controlledList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controlledList, SUPER_CLASS_CONV, "controlled"));
        }	
        if (!cofactorList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(cofactorList, SUPER_CLASS_PHYE, "cofactor"));
        }
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateCatStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

