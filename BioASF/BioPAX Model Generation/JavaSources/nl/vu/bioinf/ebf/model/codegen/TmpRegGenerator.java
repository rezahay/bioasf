package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.TemplateReactionRegulation;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.cats;

import java.util.ArrayList;

import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.Conversion;
import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;
import nl.vu.bioinf.ebf.model.ints.*;

public class TmpReg_000011 extends TemplateReactionRegulation {

    private static TmpReg_000011 tmpReg_000011 = null;

    private TmpReg_000011() {
    }

    public static Cat_000011 getInstance() {
        if (tmpReg_000011 == null) {
        	tmpReg_000011 = new TmpReg_000011();
        	tmpReg_000011.init();
        }
        return tmpReg_000011;
    }
    
    private void init() {
    	id = "TmpReg_000011";
    	controller = new ArrayList<PhysicalEntity>();
    	controller.add(Prot_000011.getInstance());
    	controlled = new ArrayList<TemplateReaction>();
    	controlled.add(TmpReac_000011.getInstance());
   }
}
=====================================================
 */
public class TmpRegGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(TmpRegGenerator.class);
	
	private TemplateReactionRegulation tmpReg = null;
	private List<String> controllerList = new ArrayList<String>();
	private List<String> controlledList = new ArrayList<String>();
	
	public TmpRegGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_TMPREG;
		tmpReg = (TemplateReactionRegulation)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		new TmpRegAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(tmpReg.getController());
		controllerList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(tmpReg.getControlled());
		controlledList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(tmpReg.getControlledOf());
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	   private void init() {
	    	id = "TmpReg_000011";
	    	controller = new ArrayList<PhysicalEntity>();
	    	controller.add(Prot_000011.getInstance());
	    	controlled = new ArrayList<TemplateReaction>();
	    	controlled.add(TmpReac_000011.getInstance());
	   }
	 */
	
	private String generateTmpRegStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenCtrlStmts(tmpReg));
    	
    	if (!controllerList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controllerList, SUPER_CLASS_PHYE, "controller"));
        }
        if (!controlledList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(controlledList, SUPER_CLASS_TMPREAC, "controlled"));
        }	
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateTmpRegStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

