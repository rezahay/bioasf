package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Complex;
import org.biopax.paxtools.model.level3.Dna;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Protein;
import org.biopax.paxtools.model.level3.Rna;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.prots;

import nl.vu.bioinf.ebf.metamodel.*;

public class Cmp_000011 extends Complex {

    private static Cmp_000011 cmp_000011 = null;

    private Cmp_000011() {
    }

    public static Cmp_000011 getInstance() {
        if (cmp_000011 == null) {
        	cmp_000011 = new Cmp_000011();
        	cmp_000011.init();
        }
        return cmp_000011;
    }
    
    private void init() {
    	id = "cmp_000011";
        cellularLocation = null;
        component = null;
        displayName = "Compex - HRAS/SOS1";
   }
}
=====================================================
 */
public class CmpGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(CmpGenerator.class);
	
	private Complex cmp = null;
	private List<String> componentList = new ArrayList<String>();
	
	public CmpGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_CMP;
		cmp = (Complex)indType;
		consChildren();
		consImports();
	}
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet(cmp.getComponent());
		componentList.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "cmp_000011";
	        cellularLocation = null;
	        component = null;
	        displayName = "Compex - HRAS/SOS1";
	   }
	 */
	
	private String generateCmpStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenPhysEntityStmts(cmp));
    	
    	/*
    	// Strange thing is that paxtools does not return a set for getEntityReference. However, it is not restricted by cardinality and
    	// therefore we have generated code which expects a set. 
    	Set<ProteinReference> entityReference = prot.getEntityReference(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_PHYE, "memberPhysicalEntity"));
    	}
    	*/
    	if (!componentList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(componentList, SUPER_CLASS_PHYE, "component"));
        }
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateCmpStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

