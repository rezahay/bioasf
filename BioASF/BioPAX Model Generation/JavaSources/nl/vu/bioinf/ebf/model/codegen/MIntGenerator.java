package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BiochemicalReaction;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.MolecularInteraction;
import org.biopax.paxtools.model.level3.Process;
import org.biopax.paxtools.model.level3.PhysicalEntity;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.MInts;


import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.MInts.*;

public class MInt_000001 extends MolecularInteraction {

    private static MInt_000001 mint_000001 = null;

    private MInt_000001() {
    }

    public static MInt_000001 getInstance() {
        if (mint_000001 == null) {
        	mint_000001 = new MInt_000001();
        	mint_000001.init();
        }
        return mint_000001;
    }
    
    private void init() {
    	id = "MInt_000001";
    	participant = new ArrayList<PhysicalEntity>();
    	participant.add(Prot_000001.getInstance());
   }
}
=====================================================
 */
public class MIntGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(MIntGenerator.class);
	
	private MolecularInteraction mintr = null;
	private List<String> participantList = new ArrayList<String>();
	
	public MIntGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_MINT;
		mintr = (MolecularInteraction)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(ASSOC_CTRLS, ctrls);
		childrenClassNames.put(ASSOC_CTRL_AGENTS, ctrlAgents);
		new MIntAGenerator().generate(null, entityClassName, childrenClassNames);
	}	
	
	private void consChildren() throws Exception {
		consChildrenFromControllerSet((Set<PhysicalEntity>)(Set<?>)mintr.getParticipant());
		participantList.addAll(entityList);
		entityList.clear();
		consChildrenFromProcessSet(mintr.getControlledOf());
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	        private void init() {
		    	id = "MInt_000001";
		    	participant = new ArrayList<PhysicalEntity>();
		    	participant.add(Prot_000001.getInstance());
		   }
	 */
	
	private String generateMIntStmts() {
    	StringBuffer text = new StringBuffer ();
    	text.append(generateGenIntStmts(mintr));
        
        if (!participantList.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(participantList, SUPER_CLASS_PHYE, "participant_MolecularInteraction"));
        }
        
        return text.toString ();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateMIntStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

