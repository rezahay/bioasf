package nl.vu.bioinf.ebf.model.codegen;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import nl.vu.bioinf.ebf.metamodel.util.Constants;

import org.apache.log4j.Logger;
import org.biopax.paxtools.io.BioPAXIOHandler;
import org.biopax.paxtools.io.SimpleIOHandler;
import org.biopax.paxtools.model.Model;

public class GenerateModel {
	private static Logger logger = Logger.getLogger(GenerateModel.class);
	
    public static final String BASE_PACKAGE = "nl.vu.bioinf.ebf.model";
    public static final String META_PACKAGE = "nl.vu.bioinf.ebf.metamodel";
    
    public static final String MASTER_PWAY = "PWay_Master";
    public static final String MASTER_PWAYA = "PWayA_Master";
    
    public static String sourceOutputDir = null;
    public static String resourceOutputDir = null;
    public static Model biopaxModel = null;
    
    private static Properties initialConcentrations = null;
    
    private static void generateModelCode() throws Exception {
    	MasterPWayGenerator masterPWayGen = new MasterPWayGenerator();
    	masterPWayGen.generate(null, MASTER_PWAY, null);
    	masterPWayGen.logStatistics();
        masterPWayGen.initiateExpressions(initialConcentrations, resourceOutputDir + "/" + Constants.EXPRESSION_FILE);
    }
    
    private static void logAndExit (String msg, Exception e) {
        if (msg != null) {
            logger.error(msg, e);
        }    

        System.exit (0);
    }
    
    private static void start(String[] args) throws Exception {
    	if (args.length != 1) { 
    		logAndExit("Usage: GenerateModel <config-file>", null);
    	}
    	String configFile = args[0];
    	
    	String currentKey = null;
    	try {
			Properties properties = new Properties();
			try {
				FileInputStream fin = new FileInputStream(configFile);
				properties.load(fin);
			}
			catch (Exception ex) {
				logAndExit("Config file '" + configFile + "' could not be found.", ex);
	    	}
			
			currentKey = Constants.KEY_OUTPUT_DIR;
			String outputDir = properties.get(currentKey).toString();		
			sourceOutputDir = outputDir + "/" + Constants.JAVA_SOURCE_DIR;
			resourceOutputDir = outputDir + "/" + Constants.JAVA_RESOURCE_DIR;
	        File theSourceDir = new File(sourceOutputDir);
	        File theResourceDir = new File(resourceOutputDir);
	        if (!theSourceDir.exists()) {
	        	boolean result = false;
	            try {
	                theSourceDir.mkdirs();
	                theResourceDir.mkdirs();
	                result = true;
	            } 
	            catch (SecurityException se) {
	                //handle it
	            }        
	            if (result) {    
	               logger.info("Output directory " + outputDir + " created.");  
	            }
	        }
	        else if (!theSourceDir.isDirectory()) {
	            logAndExit("Output directory " + outputDir + " is not a directory.", null);
	        }
	            
	        currentKey = Constants.KEY_INPUT_FILE;
	        String inputFile = null;
	        try {
	        	inputFile = properties.get(currentKey).toString(); 
	        	FileInputStream fin = new FileInputStream(inputFile);
		    	BioPAXIOHandler handler = new SimpleIOHandler();
		    	biopaxModel = handler.convertFromOWL(fin);
	        }
	        catch (NullPointerException nex) {
	        	logAndExit("Missing Biopax input model in the config file: " + Constants.CONFIG_FILE, nex);
	        }
	        catch (Exception ex) {
	    		logAndExit("Biopax input model file could not be found.", ex);
	        }	
	        
	        currentKey = Constants.KEY_INITIAL_CONCENTRATIONS;
	        String initialConcentrationFile = null;
	    	try {
	    		initialConcentrationFile = properties.get(currentKey).toString(); 
	    		FileInputStream fin = new FileInputStream(initialConcentrationFile);
	    		initialConcentrations = new Properties();
	    		initialConcentrations.load(fin);
	    	}
	    	catch (NullPointerException nex) {
	    		logger.info("No initial concentrations specified.");
	        }
	        catch (Exception ex) {
	        	logAndExit("Initial concentration file '" + initialConcentrationFile + "' could not be found.", ex);
	        }	
	    	
	    	generateModelCode();
    	}
    	catch (Throwable t) {
            logger.error("Exception during ModelGenerator.start: ", t);
        }
    	
    }

    public static void main(String[] args) {
        try {
        	GenerateModel.start(args);
        }
        catch (Exception e) {
            logger.error("Exception in ModelGenerator: ", e);
            System.exit (1);
        }
    }
}
