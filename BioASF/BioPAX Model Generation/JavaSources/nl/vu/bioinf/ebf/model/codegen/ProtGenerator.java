package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Protein;

import org.biopax.paxtools.model.Model;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.test.prots;

import nl.vu.bioinf.ebf.metamodel.*;

public class Prot_000001 extends Protein {

    private static Prot_000001 prot_000001 = null;

    private Prot_000001() {
    }

    public static Prot_000001 getInstance() {
        if (prot_000001 == null) {
        	prot_000001 = new Prot_000001();
        	prot_000001.init();
        }
        return prot_000001;
    }
    
    private void init() {
    	id = "prot_000001";
    	memberPhysicalEntity_Protein = null;
        entityReference = null;
        cellularLocation = null;
        displayName = "HRAS";
   }
}
=====================================================
 */
public class ProtGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(ProtGenerator.class);
	
	private Protein prot = null;
	
	public ProtGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_PROT;
		prot = (Protein)indType;
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "prot_000001";
	    	memberPhysicalEntity_Protein = null;
	        entityReference = null;
	        cellularLocation = null;
	        displayName = "HRAS";
	   }
	 */
	
	private String generateProtStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateGenPhysEntityStmts(prot));
    	
    	/*
    	// Strange thing is that paxtools does not return a set for getEntityReference. However, it is not restricted by cardinality and
    	// therefore we have generated code which expects a set. 
    	Set<ProteinReference> entityReference = prot.getEntityReference(); 
    	if (memberPhysicalEntitySet != null && !memberPhysicalEntitySet.isEmpty()) {
    		//text.append(INDENT2);
    		//text.append(generateSetItems(memberPhysicalEntitySet, SUPER_CLASS_PHYE, "memberPhysicalEntity"));
    	}
    	*/
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateProtStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

