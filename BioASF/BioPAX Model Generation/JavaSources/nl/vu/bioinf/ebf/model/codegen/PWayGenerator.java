package nl.vu.bioinf.ebf.model.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.BioSource;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.Pathway;
import org.biopax.paxtools.model.level3.Process;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.pways;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.test.ints.*;

// Pathway: display-name
public class PWay_000001 extends Pathway {

    private static PWay_000001 pway_000001 = null;

    private PWay_000001() {
    }

    public static PWay_000001 getInstance() {
        if (pway_000001 == null) {
        	pway_000001 = new PWay_000001();
        	pway_000001.init();
        }
        return pway_000001;
    }
    
    private void init() {
    	 id = "PWay_000001";
    	 pathwayComponent_Pathway = new ArrayList<Pathway>();
    	 pathwayComponent_Pathway.add(PWay_000011.getInstance());
    	 pathwayComponent_Pathway.add(PWay_000012.getInstance());
         pathwayComponent_Interaction = new ArrayList<Interaction>();
         pathwayComponent_Interaction.add(BioReac_000001.getInstance());
    }
}
=====================================================
 */
public class PWayGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(PWayGenerator.class);
	
	private Pathway pway = null;
	private List<String> subPways = new ArrayList<String>();
	private List<String> subInts = new ArrayList<String>();
	
	public PWayGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_PWAYS;
		className = entityClassName;
		superClassName = SUPER_CLASS_PWAY;
		pway = (Pathway)indType;
		consChildren();
		consAgent(entityClassName);
		consImports();
	}
	
	private void consAgent(String entityClassName) throws Exception {
		Map<String, List<String>> childrenClassNames = new HashMap<String, List<String>>();
		childrenClassNames.put(SUB_PWAY_AGENTS, subPwayAgents);
		childrenClassNames.put(SUB_INT_AGENTS, subIntAgents);
		new PWayAGenerator().generate(null, entityClassName, childrenClassNames);
	}
	
	
	private void consChildren() throws Exception {
		Set<Process> pwayComponents = pway.getPathwayComponent();
		Set<Process> pwayComponentsCopy = new HashSet<Process>(pwayComponents);
		for (Process childProcess: pwayComponentsCopy) {
			if (childProcess instanceof Pathway) {
				consChildEntity(childProcess, PRE_CLASS_PWAY, PRE_CLASS_PWAYA);
				pwayComponents.remove(childProcess);
			}
		}
		subPways.addAll(entityList);
		entityList.clear();
		
		consChildrenFromProcessSet(pwayComponents);
		subInts.addAll(entityList);
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
		importStmts.add(MODEL_ENTITY_PACKAGE_INTS + ".*");
	}
	
	/*
	 *  This method generates the following sample code:
	 
	      private void init() {
	    	 id = "PWay_000001";
	    	 pathwayComponent_Pathway = new ArrayList<Pathway>();
	    	 pathwayComponent_Pathway.add(PWay_000011.getInstance());
	    	 pathwayComponent_Pathway.add(PWay_000012.getInstance());
	         pathwayComponent_Interaction = new ArrayList<Interaction>();
	         pathwayComponent_Interaction.add(BioReac_000001.getInstance());
	    }
	 */
	
	private String generatePwayStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateEntityStmts(pway));
    	
    	BioSource organism = pway.getOrganism();
    	if (organism != null) {
    		//text.append(INDENT2);
    		// ???
    	}
    	if (!subPways.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subPways, SUPER_CLASS_PWAY, "pathwayComponent_Pathway"));
        }
        if (!subInts.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(subInts, SUPER_CLASS_INT, "pathwayComponent_Interaction"));
        }	
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generatePwayStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

