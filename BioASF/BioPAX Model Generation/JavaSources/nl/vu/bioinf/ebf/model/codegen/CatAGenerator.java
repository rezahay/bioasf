package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;

import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class CatA_000011 extends CatAgent {

    private static CatA_000011 catA_000011 = null;

    private CatA_000011() {
    }

    public static CatA_000011 getInstance() {
        if (catA_000011 == null) {
        	catA_000011 = new CatA_000011();
        	catA_000011.init();
        }
        return catA_000011;
    }
    
    protected void init() {
    	cat = Cat_000011.getInstance();
    	interaction = cat;
    }
    
    protected void initAgent() {
    	mods = null;
    	super.initAgent();
   }
}
=====================================================
 */
public class CatAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(CatAGenerator.class);
	
	private String entityClassName = null;
	
	public CatAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		
		// Get classNum from the end of entity name (e.g., PWay_000001) and append it at the end of its agent (e.g., PWayA_000001).
		String[] tokens = entityClassName.split("_");
		String entityClassNum = tokens[1];
		className = PRE_CLASS_CATA + entityClassNum;
		
		superClassName = SUPER_CLASS_CATA;
		ctrls = childrenClassNames.get(ASSOC_CTRLS);
		ctrlAgents = childrenClassNames.get(ASSOC_CTRL_AGENTS);
		mods = childrenClassNames.get(ASSOC_MODS);
		modAgents = childrenClassNames.get(ASSOC_MOD_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
		importStmts.add(META_AGENT_PACKAGE_INTS);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    protected void init() {
	    	cat = Cat_000011.getInstance();
	    	interaction = cat;
	    }
	 */
	protected String generateInitMethod() {
		return generateGIntrInitMethod(entityClassName, VAR_CAT);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     protected void initAgent() {
	    	mods = new ArrayList<Modulation>();
	    	mods.add(Mod_000011.getInstance());
	    	super.initAgent();
	     }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        if (!ctrls.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrls, SUPER_CLASS_CTRL, ASSOC_CTRLS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrlAgents, SUPER_CLASS_CTRLA, ASSOC_CTRL_AGENTS));
        }
        if (!mods.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(mods, SUPER_CLASS_MOD, ASSOC_MODS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(modAgents, SUPER_CLASS_MODA, ASSOC_MOD_AGENTS));
        }	
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

