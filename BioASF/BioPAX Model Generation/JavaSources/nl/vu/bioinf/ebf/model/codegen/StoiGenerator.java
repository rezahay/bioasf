package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Complex;
import org.biopax.paxtools.model.level3.Dna;
import org.biopax.paxtools.model.level3.DnaRegion;
import org.biopax.paxtools.model.level3.Level3Element;
import org.biopax.paxtools.model.level3.PhysicalEntity;
import org.biopax.paxtools.model.level3.Protein;
import org.biopax.paxtools.model.level3.Rna;
import org.biopax.paxtools.model.level3.RnaRegion;
import org.biopax.paxtools.model.level3.SmallMolecule;
import org.biopax.paxtools.model.level3.Stoichiometry;


/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;

import nl.vu.bioinf.ebf.metamodel.*;

public class Stoi_000001 extends Stoichiometry {

    private static Stoi_000001 stoi_000001 = null;

    private Stoi_000001() {
    }

    public static Stoi_000001 getInstance() {
        if (stoi_000001 == null) {
        	stoi_000001 = new Stoi_000001();
        	stoi_000001.init();
        }
        return stoi_000001;
    }
    
    private void init() {
    	id = "Stoi_000001";
    	physicalEntity = Prot_000001.getInstance();
        stoichiometricCoefficient = 3.0;
   }
}
=====================================================
 */
public class StoiGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(StoiGenerator.class);
	
	private Stoichiometry stoi = null;
	private String physicalEntity = null;
	
	public StoiGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		className = entityClassName;
		superClassName = SUPER_CLASS_STOI;
		stoi = (Stoichiometry)indType;
		consChildren();
		consImports();
	}
	
	private void consChildren() throws Exception {
		PhysicalEntity pe = stoi.getPhysicalEntity();
		
		if (pe instanceof Protein) {
			consChildEntity(pe, PRE_CLASS_PROT, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof Complex) {
			consChildEntity(pe, PRE_CLASS_CMP, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof Dna) {
			consChildEntity(pe, PRE_CLASS_DNA, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof DnaRegion) {
			consChildEntity(pe, PRE_CLASS_DNAREG, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof Rna) {
			consChildEntity(pe, PRE_CLASS_RNA, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof RnaRegion) {
			consChildEntity(pe, PRE_CLASS_RNAREG, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof SmallMolecule) {
			consChildEntity(pe, PRE_CLASS_SMLM, null);
			physicalEntity = entityList.get(0);
		}
		else if (pe instanceof PhysicalEntity) {
			consChildEntity(pe, PRE_CLASS_PHYE, null);
			physicalEntity = entityList.get(0);
		}
		else {
			logger.warn("An instance of PhysicalEntity expected.");
			return;
		}
		entityList.clear();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    private void init() {
	    	id = "Stoi_000001";
	    	physicalEntity = Prot_000001.getInstance();
	        stoichiometricCoefficient = 3.0f;
	    }
	 */
	
	private String generateStoiStmts() {
    	StringBuffer text = new StringBuffer();
    	text.append(generateBasicEntityStmts(stoi));
    	if (physicalEntity != null) {
    		text.append(INDENT2);
    		text.append("physicalEntity" + " = " + physicalEntity +  ".getInstance();\n");
    	}
    	text.append(INDENT2);
		text.append("stoichiometricCoefficient" + " = " + stoi.getStoichiometricCoefficient() +  "f;\n");
    	
        return text.toString();
	}
	
	protected String generateInitMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("private void init() {\n");
        text.append(INDENT2);
        text.append(generateStoiStmts());
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

