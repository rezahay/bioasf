package nl.vu.bioinf.ebf.model.codegen;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.biopax.paxtools.model.level3.Level3Element;

/*
 * This class generates the following sample code:
=====================================================
package nl.vu.bioinf.ebf.model.ints;

import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000011 extends TmpRegAgent {

    private static TmpRegA_000011 tmpRegA_000011 = null;

    private TmpRegA_000011() {
    }

    public static TmpRegA_000011 getInstance() {
        if (tmpRegA_000011 == null) {
        	tmpRegA_000011 = new TmpRegA_000011();
        	tmpRegA_000011.init();
        }
        return catA_000011;
    }
    
    protected void init() {
    	tmpReg = TmpRegA_000011.getInstance();
    	interaction = tmpReg;
    }
    
    protected void initAgent() {
    	super.initAgent();
   }
}
=====================================================
 */
public class TmpRegAGenerator extends ModelElementGenerator {
	private static Logger logger = Logger.getLogger(TmpRegAGenerator.class);
	
	private String entityClassName = null;
	
	public TmpRegAGenerator() {
	}
	
	public void prepareGeneration(Level3Element indType, String entityClassName, Map<String, List<String>> childrenClassNames) throws Exception {
		this.entityClassName = entityClassName;
		modelPackage = MODEL_ENTITY_PACKAGE_INTS;
		
		// Get classNum from the end of entity name (e.g., PWay_000001) and append it at the end of its agent (e.g., PWayA_000001).
		String[] tokens = entityClassName.split("_");
		String entityClassNum = tokens[1];
		className = PRE_CLASS_TMPREGA + entityClassNum;
		
		superClassName = SUPER_CLASS_TMPREGA;
		ctrls = childrenClassNames.get(ASSOC_CTRLS);
		ctrlAgents = childrenClassNames.get(ASSOC_CTRL_AGENTS);
		consImports();
	}
	
	private void consImports() {
		importStmts.add(JAVA_PACKAGE_LIST);
		importStmts.add(META_BIOPAX_PACKAGE_ALL);
		importStmts.add(META_AGENT_PACKAGE_INTS);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	    protected void init() {
	    	cat = Cat_000011.getInstance();
	    	interaction = cat;
	    }
	 */
	protected String generateInitMethod() {
		return generateGIntrInitMethod(entityClassName, VAR_TMPREG);
	}
	
	/*
	 *  This method generates the following sample code:
	 
	     protected void initAgent() {
	    	super.initAgent();
	     }
	 */
	protected String generateCustomMethod() {
		StringBuffer text = new StringBuffer ();

        text.append(INDENT1);
        text.append("protected void initAgent() {\n");
        if (!ctrls.isEmpty()) {
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrls, SUPER_CLASS_CTRL, ASSOC_CTRLS));
        	text.append(INDENT2);
        	text.append(generateListItemsByListAddOp(ctrlAgents, SUPER_CLASS_CTRLA, ASSOC_CTRL_AGENTS));
        }
        text.append(INDENT2);
        text.append("super.initAgent();\n");
        text.append(INDENT1);
        text.append("}\n\n");
        
        return text.toString ();
	}
}

