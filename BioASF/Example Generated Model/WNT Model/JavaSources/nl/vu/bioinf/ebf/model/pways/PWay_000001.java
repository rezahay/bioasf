/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.pways;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.model.ints.*;

public class PWay_000001 extends Pathway {

    private static PWay_000001 pWay_000001 = null;

    private PWay_000001() {
    }

    public static PWay_000001 getInstance() {
        if (pWay_000001 == null) {
            pWay_000001 = new PWay_000001();
            pWay_000001.init();
        }
        return pWay_000001;
    }

    private void init() {
        id = "PWay_000001";
        rdfId = "Wnt-Experiment";
        displayName = "Wnt Experiment";
        pathwayComponent_Interaction = new ArrayList<Interaction>();
        pathwayComponent_Interaction.add(CmpAsm_000001.getInstance());
        pathwayComponent_Interaction.add(TmpReg_000001.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000002.getInstance());
        pathwayComponent_Interaction.add(Degr_000001.getInstance());
        pathwayComponent_Interaction.add(Degr_000002.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000003.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000004.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000005.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000006.getInstance());
        pathwayComponent_Interaction.add(TmpReac_000001.getInstance());
        pathwayComponent_Interaction.add(TmpReac_000002.getInstance());
        pathwayComponent_Interaction.add(CmpAsm_000007.getInstance());
        pathwayComponent_Interaction.add(Trans_000001.getInstance());
    }

}

