/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class CmpAsmA_000005 extends CmpAsmAgent {

    private static CmpAsmA_000005 cmpAsmA_000005 = null;

    private CmpAsmA_000005() {
    }

    public static CmpAsmA_000005 getInstance() {
        if (cmpAsmA_000005 == null) {
            cmpAsmA_000005 = new CmpAsmA_000005();
            cmpAsmA_000005.init();
        }
        return cmpAsmA_000005;
    }

    private void init() {
        cmpAsm = CmpAsm_000005.getInstance();
        interaction = cmpAsm;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

