/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class Stoi_000006 extends Stoichiometry {

    private static Stoi_000006 stoi_000006 = null;

    private Stoi_000006() {
    }

    public static Stoi_000006 getInstance() {
        if (stoi_000006 == null) {
            stoi_000006 = new Stoi_000006();
            stoi_000006.init();
        }
        return stoi_000006;
    }

    private void init() {
        id = "Stoi_000006";
        rdfId = "http://www.vu.nl/bioinf/ebf/Stoi-Trans-TARGET-PROTEIN";
        physicalEntity = Prot_000008.getInstance();
        stoichiometricCoefficient = 10.0f;
    }

}

