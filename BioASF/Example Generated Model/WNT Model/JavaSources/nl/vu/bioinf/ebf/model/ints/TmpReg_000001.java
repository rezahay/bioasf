/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReg_000001 extends TemplateReactionRegulation {

    private static TmpReg_000001 tmpReg_000001 = null;

    private TmpReg_000001() {
    }

    public static TmpReg_000001 getInstance() {
        if (tmpReg_000001 == null) {
            tmpReg_000001 = new TmpReg_000001();
            tmpReg_000001.init();
        }
        return tmpReg_000001;
    }

    private void init() {
        id = "TmpReg_000001";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReg-TCF-LEF-CTNNB1-TARGET";
        controlType = Enum_controlType.ACTIVATION;
        controller = new ArrayList<PhysicalEntity>();
        controller.add(Cmp_000004.getInstance());
        controlled = new ArrayList<TemplateReaction>();
        controlled.add(TmpReac_000001.getInstance());
    }

}

