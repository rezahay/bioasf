/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class Cmp_000003 extends Complex {

    private static Cmp_000003 cmp_000003 = null;

    private Cmp_000003() {
    }

    public static Cmp_000003 getInstance() {
        if (cmp_000003 == null) {
            cmp_000003 = new Cmp_000003();
            cmp_000003.init();
        }
        return cmp_000003;
    }

    private void init() {
        id = "Cmp_000003";
        rdfId = "http://www.vu.nl/bioinf/ebf/RC";
        displayName = "RC";
        component = new ArrayList<PhysicalEntity>();
        component.add(Prot_000001.getInstance());
        component.add(Cmp_000001.getInstance());
    }

}

