/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class Prot_000004 extends Protein {

    private static Prot_000004 prot_000004 = null;

    private Prot_000004() {
    }

    public static Prot_000004 getInstance() {
        if (prot_000004 == null) {
            prot_000004 = new Prot_000004();
            prot_000004.init();
        }
        return prot_000004;
    }

    private void init() {
        id = "Prot_000004";
        rdfId = "http://www.vu.nl/bioinf/ebf/FZD";
        displayName = "FZD";
    }

}

