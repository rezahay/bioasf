/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000024 extends TmpRegAgent {

    private static TmpRegA_000024 tmpRegA_000024 = null;

    private TmpRegA_000024() {
    }

    public static TmpRegA_000024 getInstance() {
        if (tmpRegA_000024 == null) {
            tmpRegA_000024 = new TmpRegA_000024();
            tmpRegA_000024.init();
        }
        return tmpRegA_000024;
    }

    private void init() {
        tmpReg = TmpReg_000024.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

