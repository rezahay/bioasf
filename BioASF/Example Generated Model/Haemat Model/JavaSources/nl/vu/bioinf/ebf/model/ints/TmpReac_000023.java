/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000023 extends TemplateReaction {

    private static TmpReac_000023 tmpReac_000023 = null;

    private TmpReac_000023() {
    }

    public static TmpReac_000023 getInstance() {
        if (tmpReac_000023 == null) {
            tmpReac_000023 = new TmpReac_000023();
            tmpReac_000023.init();
        }
        return tmpReac_000023;
    }

    private void init() {
        id = "TmpReac_000023";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Hhex-1";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000010.getInstance());
    }

}

