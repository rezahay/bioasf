/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000039 extends TemplateReaction {

    private static TmpReac_000039 tmpReac_000039 = null;

    private TmpReac_000039() {
    }

    public static TmpReac_000039 getInstance() {
        if (tmpReac_000039 == null) {
            tmpReac_000039 = new TmpReac_000039();
            tmpReac_000039.init();
        }
        return tmpReac_000039;
    }

    private void init() {
        id = "TmpReac_000039";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Fli1-4";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000004.getInstance());
    }

}

