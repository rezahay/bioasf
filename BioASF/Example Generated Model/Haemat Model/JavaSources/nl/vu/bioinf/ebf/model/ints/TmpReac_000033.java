/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000033 extends TemplateReaction {

    private static TmpReac_000033 tmpReac_000033 = null;

    private TmpReac_000033() {
    }

    public static TmpReac_000033 getInstance() {
        if (tmpReac_000033 == null) {
            tmpReac_000033 = new TmpReac_000033();
            tmpReac_000033.init();
        }
        return tmpReac_000033;
    }

    private void init() {
        id = "TmpReac_000033";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Pu1-4";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000008.getInstance());
    }

}

