/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000011 extends TemplateReaction {

    private static TmpReac_000011 tmpReac_000011 = null;

    private TmpReac_000011() {
    }

    public static TmpReac_000011 getInstance() {
        if (tmpReac_000011 == null) {
            tmpReac_000011 = new TmpReac_000011();
            tmpReac_000011.init();
        }
        return tmpReac_000011;
    }

    private void init() {
        id = "TmpReac_000011";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Hhex-2";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000010.getInstance());
    }

}

