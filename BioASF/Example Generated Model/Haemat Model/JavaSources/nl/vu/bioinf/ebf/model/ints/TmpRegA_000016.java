/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000016 extends TmpRegAgent {

    private static TmpRegA_000016 tmpRegA_000016 = null;

    private TmpRegA_000016() {
    }

    public static TmpRegA_000016 getInstance() {
        if (tmpRegA_000016 == null) {
            tmpRegA_000016 = new TmpRegA_000016();
            tmpRegA_000016.init();
        }
        return tmpRegA_000016;
    }

    private void init() {
        tmpReg = TmpReg_000016.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

