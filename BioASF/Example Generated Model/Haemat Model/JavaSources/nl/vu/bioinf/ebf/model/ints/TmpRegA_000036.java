/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000036 extends TmpRegAgent {

    private static TmpRegA_000036 tmpRegA_000036 = null;

    private TmpRegA_000036() {
    }

    public static TmpRegA_000036 getInstance() {
        if (tmpRegA_000036 == null) {
            tmpRegA_000036 = new TmpRegA_000036();
            tmpRegA_000036.init();
        }
        return tmpRegA_000036;
    }

    private void init() {
        tmpReg = TmpReg_000036.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

