/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReg_000024 extends TemplateReactionRegulation {

    private static TmpReg_000024 tmpReg_000024 = null;

    private TmpReg_000024() {
    }

    public static TmpReg_000024 getInstance() {
        if (tmpReg_000024 == null) {
            tmpReg_000024 = new TmpReg_000024();
            tmpReg_000024.init();
        }
        return tmpReg_000024;
    }

    private void init() {
        id = "TmpReg_000024";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReg-Runx1-Erg";
        controlType = Enum_controlType.ACTIVATION;
        controller = new ArrayList<PhysicalEntity>();
        controller.add(Prot_000009.getInstance());
        controlled = new ArrayList<TemplateReaction>();
        controlled.add(TmpReac_000024.getInstance());
    }

}

