/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpReacA_000032 extends TmpReacAgent {

    private static TmpReacA_000032 tmpReacA_000032 = null;

    private TmpReacA_000032() {
    }

    public static TmpReacA_000032 getInstance() {
        if (tmpReacA_000032 == null) {
            tmpReacA_000032 = new TmpReacA_000032();
            tmpReacA_000032.init();
        }
        return tmpReacA_000032;
    }

    private void init() {
        tmpReac = TmpReac_000032.getInstance();
        interaction = tmpReac;
    }

    protected void initAgent() {
        tmpRegs = new ArrayList<TemplateReactionRegulation>();
        tmpRegs.add(TmpReg_000032.getInstance());
        tmpRegAgents = new ArrayList<TmpRegAgent>();
        tmpRegAgents.add(TmpRegA_000032.getInstance());
        super.initAgent();
    }

}

