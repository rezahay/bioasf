/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000033 extends TmpRegAgent {

    private static TmpRegA_000033 tmpRegA_000033 = null;

    private TmpRegA_000033() {
    }

    public static TmpRegA_000033 getInstance() {
        if (tmpRegA_000033 == null) {
            tmpRegA_000033 = new TmpRegA_000033();
            tmpRegA_000033.init();
        }
        return tmpRegA_000033;
    }

    private void init() {
        tmpReg = TmpReg_000033.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

