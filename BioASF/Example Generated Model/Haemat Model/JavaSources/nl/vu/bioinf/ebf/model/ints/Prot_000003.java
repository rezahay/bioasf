/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class Prot_000003 extends Protein {

    private static Prot_000003 prot_000003 = null;

    private Prot_000003() {
    }

    public static Prot_000003 getInstance() {
        if (prot_000003 == null) {
            prot_000003 = new Prot_000003();
            prot_000003.init();
        }
        return prot_000003;
    }

    private void init() {
        id = "Prot_000003";
        rdfId = "http://www.vu.nl/bioinf/ebf/Gata2";
        displayName = "Gata2";
    }

}

