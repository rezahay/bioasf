/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpReacA_000034 extends TmpReacAgent {

    private static TmpReacA_000034 tmpReacA_000034 = null;

    private TmpReacA_000034() {
    }

    public static TmpReacA_000034 getInstance() {
        if (tmpReacA_000034 == null) {
            tmpReacA_000034 = new TmpReacA_000034();
            tmpReacA_000034.init();
        }
        return tmpReacA_000034;
    }

    private void init() {
        tmpReac = TmpReac_000034.getInstance();
        interaction = tmpReac;
    }

    protected void initAgent() {
        tmpRegs = new ArrayList<TemplateReactionRegulation>();
        tmpRegs.add(TmpReg_000034.getInstance());
        tmpRegAgents = new ArrayList<TmpRegAgent>();
        tmpRegAgents.add(TmpRegA_000034.getInstance());
        super.initAgent();
    }

}

