/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReg_000015 extends TemplateReactionRegulation {

    private static TmpReg_000015 tmpReg_000015 = null;

    private TmpReg_000015() {
    }

    public static TmpReg_000015 getInstance() {
        if (tmpReg_000015 == null) {
            tmpReg_000015 = new TmpReg_000015();
            tmpReg_000015.init();
        }
        return tmpReg_000015;
    }

    private void init() {
        id = "TmpReg_000015";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReg-Gata2-Scl-Runx1";
        controlType = Enum_controlType.ACTIVATION;
        controller = new ArrayList<PhysicalEntity>();
        controller.add(Prot_000003.getInstance());
        controller.add(Prot_000005.getInstance());
        controlled = new ArrayList<TemplateReaction>();
        controlled.add(TmpReac_000015.getInstance());
    }

}

