/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000028 extends TemplateReaction {

    private static TmpReac_000028 tmpReac_000028 = null;

    private TmpReac_000028() {
    }

    public static TmpReac_000028 getInstance() {
        if (tmpReac_000028 == null) {
            tmpReac_000028 = new TmpReac_000028();
            tmpReac_000028.init();
        }
        return tmpReac_000028;
    }

    private void init() {
        id = "TmpReac_000028";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Erg-1";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000007.getInstance());
    }

}

