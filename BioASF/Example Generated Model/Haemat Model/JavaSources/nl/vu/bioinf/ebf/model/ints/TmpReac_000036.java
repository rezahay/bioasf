/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000036 extends TemplateReaction {

    private static TmpReac_000036 tmpReac_000036 = null;

    private TmpReac_000036() {
    }

    public static TmpReac_000036 getInstance() {
        if (tmpReac_000036 == null) {
            tmpReac_000036 = new TmpReac_000036();
            tmpReac_000036.init();
        }
        return tmpReac_000036;
    }

    private void init() {
        id = "TmpReac_000036";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Scl-5";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000005.getInstance());
    }

}

