/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000039 extends TmpRegAgent {

    private static TmpRegA_000039 tmpRegA_000039 = null;

    private TmpRegA_000039() {
    }

    public static TmpRegA_000039 getInstance() {
        if (tmpRegA_000039 == null) {
            tmpRegA_000039 = new TmpRegA_000039();
            tmpRegA_000039.init();
        }
        return tmpRegA_000039;
    }

    private void init() {
        tmpReg = TmpReg_000039.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

