/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000027 extends TemplateReaction {

    private static TmpReac_000027 tmpReac_000027 = null;

    private TmpReac_000027() {
    }

    public static TmpReac_000027 getInstance() {
        if (tmpReac_000027 == null) {
            tmpReac_000027 = new TmpReac_000027();
            tmpReac_000027.init();
        }
        return tmpReac_000027;
    }

    private void init() {
        id = "TmpReac_000027";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Pu1-2";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000008.getInstance());
    }

}

