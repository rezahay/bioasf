/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000027 extends TmpRegAgent {

    private static TmpRegA_000027 tmpRegA_000027 = null;

    private TmpRegA_000027() {
    }

    public static TmpRegA_000027 getInstance() {
        if (tmpRegA_000027 == null) {
            tmpRegA_000027 = new TmpRegA_000027();
            tmpRegA_000027.init();
        }
        return tmpRegA_000027;
    }

    private void init() {
        tmpReg = TmpReg_000027.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

