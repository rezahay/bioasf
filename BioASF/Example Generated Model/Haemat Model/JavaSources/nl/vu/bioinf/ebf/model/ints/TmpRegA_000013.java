/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpRegA_000013 extends TmpRegAgent {

    private static TmpRegA_000013 tmpRegA_000013 = null;

    private TmpRegA_000013() {
    }

    public static TmpRegA_000013 getInstance() {
        if (tmpRegA_000013 == null) {
            tmpRegA_000013 = new TmpRegA_000013();
            tmpRegA_000013.init();
        }
        return tmpRegA_000013;
    }

    private void init() {
        tmpReg = TmpReg_000013.getInstance();
        interaction = tmpReg;
    }

    protected void initAgent() {
        super.initAgent();
    }

}

