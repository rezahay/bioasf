/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReg_000030 extends TemplateReactionRegulation {

    private static TmpReg_000030 tmpReg_000030 = null;

    private TmpReg_000030() {
    }

    public static TmpReg_000030 getInstance() {
        if (tmpReg_000030 == null) {
            tmpReg_000030 = new TmpReg_000030();
            tmpReg_000030.init();
        }
        return tmpReg_000030;
    }

    private void init() {
        id = "TmpReg_000030";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReg-Gata2-Fli1";
        controlType = Enum_controlType.ACTIVATION;
        controller = new ArrayList<PhysicalEntity>();
        controller.add(Prot_000003.getInstance());
        controlled = new ArrayList<TemplateReaction>();
        controlled.add(TmpReac_000030.getInstance());
    }

}

