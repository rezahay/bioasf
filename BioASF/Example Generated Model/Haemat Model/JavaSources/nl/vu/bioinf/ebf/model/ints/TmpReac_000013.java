/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;

public class TmpReac_000013 extends TemplateReaction {

    private static TmpReac_000013 tmpReac_000013 = null;

    private TmpReac_000013() {
    }

    public static TmpReac_000013 getInstance() {
        if (tmpReac_000013 == null) {
            tmpReac_000013 = new TmpReac_000013();
            tmpReac_000013.init();
        }
        return tmpReac_000013;
    }

    private void init() {
        id = "TmpReac_000013";
        rdfId = "http://www.vu.nl/bioinf/ebf/TmpReac-Gata1-1";
        product_Protein = new ArrayList<Protein>();
        product_Protein.add(Prot_000002.getInstance());
    }

}

