/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpReacA_000038 extends TmpReacAgent {

    private static TmpReacA_000038 tmpReacA_000038 = null;

    private TmpReacA_000038() {
    }

    public static TmpReacA_000038 getInstance() {
        if (tmpReacA_000038 == null) {
            tmpReacA_000038 = new TmpReacA_000038();
            tmpReacA_000038.init();
        }
        return tmpReacA_000038;
    }

    private void init() {
        tmpReac = TmpReac_000038.getInstance();
        interaction = tmpReac;
    }

    protected void initAgent() {
        tmpRegs = new ArrayList<TemplateReactionRegulation>();
        tmpRegs.add(TmpReg_000038.getInstance());
        tmpRegAgents = new ArrayList<TmpRegAgent>();
        tmpRegAgents.add(TmpRegA_000038.getInstance());
        super.initAgent();
    }

}

