/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class.
 */

package nl.vu.bioinf.ebf.model.ints;

import java.util.ArrayList;
import nl.vu.bioinf.ebf.metamodel.*;
import nl.vu.bioinf.ebf.metamodel.interaction.*;

public class TmpReacA_000024 extends TmpReacAgent {

    private static TmpReacA_000024 tmpReacA_000024 = null;

    private TmpReacA_000024() {
    }

    public static TmpReacA_000024 getInstance() {
        if (tmpReacA_000024 == null) {
            tmpReacA_000024 = new TmpReacA_000024();
            tmpReacA_000024.init();
        }
        return tmpReacA_000024;
    }

    private void init() {
        tmpReac = TmpReac_000024.getInstance();
        interaction = tmpReac;
    }

    protected void initAgent() {
        tmpRegs = new ArrayList<TemplateReactionRegulation>();
        tmpRegs.add(TmpReg_000024.getInstance());
        tmpRegAgents = new ArrayList<TmpRegAgent>();
        tmpRegAgents.add(TmpRegA_000024.getInstance());
        super.initAgent();
    }

}

