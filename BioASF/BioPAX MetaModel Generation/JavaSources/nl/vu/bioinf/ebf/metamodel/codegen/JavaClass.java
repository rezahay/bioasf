package nl.vu.bioinf.ebf.metamodel.codegen;

import java.util.ArrayList;
import java.util.List;

import nl.vu.bioinf.ebf.metamodel.codegen.JavaField;

/**
 */

public class JavaClass
{
    // the same for all generated classes
    private static String basePackageName = null;

    // package + class name
    private String packageName;     // optional
    private String className;       // required
    private JavaClass superClass;   // if any

    private boolean isNativeClass;
    private String enumElements;
    private String owlName;

    // all fields
    private List<JavaField> fields;

    public JavaClass (String packageName, String className)
    {
        this (packageName, className, null);
    }

    public JavaClass (String packageName, String className, JavaClass superClass) {
        this.packageName = packageName;
        this.className = className;
        this.superClass = superClass;

        this.isNativeClass = false;
        this.enumElements = null;

        this.fields = new ArrayList<JavaField>(20);
    }

    //
    // get/set methods
    //

    public String getEnumElements() {
		return enumElements;
	}

	public void setEnumElements(String enumElements) {
		this.enumElements = enumElements;
	}

	public static void setBasePackageName (String basePackageName)
    {
        synchronized (JavaClass.class)
        {
            JavaClass.basePackageName = basePackageName;
        }
    }

    public static String getBasePackageName ()
    {
        return basePackageName;
    }

    public void setSuperClass (JavaClass superClass)
    {
        this.superClass = superClass;
    }

    public JavaClass getSuperClass ()
    {
        return this.superClass;
    }

    public boolean hasSuperClass ()
    {
        return this.superClass != null;
    }

    public boolean isSubClassOf (JavaClass jc)
    {
        JavaClass sc = this;

        while (true) {
            if (sc.equals (jc))
                return true;

            if (!sc.hasSuperClass())
                return false;

            sc = sc.getSuperClass ();
        }

    }

    public boolean isSuperClassOf (JavaClass jc)
    {
        return jc.isSubClassOf (this);
    }

    public String getFullPackageName ()
    {
        if (isNativeClass())
            return null;

        StringBuffer result = new StringBuffer();

        if (basePackageName != null)
        {
            result.append (basePackageName);
            if (packageName != null)
                result.append (".");
        }

        if (packageName != null)
        {
            result.append (packageName);
        }

        return result.toString();
    }

    public String getFullName ()
    {
        if (isNativeClass())
            return getClassName();

        StringBuffer result = new StringBuffer();

        if (basePackageName != null)
        {
            result.append (basePackageName);
            result.append (".");
        }

        if (packageName != null)
        {
            result.append (packageName);
            result.append (".");
        }

        result.append (className);

        return result.toString();
    }

    public String getPackageName ()
    {
        if (isNativeClass())
            return null;

        return packageName;
    }

    public String getClassName ()
    {
        return className;
    }

    public String getName ()
    {
        if (packageName != null)
            return packageName + "." + className;

        return className;
    }

    public void setPackageName (String packageName)
    {
        this.packageName = packageName;
    }

    public void setName (String className)
    {
        this.className = className;
    }

    public void addField (JavaField field)
    {
        if (!fields.contains(field))
            fields.add(field);
    }

    public List<JavaField> getFields ()
    {
        return fields;
    }

    public JavaField getField (String name)
        throws Exception
    {
        for (JavaField jf : fields)
        {
            if (name.equals(jf.getName()))
                return jf;
        }

        if (superClass != null)
        {
            return superClass.getField (name);
        }

        throw new Exception ("NoSuchField: " + className + "." + name);
    }

    //
    // Comparison/hashcodes
    //

    public boolean packageNameEquals (String packageName)
    {
        if (packageName == null)
            return this.packageName == null;

        if (this.packageName == null)
            return false;

        return packageName.equals (this.packageName);
    }

    public boolean equals (JavaClass cmp)
    {
        return packageNameEquals(cmp.packageName) &&
            this.className.equals(cmp.className);
    }

    // extra info

    public void setIsNativeClass (boolean value)
    {
        this.isNativeClass = value;
    }

    public boolean isNativeClass ()
    {
        return this.isNativeClass;
    }

    public void setOwlName (String owlName)
    {
        this.owlName = owlName;
    }

    public String getOwlName ()
    {
        return this.owlName;
    }
}
