package nl.vu.bioinf.ebf.metamodel.codegen;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.vu.bioinf.ebf.metamodel.codegen.JavaClass;
import nl.vu.bioinf.ebf.metamodel.codegen.JavaTextGenerator;
import nl.vu.bioinf.ebf.metamodel.codegen.OWLJavaFilter;

public class GenerateMetaModel {
	
    // default values
	// -d "C:/Workspace/EclipseWS/EBF/OWL" "file:///C:/Workspace/EclipseWS/EBF/OWL/biopax-level3.owl" 
	// -Dprotege.dir=.
    private static final String DEFAULT_OUTDIR = ".";
    private static final String DEFAULT_PACKAGE = "nl.vu.bioinf.ebf.metamodel";
    private String outputDir = DEFAULT_OUTDIR;
    private String basePackage = DEFAULT_PACKAGE;

    // classes to ignore (simple hack to prevent generating classes for
    // builtin/primitive Java types).
    private static String[] ignoreGenerateStrings = 
        new String[] { "int", "string", "dateTime", "float" };

    private static boolean ignoreGenerate (String name)
    {
        for (String s : ignoreGenerateStrings)
            if (s.equalsIgnoreCase (name))
                return true;

        return false;
    }

    // filter instance
    private OWLJavaFilter filter;


    private void generateAllClasses (BufferedReader in)
        throws Exception
    {
        // get all classes
        JavaClass.setBasePackageName (basePackage);
        for (JavaClass jc : filter.getJClasses())
        {
            if (!jc.isNativeClass() && !ignoreGenerate(jc.getClassName()))
                generateJavaFile (jc);
        }
    }

    private boolean skipGeneration(String className) {
    	boolean result = false;
        for (int i = 0; i < className.length(); i++){
            if (Character.isWhitespace(className.charAt(i))) {
            	result = true;
            }
        }
    	
    	//System.out.println("### className: " + className);
        return result;
    }
    
    private void generateJavaFile (JavaClass jc) throws Exception {
        File dir = new File (outputDir);
        if (!dir.isDirectory()) {
            System.out.println ("Cannot output to directory " + outputDir);
            throw new Exception ("Cannot output to directory " + outputDir);
        }
        
        if (skipGeneration(jc.getClassName())) {
        	return;
        }
        
        if (jc.getFullPackageName() != null)
            dir = generateDirFromPackageName (dir, jc.getFullPackageName());

        // create java files
        // ##Reza
        //File abstractFile = new File (dir.getPath() + File.separator + "Base" + jc.getClassName() + ".java");
        File abstractFile = new File (dir.getPath() + File.separator + jc.getClassName() + ".java");

        // generate file contents
        String abstractText = JavaTextGenerator.generateJavaText (jc);

        PrintWriter out = new PrintWriter (abstractFile);
        out.println (abstractText);
        out.flush();
        out.close();       
    }

    private File generateDirFromPackageName (File basedir, String name)
        throws Exception
    {
        // substitute package separator char (.) with file separator char
        StringBuffer dirName = new StringBuffer(name);
        for (int i = 0; i < dirName.length(); i++)
        {
            if (dirName.charAt(i) == '.')
                dirName.setCharAt(i, File.separatorChar);
        }

        // create the new directory
        File dir = new File (basedir.getPath() + 
                File.separator + dirName.toString());
        dir.mkdirs();
        return dir;
    }

    private void usageAndExit (String msg)
    {
        if (msg != null)
            System.out.println (msg);

        System.out.println ("Usage: GenerateMetaModel [OPTIONS] inputfile.owl");
        System.out.println ("OPTIONS: ");
        System.out.println (" -d <dir>     Set output directory (default " + 
                DEFAULT_OUTDIR + ")");
        System.out.println (" -p <dir>     Set output classpath (default " + 
                DEFAULT_PACKAGE + ")");
        System.out.println (" -renew       " + 
                "Generate new implementation files (may remove old ones)");

        System.exit (0);
    }

    private void usageAndExit ()
    {
        usageAndExit (null);
    }

    private void start (String[] args)
        throws Exception
    {
        String inputFile = null;

        for (int i = 0; i < args.length; i++)
        {
            if (inputFile != null)
            {
                usageAndExit ("Multiple input files supplied");
            }

            if (args[i].equals("-d"))
            {
                if (i+1 < args.length)
                {
                    outputDir = args[++i];
                    File test = new File (outputDir);
                    if (!test.isDirectory())
                        usageAndExit ("Cannot use directory " + outputDir);
                }
                else 
                {
                    usageAndExit ("Please provide output location");
                }
            }

            else if (args[i].equals("-p"))
            {
                if (i+1 < args.length) 
                {
                    if (i+1 < args.length) basePackage = args[++i];
                    else usageAndExit ("Please provide package name");
                }
                else
                { 
                    usageAndExit();
                }
            }
            else if (args[i].equals("-renew"))
            {
                System.out.println ("Warning: overwriting any " + "old implementation files!");
            }

            else
            {
                inputFile = args[i];
            }
        }

        if (inputFile == null)
            usageAndExit ();

        // create owl model
        try {
            filter = new OWLJavaFilter (inputFile, false);
        }
        catch (Exception ex)
        {
            System.out.println ("Exception creating OWLJavaFilter: " + 
                    ex.getMessage());
            ex.printStackTrace();

            throw ex;
        }

        generateAllClasses (null);
    }

    public static void main(String[] args) 
    {
        try 
        {
            new GenerateMetaModel().start(args);
        }
        catch (Exception e)
        {
            System.out.println ("Exception in GenerateMetaModel: " + e.getMessage());
            e.printStackTrace();


            System.exit (1);
        }
    }
}
