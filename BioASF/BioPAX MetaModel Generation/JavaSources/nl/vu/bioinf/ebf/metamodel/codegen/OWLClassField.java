package nl.vu.bioinf.ebf.metamodel.codegen;

public class OWLClassField {
    private String fieldName;
    private String fieldType;
    private int restrictionValue;
    private boolean isNativeClass;
    private String enumElements;
    private String hasValue;

    private boolean available; // if class type not found in OWL document

    OWLClassField (String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.available = true;
        this.isNativeClass = false;
        this.enumElements = null;
        this.restrictionValue = 0;
        this.hasValue = null;
    }

	public String getHasValue() {
		return hasValue;
	}

	public void setHasValue(String hasValue) {
		this.hasValue = hasValue;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public void setAvailable (boolean available)
    {
        this.available = available;
    }

    public boolean isAvailable ()
    {
        return this.available;
    }

    public String getFieldName ()
    {
        String[] tokens = this.fieldName.split(":");
        return tokens[tokens.length-1];
    }

    public String getFullFieldName ()
    {
        return this.fieldName;
    }

    public String getFieldType ()
    {
        return this.fieldType;
    }

    public void setRestrictionValue(int restrictionValue) {
		this.restrictionValue = restrictionValue;
	}

	public void setNativeClass(boolean isNativeClass) {
		this.isNativeClass = isNativeClass;
	}

	public int getRestrictionValue ()
    {
        return this.restrictionValue;
    }

    public void setIsNativeClass (boolean value)
    {
        this.isNativeClass = value;
    }

    public boolean isNativeClass ()
    {
        return this.isNativeClass;
    }

	public String getEnumElements() {
		return enumElements;
	}

	public void setEnumElements(String enumElements) {
		this.enumElements = enumElements;
	}

    
}
