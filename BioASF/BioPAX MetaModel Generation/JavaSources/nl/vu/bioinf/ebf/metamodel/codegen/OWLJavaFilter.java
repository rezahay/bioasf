package nl.vu.bioinf.ebf.metamodel.codegen;

import nl.vu.bioinf.ebf.metamodel.codegen.JavaClass;
import nl.vu.bioinf.ebf.metamodel.codegen.JavaField;

import java.util.*;

public class OWLJavaFilter {
    // default values
    private static final String DEFAULT_PACKAGE = "nl.vu.bioinf.ebf.metamodel";
    private String basePackage = DEFAULT_PACKAGE;

    // parser instance
    private OWLParser parser;
    private List<JavaClass> javaClasses;

    // these classes should be treated as native classes
    private static String[] nativeClasses = 
        new String[] { "boolean", "Object", "String", "float" };


    public OWLJavaFilter (String fileName) throws Exception {
        this (fileName, true);
    }

    public OWLJavaFilter (String fileName, boolean generateIndividuals) throws Exception {
        parser = new OWLParser (fileName);
        javaClasses = new Vector<JavaClass>();
        JavaClass.setBasePackageName (basePackage);

        for (String className : parser.getOwlClasses()) {
            // get the java class info structure
            JavaClass jc = getJClassFromOwl (className);

            // add all the fields to it
            for (OWLClassField ocf : parser.getClassFields (className)) {
                // obtain field name + tpye
                JavaClass type = getJClassFromOwl (ocf.getFieldType());
                type.setEnumElements(ocf.getEnumElements());
                String name = ocf.getFieldName();
                String hasValue = ocf.getHasValue();

                // XXX if field has an illegal name, append "Field" to it
                if (isReservedName(name))
                    name = getNonReservedName (name);

                // obtain field cardinality
                JavaField.Cardinality c = JavaField.Cardinality.Multiple;
        		int cardinality = ocf.getRestrictionValue();
        		if (cardinality == 1) {
        			c = JavaField.Cardinality.Single;
        		}
        		
        		// create field + add to the JavaClass
                JavaField jf = new JavaField (type, name, c, hasValue);
                jc.addField (jf);
            }
        }
    }

    public OWLParser getParser ()
    {
        return parser;
    }

    public List<JavaClass> getJClasses ()
    {
        return javaClasses;
    }


    //
    // OWL2Java naming + conversion
    //

    public JavaClass getJClassFromOwl (String owlBrowserTextName)
        throws Exception
    {
        // OWL browser text is formatted as <package>:<class>
        // convert this to suitable package.class names
        String className = null;
        String packageName = null;

        String[] tokens = owlBrowserTextName.trim().split(":");
        switch (tokens.length)
        {
            case 1:
                // no package name
                // 1st one is class name
                className = tokens[0];

                // class name conversion done
                break;

            case 2:
                // 1st one is package name
                // filter out all non-alphanumeric chars
                StringBuffer tmp = new StringBuffer ();
                for (int i = 0; i < tokens[0].length(); i++)
                {
                    char ch = tokens[0].charAt(i);
                    if (Character.isLetterOrDigit(ch))
                        tmp.append (ch);
                }
                packageName = tmp.toString();

                // 2nd one is class name
                className = tokens[1];

                // class name conversion done
                break;

            default:
                throw new Exception ("Cannot create JavaClass from [" + 
                        owlBrowserTextName + "]");
        }

        try 
        {
            return getJClass (packageName, className);
        }
        catch (Exception e)
        {
            // create a new class, need the superclass for this as well
            // (if there is one), otherwise just skip the super class
            JavaClass result = null;

            try
            {
                JavaClass superClass = getJClassFromOwl (
                        parser.getSuperClassName (owlBrowserTextName));

                result = addJClass (packageName, className, superClass);
            }
            catch (Exception ex)
            {
                // no superclass, create top level class
                result = addJClass (packageName, className);
            }

            result.setOwlName (owlBrowserTextName);
            return result;
        }
    }

    public JavaClass getJClass (String className)
        throws Exception
    {
        if (className.equals("string"))
            className = "String";

        return getJClass (null, className);
    }

    public JavaClass getJClass (String packageName, String className)
        throws Exception
    {
        synchronized (javaClasses)
        {
            // see if there already is an JavaClass with this name
            for (JavaClass jc : javaClasses)
            {
                if (jc.packageNameEquals(packageName) &&
                        jc.getClassName().equals(className))
                {
                    return jc;
                }
            }
        }

        throw new Exception ("JavaClass " + 
                (packageName == null ? (packageName + ".") : "") + 
                className + " unknown");
    }

    private JavaClass addJClass (String packageName, String className)
    {
        return addJClass (packageName, className, null);
    }

    private JavaClass addJClass (String packageName, String className,
            JavaClass superClass)
    {
        if (className.equals("string"))
            className = "String";

        JavaClass jc = new JavaClass (packageName, className, superClass);

        if (packageName == null)
        {
            for (String nativeName : nativeClasses)
            {
                if (className.equals(nativeName))
                    jc.setIsNativeClass (true);
            }
        }

        javaClasses.add (jc);
        return jc;
    }

    public String getPlainClassName (String name)
    {
        String[] tokens = name.split(":");
        return tokens[tokens.length-1];
    }

    public String getUpperCaseString (String name)
    {
        return "" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
    }

    // reserved name matching

    private static Hashtable<String,String> nameMappings = null;

    private static void buildNameMappings ()
    {
        if (nameMappings == null)
        {
            nameMappings = new Hashtable<String,String>();

            nameMappings.put ("class", "className");
            nameMappings.put ("int", "int");
            nameMappings.put ("string", "String");
            nameMappings.put ("datetime", "dateTime");
            nameMappings.put ("float", "float");
        }
    }

    private static boolean isReservedName (String name)
    {
        if (nameMappings == null)
            buildNameMappings();

        return nameMappings.containsKey (name);
    }

    private static String getJavaTypeReservedName (String name)
    {
        if (nameMappings == null)
            buildNameMappings();

        String result = nameMappings.get (name);
        if (result == null)
            result = name;

        return result;
    }

    private static String getNonReservedName (String name)
    {
        if (isReservedName (name))
            return name + "Field";

        return name;
    }
}
