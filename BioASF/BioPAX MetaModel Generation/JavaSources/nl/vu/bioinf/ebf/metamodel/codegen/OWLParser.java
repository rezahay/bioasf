package nl.vu.bioinf.ebf.metamodel.codegen;

import edu.stanford.smi.protegex.owl.model.OWLDataRange;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLUnionClass;
import edu.stanford.smi.protegex.owl.model.RDFProperty;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;

import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.model.OWLRestriction;
import edu.stanford.smi.protegex.owl.model.impl.DefaultOWLAllValuesFrom;
import edu.stanford.smi.protegex.owl.model.impl.DefaultOWLCardinality;
import edu.stanford.smi.protegex.owl.model.impl.DefaultOWLHasValue;
import edu.stanford.smi.protegex.owl.model.impl.DefaultOWLMaxCardinality;
import edu.stanford.smi.protegex.owl.model.impl.DefaultOWLMinCardinality;
import edu.stanford.smi.protegex.owl.model.impl.AbstractOWLNAryLogicalClass;

import java.util.*;

public class OWLParser {

	private OWLModel owlModel;
	private Hashtable<String, OWLNamedClass> classes;
	private Hashtable<String, List<OWLClassField>> classFields;

	//
	// constructor parses OWL file and creates the model
	//

	// public OWLParser (File f) throws Exception
	public OWLParser(String fileURI) throws Exception {
		// This causes problems for windows environment. The
		// f.getCanonicalPath() returns the absolute path of the file
		// containing windows file separator. The result is an illegal URI.
		// owlModel = ProtegeOWL.createJenaOWLModelFromURI(
		// "file:/" + f.getCanonicalPath());

		// owlModel =
		// ProtegeOWL.createJenaOWLModelFromURI(f.toURI().toString());
		// String uri = "http://localhost/owl/trade-app.owl";
		// owlModel = ProtegeOWL.createJenaOWLModelFromURI(uri);
		owlModel = ProtegeOWL.createJenaOWLModelFromURI(fileURI);
		extractDefinedClasses();

		// for now: filter out the references that are not present
		filterClassReferences();
	}

	public OWLModel getModel() {
		return owlModel;
	}

	// get defined class names from OWL document
	// and create hash map of all class names

	private void extractDefinedClasses() throws Exception {
		// mapping from (ClassName -> OWLNamedClass)
		classes = new Hashtable<String, OWLNamedClass>();
		Collection classCollection = owlModel.getUserDefinedOWLNamedClasses();
		for (Object o : classCollection) {
			OWLNamedClass cls = (OWLNamedClass) o;
			String className = getClassName(cls);
			classes.put(className, cls);
		}

		// next, find out the properties for all of the classes
		extractClassProperties();
		
		
		for (Object o : classCollection) {
			OWLNamedClass cls = (OWLNamedClass) o;
			String className = getClassName(cls);
			handleRestrictions(className);
		}
		

	}

	// get all user defined model properties, these are the class fields
	// store them in the classFields hash table

	private void extractClassProperties() throws Exception {
		classFields = new Hashtable<String, List<OWLClassField>>();
		Collection c = owlModel.getUserDefinedOWLProperties();
		for (Object o : c) {
			RDFProperty prop = (RDFProperty) o;
			System.out.println("Processing property: " + prop.getBrowserText());

			// Create classfield info structure
			String fieldName = prop.getBrowserText();
			
			// ##Reza## This is a protege bug.
			if (fieldName.contains("isRuleEnabled") || fieldName.contains("hasRuleGroup") ) {
				continue;
			}

			// This can be a subproperty who does not have any domain. In this case, we consider its domain 
			// as the domain of its super-property.
			Collection domains = prop.getDomains(false);
			if (domains != null && domains.isEmpty()) {
				System.out.println("(2) Processing property: " + prop.getBrowserText());
				for (Object s : prop.getSuperproperties(false)) {
					RDFProperty superProp = (RDFProperty)s;
					Collection superDomains = superProp.getDomains(false);
					handleDomain(prop, superDomains);
				}
			}
			else {
				handleDomain(prop, domains);
			}
		}
	}
	
	private void handleDomain(RDFProperty prop, Collection domains) throws Exception {
		for (Object d : domains) {
			if (d instanceof OWLNamedClass) {
				processDomain((OWLNamedClass) d, prop);
			} 
			else if (d instanceof AbstractOWLNAryLogicalClass) {
				AbstractOWLNAryLogicalClass a = (AbstractOWLNAryLogicalClass) d;

				for (Object elt : a.getNamedOperands()) {
					if (elt instanceof OWLNamedClass) {
						processDomain((OWLNamedClass) elt, prop);
					} 
				}

			} 
			else {
				System.out.println("UNKNOWN DOMAIN: " + d.getClass().getName());
			}
		}
	}
	
	// ##Reza
	// Replace these kinds of field types:
	// fieldname: 'structureFormat'+ filedtype: 'owl:oneOf{"CML" "SMILES" "InChI"}' 
	// with:
	// public enum StructureFormat{ CML, SMILES, InChI};
    // StructureFormat structureFormat;
	private void handleEnumType(String fieldName, OWLDataRange fieldRange, int cardinality, String hasValue, List<OWLClassField> fields) {
		String enumType = "Enum_" + fieldName;
		StringBuffer enumElementsBuf = new StringBuffer();
		String comma = "";
		for (Object elt : fieldRange.getOneOfValueLiterals()) {
			if (elt instanceof RDFSLiteral) {
				enumElementsBuf.append(comma);
				enumElementsBuf.append(((RDFSLiteral)elt).getBrowserText());
				comma = ", ";
			}
		}
		String enumElements = enumElementsBuf.toString();
		enumElements = enumElements.replaceAll("-", "_");
		
		OWLClassField field = new OWLClassField(fieldName, enumElements);
		field.setRestrictionValue(cardinality);
		field.setHasValue(hasValue);
		field.setEnumElements(enumElements);
		field.setFieldType(enumType);
		fields.add(field);
	}
	
	/*
	 * Replace these kinds of field types:
	 * ArrayList<Pathway or PhysicalEntity> controller;
	 * with:
	 * ArrayList<Pathway> controller_Pathway;
	 * ArrayList<PhysicalEntity> controller_PhysicalEntity;
	 */
	private void handleUnionType(String fieldName, OWLUnionClass fieldRange, int cardinality, List<OWLClassField> fields) {
		for (Object elt : fieldRange.getNamedOperands()) {
			if (elt instanceof OWLNamedClass) {
				String newFieldType = ((OWLNamedClass)elt).getBrowserText();
				String newFieldName = fieldName + "_" + newFieldType;
				OWLClassField newField = new OWLClassField(newFieldName, newFieldType);
				newField.setRestrictionValue(cardinality);
				fields.add(newField);
			}
		}	
	}
	
	private void handleNormalType(String fieldName, RDFResource fieldRange, int cardinality, List<OWLClassField> fields) {
		String fieldType = fieldRange.getBrowserText();
		OWLClassField field = new OWLClassField(fieldName, fieldType);
		field.setRestrictionValue(cardinality);
		fields.add(field);
	}
	
	private void handleRestrictions(String className) throws Exception {
		for (OWLRestriction or : getClassRestrictions(className)) {
			RDFProperty prop = or.getOnProperty();
			String restrictionFieldName = prop.getBrowserText(); 
			
			System.out.println("(2) className: " + className + " && restriction name: " + restrictionFieldName);

			int cardinality = 0;
			String hasValue = null;
			boolean fieldTypeRestricted = false;
			RDFResource fieldRange = prop.getRange();
			
			if (or instanceof DefaultOWLAllValuesFrom) {
				fieldRange = ((DefaultOWLAllValuesFrom) or).getAllValuesFrom();
				fieldTypeRestricted = true;
			}
			
			if (or instanceof DefaultOWLMinCardinality) {
				cardinality = ((DefaultOWLMinCardinality) or).getCardinality();
			}
			else if (or instanceof DefaultOWLMaxCardinality) {
				cardinality = ((DefaultOWLMaxCardinality) or).getCardinality();
			}
			else if (or instanceof DefaultOWLCardinality) {
				cardinality = ((DefaultOWLCardinality) or).getCardinality();
			}
			else if (or instanceof DefaultOWLHasValue) {
				hasValue = ((DefaultOWLHasValue) or).getHasValue().toString();
				hasValue = hasValue.replaceAll("-", "_");
				cardinality = 1;
			}

			if (cardinality == 1 || fieldTypeRestricted) {
				addFieldToClassFields(className, restrictionFieldName, fieldRange, cardinality, hasValue);
			}	
		}
	}

	private void addFieldToClassFields(String className, String fieldName, RDFResource fieldRange, int cardinality, String hasValue) {
		List<OWLClassField> fields = classFields.get(className);
		if (fields == null) {
			fields = new Vector<OWLClassField>();
			classFields.put(className, fields);
		}
		if  (fieldRange instanceof OWLDataRange)  {
			handleEnumType(fieldName, (OWLDataRange)fieldRange, cardinality, hasValue, fields);
		}
		else if (fieldRange instanceof OWLUnionClass) {
			handleUnionType(fieldName, (OWLUnionClass)fieldRange, cardinality, fields);
		}
		else {
			handleNormalType(fieldName, fieldRange, cardinality, fields);
		}
	}

	private void processDomain(OWLNamedClass cls, RDFProperty prop) throws Exception {
		String className = cls.getBrowserText();
		String fieldName = prop.getBrowserText();		
		
		// Don't do anything if a field has restrictions because restricted fields are handled later.
		for (OWLRestriction or : getClassRestrictions(className)) {
			String restrictionFieldName = or.getOnProperty().getBrowserText();
			System.out.println("(1) restrictionFieldName: " + restrictionFieldName);
			if (restrictionFieldName.equals(fieldName)) {
				return;
			}	
		}
		
		int cardinality = prop.isFunctional() ? 1 : 0;
		addFieldToClassFields(className, fieldName, prop.getRange(), cardinality, null);
	}

	// remove all class fields that refer to class types that are NOT
	// present in the OWL document
	// this means we traverse all of the OWLClassFields and disable
	// the classes we do not know..

	// XXX is this still used??
	private void filterClassReferences() {
		for (Map.Entry<String, List<OWLClassField>> me : classFields.entrySet()) {
			for (OWLClassField field : me.getValue()) {
				
				if (classes.get(field.getFieldType()) == null) {
					field.setAvailable(false);
				}
			}
		}
	}

	/**
	 * Extract all defined class (types) from the model.
	 * <p/>
	 * 
	 * @return A list of Strings, containing the names of all defined classes.
	 *         For each name, obtain the corresponding OWLNamedClass using
	 *         {@link #getOwlClass(String)}.
	 */
	public List<String> getOwlClasses() {
		List<String> result = new ArrayList<String>(classes.keySet().size());
		for (String className : classes.keySet()) {
			result.add(className);
			//System.out.println("className: " + className);
		}	
		return result;
	}

	/**
	 * Get all the fields specified by a certain class
	 */

	public List<OWLClassField> getClassFields(String className) {
		List<OWLClassField> result = classFields.get(className);
		return result == null ? (new Vector<OWLClassField>()) : result;
	}

	/**
	 * Get all restrictions belonging to a certain class.
	 * <p/>
	 * <b> TODO implement </b>
	 */

	private List<OWLRestriction> getClassRestrictions(String className) throws Exception {
		List<OWLRestriction> result = new Vector<OWLRestriction>();


		OWLNamedClass cls = getOwlClass(className);
		Collection propertySet = cls.getRDFProperties();

		for (Object p : propertySet) {
			Collection values = cls.getPropertyValues((RDFProperty) p);
			if (values == null)
				break;

			for (Object v : values) {
				if (v instanceof OWLRestriction) {
					result.add((OWLRestriction) v);
				} 
			}
		}

		return result;
	}

	/**
	 * Obtain a set names of all OWL classes parsed by the OWLParser.
	 */

	public Set<String> getOwlClassNames() {
		return classes.keySet();
	}

	/**
	 * Extract the class name of an OWL named class.
	 */

	public String getClassName(OWLNamedClass o) {
		return o.getBrowserText();
	}

	/**
	 * Obtain the name of the direct superclass of the OWL class with name
	 * {@code clsName}.
	 */

	public String getSuperClassName(String clsName) throws Exception {
		return getSuperClassName(getOwlClass(clsName));
	}

	/**
	 * Obtain the name of the direct superclass of an OWL named class.
	 */

	public String getSuperClassName(OWLNamedClass cls) throws Exception {
		final String[] ignoreList = new String[] { "owl:Thing" };
		Collection superClasses = cls.getNamedSuperclasses();
		for (Object o : superClasses) {
			if (o instanceof OWLNamedClass) {
				OWLNamedClass scls = (OWLNamedClass) o;
				String name = getClassName(scls);

				// see if we can ignore the super class (Owl:Thing etc)
				for (String ignore : ignoreList) {
					if (ignore.equals(name))
						continue;
				}

				if (name.equals("owl:Thing")) {
					continue;
				}
				return name;
			}
		}
		throw new Exception("No superclass for " + getClassName(cls));
	}

	/**
	 * Obtain the class name of an OWL individual.
	 */

	public String getClassName(OWLIndividual o) {
		return o.getProtegeType().getNestedBrowserText();
	}

	/**
	 * Obtain the OWL class type of an individual.
	 */

	public OWLNamedClass getOwlClass(OWLIndividual o) throws Exception {
		return getOwlClass(getClassName(o));
	}

	/**
	 * Obtain an OWL class by its ('browser text') name.
	 */
	public OWLNamedClass getOwlClass(String s) throws Exception {
		OWLNamedClass result = classes.get(s);
		return result;
	}
}
