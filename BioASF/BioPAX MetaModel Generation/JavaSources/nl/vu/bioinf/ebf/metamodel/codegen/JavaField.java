package nl.vu.bioinf.ebf.metamodel.codegen;

import nl.vu.bioinf.ebf.metamodel.codegen.JavaClass;

public class JavaField
{
    public enum Cardinality { Single, Multiple };

    private JavaClass type;          // Class type
    private String name;                // Field name
    private Cardinality cardinality;    // Occurrences
    private String hasValue;

    public JavaField (JavaClass type, String name)
    {
        this (type, name, Cardinality.Single, null);
    }

    public JavaField (JavaClass type, String name, Cardinality c, String hasValue)
    {
        this.type = type;
        this.name = name;
        this.cardinality = c;
        this.hasValue = hasValue;
    }

    public String getHasValue() {
		return hasValue;
	}

	public void setHasValue(String hasValue) {
		this.hasValue = hasValue;
	}

	public JavaClass getType ()
    {
        return type;
    }

    public boolean isMultiple ()
    {
        return cardinality == Cardinality.Multiple;
    }

    public String getName ()
    {
        return name;
    }

    public void setName(String name) {
		this.name = name;
	}

	public Cardinality getCardinality ()
    {
        return cardinality;
    }

    public int hashCode()
    {
        return name.hashCode();
    }

    public String toString ()
    {
        return getType().getName() + " " + getName() + " " + getCardinality();
    }

    public boolean equals (Object o)
    {
	if (o instanceof JavaField)
	{
	    JavaField that = (JavaField) o;
	    return this.name == that.name;
	}

	return false;
    }
}
