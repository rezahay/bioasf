/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.ExperimentalForm.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.UtilityClass;

import java.util.ArrayList;

public abstract class ExperimentalForm extends UtilityClass {

    protected ArrayList<EntityFeature> experimentalFeature;
    protected ArrayList<Gene> experimentalFormEntity_Gene;
    protected ArrayList<PhysicalEntity> experimentalFormEntity_PhysicalEntity;
    protected ExperimentalFormVocabulary experimentalFormDescription;

    public ArrayList<EntityFeature> getExperimentalFeature() {
        return experimentalFeature;
    }

    public ArrayList<Gene> getExperimentalFormEntity_Gene() {
        return experimentalFormEntity_Gene;
    }

    public ArrayList<PhysicalEntity> getExperimentalFormEntity_PhysicalEntity() {
        return experimentalFormEntity_PhysicalEntity;
    }

    public ExperimentalFormVocabulary getExperimentalFormDescription() {
        return experimentalFormDescription;
    }

}

