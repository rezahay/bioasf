/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.BiochemicalPathwayStep.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.PathwayStep;

import java.util.ArrayList;

public abstract class BiochemicalPathwayStep extends PathwayStep {

    protected Enum_stepDirection stepDirection;
    protected Conversion stepConversion;
    protected ArrayList<Control> stepProcess;

    public Enum_stepDirection getStepDirection() {
        return stepDirection;
    }

    public Conversion getStepConversion() {
        return stepConversion;
    }

    public ArrayList<Control> getStepProcess() {
        return stepProcess;
    }

}

