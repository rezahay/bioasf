/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.Provenance.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.UtilityClass;

import java.util.ArrayList;

public abstract class Provenance extends UtilityClass {

    protected ArrayList<String> name;
    protected String displayName;
    protected String standardName;
    protected ArrayList<PublicationXref> xref_PublicationXref;
    protected ArrayList<UnificationXref> xref_UnificationXref;

    public ArrayList<String> getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getStandardName() {
        return standardName;
    }

    public ArrayList<PublicationXref> getXref_PublicationXref() {
        return xref_PublicationXref;
    }

    public ArrayList<UnificationXref> getXref_UnificationXref() {
        return xref_UnificationXref;
    }

}

