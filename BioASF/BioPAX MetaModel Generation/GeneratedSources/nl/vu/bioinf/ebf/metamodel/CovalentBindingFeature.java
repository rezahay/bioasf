/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.CovalentBindingFeature.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.BindingFeature;

public abstract class CovalentBindingFeature extends BindingFeature {

}

