/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.GeneticInteraction.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.Interaction;

import java.util.ArrayList;

public abstract class GeneticInteraction extends Interaction {

    protected ArrayList<Score> interactionScore;
    protected InteractionVocabulary interactionType_GeneticInteraction;
    protected PhenotypeVocabulary phenotype;
    protected ArrayList<Gene> participant_GeneticInteraction;

    public ArrayList<Score> getInteractionScore() {
        return interactionScore;
    }

    public InteractionVocabulary getInteractionType_GeneticInteraction() {
        return interactionType_GeneticInteraction;
    }

    public PhenotypeVocabulary getPhenotype() {
        return phenotype;
    }

    public ArrayList<Gene> getParticipant_GeneticInteraction() {
        return participant_GeneticInteraction;
    }

}

