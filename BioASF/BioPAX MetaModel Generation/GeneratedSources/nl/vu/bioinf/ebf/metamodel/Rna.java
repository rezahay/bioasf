/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.Rna.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;

import java.util.ArrayList;

public abstract class Rna extends PhysicalEntity {

    protected ArrayList<Rna> memberPhysicalEntity_Rna;
    protected ArrayList<RnaReference> entityReference;

    public ArrayList<Rna> getMemberPhysicalEntity_Rna() {
        return memberPhysicalEntity_Rna;
    }

    public ArrayList<RnaReference> getEntityReference() {
        return entityReference;
    }

}

