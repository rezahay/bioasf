package nl.vu.bioinf.ebf.metamodel;

public abstract class BasicEntity {

	protected String id;
	protected String rdfId;
	
    public String getId() {
		return id;
	}
    
    public String getRdfId() {
		return rdfId;
	}
}

