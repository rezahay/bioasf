/**
 * This file has automatically been generated by BioPAX2Java (IBIVU - Reza Haydarlou).
 * DO NOT EDIT this Base class. Changes can be applied to the implementation class:
 * nl.vu.bioinf.ebf.metamodel.RnaRegion.
 */

package nl.vu.bioinf.ebf.metamodel;

import nl.vu.bioinf.ebf.metamodel.PhysicalEntity;

import java.util.ArrayList;

public abstract class RnaRegion extends PhysicalEntity {

    protected ArrayList<RnaRegionReference> entityReference;
    protected ArrayList<RnaRegion> memberPhysicalEntity_RnaRegion;

    public ArrayList<RnaRegionReference> getEntityReference() {
        return entityReference;
    }

    public ArrayList<RnaRegion> getMemberPhysicalEntity_RnaRegion() {
        return memberPhysicalEntity_RnaRegion;
    }

}

